// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:another_flushbar/flushbar.dart' as _i5;
import 'package:dio/dio.dart' as _i3;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;
import 'package:shared_preferences/shared_preferences.dart' as _i9;

import 'application/auth/auth_bloc.dart' as _i17;
import 'application/profile/get_profile_bloc.dart' as _i18;
import 'application/test_result/test_result_cubit.dart' as _i10;
import 'domain/auth/i_auth_facade.dart' as _i13;
import 'domain/profile/i_profile_facade.dart' as _i15;
import 'domain/test_result/i_test_result_facade.dart' as _i7;
import 'infrastructure/auth/auth_facade.dart' as _i14;
import 'infrastructure/core/dio_interceptor.dart' as _i12;
import 'infrastructure/core/dio_options.dart' as _i4;
import 'infrastructure/core/injectable_module.dart' as _i19;
import 'infrastructure/profile/profile_facade.dart' as _i16;
import 'infrastructure/storage/shared_pref.dart' as _i11;
import 'infrastructure/test_result/test_result_facade.dart' as _i8;
import 'presentation/widget/flushybar.dart'
    as _i6; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
Future<_i1.GetIt> $initGetIt(_i1.GetIt get,
    {String? environment, _i2.EnvironmentFilter? environmentFilter}) async {
  final gh = _i2.GetItHelper(get, environment, environmentFilter);
  final injectableModule = _$InjectableModule();
  gh.lazySingleton<_i3.Dio>(() => injectableModule.dio);
  gh.lazySingleton<_i4.DioOptions>(() => _i4.DioOptions(get<_i3.Dio>()));
  gh.lazySingleton<_i5.Flushbar<dynamic>>(() => injectableModule.flushbar);
  gh.lazySingleton<_i6.FlushyBar>(
      () => _i6.FlushyBar(get<_i5.Flushbar<dynamic>>()));
  gh.lazySingleton<_i7.ITestResultFacade>(() => _i8.TestResultFacade());
  await gh.factoryAsync<_i9.SharedPreferences>(() => injectableModule.prefs,
      preResolve: true);
  gh.factory<_i10.TestResultCubit>(
      () => _i10.TestResultCubit(get<_i7.ITestResultFacade>()));
  gh.lazySingleton<_i11.SharedPref>(
      () => _i11.SharedPref(get<_i9.SharedPreferences>()));
  gh.lazySingleton<_i12.CheckRequestErrorInterceptor>(
      () => _i12.CheckRequestErrorInterceptor(get<_i11.SharedPref>()));
  gh.lazySingleton<_i13.IAuthFacade>(
      () => _i14.AuthFacade(get<_i3.Dio>(), get<_i11.SharedPref>()));
  gh.lazySingleton<_i15.IProfileFacade>(
      () => _i16.ProfileFacade(get<_i3.Dio>(), get<_i11.SharedPref>()));
  gh.factory<_i17.AuthBloc>(() => _i17.AuthBloc(get<_i13.IAuthFacade>()));
  gh.factory<_i18.GetProfileBloc>(() =>
      _i18.GetProfileBloc(get<_i15.IProfileFacade>(), get<_i11.SharedPref>()));
  return get;
}

class _$InjectableModule extends _i19.InjectableModule {}
