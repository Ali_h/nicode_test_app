class Messages {
  static const String questionStepsAppbarTitle = 'تست شخصیت شناسی';
  static const String introMessage =
      'توی این برنامه میتونی با جواب دادنِ چندتا سوال خیلی ساده ببینی شخصیتت شبیه به کدوم یکی از آدمای معروفه\nیادت باشه که این برنامه فقط جنبه فان داره پس لبخند بزن :)';
  static const String notAuthenticatedErrorMessage = 'شما در برنامه لاگین نیستید';
}
