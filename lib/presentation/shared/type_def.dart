import 'package:flutter/material.dart';

typedef ListItemBuilder = Widget Function(int index);

typedef DiscountAndRefferalCodeChanged = void Function(
  String discount,
  String refferalCode,
);
