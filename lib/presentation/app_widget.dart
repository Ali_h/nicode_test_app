import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:nicode_charecter/presentation/global_provider_data.dart';
import 'package:nicode_charecter/presentation/page/splash/splash_page.dart';

class AppWidget extends StatelessWidget {
  const AppWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      builder: (_) => GlobalProviderData(
        child: MaterialApp(
          title: 'Nicode Test',
          debugShowCheckedModeBanner: false,
          theme: ThemeData(fontFamily: 'Bakh'),
          builder: (BuildContext context, Widget? child) {
            final MediaQueryData data = MediaQuery.of(context);
            return Directionality(
              textDirection: TextDirection.rtl,
              child: MediaQuery(
                data: data.copyWith(textScaleFactor: 1),
                child: child!,
              ),
            );
          },
          home: const SplashPage(),
        ),
      ),
    );
  }
}
