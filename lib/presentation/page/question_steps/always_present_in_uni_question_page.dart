import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nicode_charecter/application/question_steps/question_steps_bloc.dart';
import 'package:nicode_charecter/domain/question/entity/question_entity.dart';
import 'package:nicode_charecter/domain/question_steps/entity/question_steps_entity.dart';
import 'package:nicode_charecter/presentation/page/question_steps/widget/data_list/step_form_data_list.dart';
import 'package:nicode_charecter/presentation/page/question_steps/widget/data_list/step_form_data_list_item_with_icon.dart';
import 'package:nicode_charecter/presentation/page/question_steps/widget/step_scaffold.dart';
import 'package:nicode_charecter/presentation/page/test_result/test_result_page.dart';

import '../../../domain/question_steps/question_steps_state_manager.dart';

class AlwaysPresentInUniQuestionPage extends StatefulWidget {
  const AlwaysPresentInUniQuestionPage({Key? key}) : super(key: key);

  @override
  State<AlwaysPresentInUniQuestionPage> createState() => _AlwaysPresentInUniQuestionPageState();
}

class _AlwaysPresentInUniQuestionPageState extends State<AlwaysPresentInUniQuestionPage> {
  late final QuestionEntity data;

  @override
  void initState() {
    super.initState();

    data = QuestionEntity.alwaysPresentInUniQuestion();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<QuestionstepsBloc, QuestionStepsEntity>(
      buildWhen: (QuestionStepsEntity p, QuestionStepsEntity c) =>
          p.alwaysPresentInUni != c.alwaysPresentInUni,
      builder: (context, stepState) {
        return StepScaffold(
          formTitle: data.questionFullTitle,
          formWidget: StepFormDataList(
            dataLength: data.answers.length,
            itemBuilder: (index) {
              return StepFormDataListItemWithIcon(
                onItemTap: () => context.addAlwaysPresentInUniToQuestionStep(
                  questionShortTitle: data.questionShortTitle,
                  data: data.answers[index],
                ),
                goToPage: () => _goToNextPage(context),
                isSelected: stepState.alwaysPresentInUni == data.answers[index],
                text: data.answers[index].answersTypeMapper,
              );
            },
          ),
          stepperData: stepState.stepsData,
          onNextTap: stepState.alwaysPresentInUni != null
              ? () {
                  context.addAlwaysPresentInUniToQuestionStep(
                    questionShortTitle: data.questionShortTitle,
                    data: stepState.alwaysPresentInUni!,
                  );
                  _goToNextPage(context);
                }
              : null,
          onPreviousTap: () => context.removeAlwaysPresentInUniFromQuestionStep(),
          currentStepTitle: data.questionShortTitle,
        );
      },
    );
  }

  void _goToNextPage(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(builder: (_) => const TestResultPage()));
  }
}
