import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nicode_charecter/application/question_steps/question_steps_bloc.dart';
import 'package:nicode_charecter/domain/question/entity/question_entity.dart';
import 'package:nicode_charecter/domain/question_steps/entity/question_steps_entity.dart';
import 'package:nicode_charecter/presentation/page/question_steps/cheating_in_exam_question_page.dart';
import 'package:nicode_charecter/presentation/page/question_steps/widget/data_list/step_form_data_list.dart';
import 'package:nicode_charecter/presentation/page/question_steps/widget/data_list/step_form_data_list_item_with_icon.dart';
import 'package:nicode_charecter/presentation/page/question_steps/widget/step_scaffold.dart';

import '../../../domain/question_steps/question_steps_state_manager.dart';
import 'love_emotional_movie_question_page.dart';

class DrinkWaterQuestionPage extends StatefulWidget {
  const DrinkWaterQuestionPage({Key? key}) : super(key: key);

  @override
  State<DrinkWaterQuestionPage> createState() => _DrinkWaterQuestionPageState();
}

class _DrinkWaterQuestionPageState extends State<DrinkWaterQuestionPage> {
  late final QuestionEntity data;

  @override
  void initState() {
    super.initState();

    data = QuestionEntity.drinkWaterQuestion();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<QuestionstepsBloc, QuestionStepsEntity>(
      buildWhen: (QuestionStepsEntity p, QuestionStepsEntity c) =>
          p.drinkWaterWithMouth != c.drinkWaterWithMouth,
      builder: (context, stepState) {
        return StepScaffold(
          formTitle: data.questionFullTitle,
          formWidget: StepFormDataList(
            dataLength: data.answers.length,
            itemBuilder: (index) {
              return StepFormDataListItemWithIcon(
                onItemTap: () => context.addDrinkWaterWithMouthToQuestionStep(
                  questionShortTitle: data.questionShortTitle,
                  data: data.answers[index],
                ),
                goToPage: () => _goToNextPage(context),
                isSelected: stepState.drinkWaterWithMouth == data.answers[index],
                text: data.answers[index].answersTypeMapper,
              );
            },
          ),
          stepperData: stepState.stepsData,
          onNextTap: stepState.drinkWaterWithMouth != null
              ? () {
                  context.addDrinkWaterWithMouthToQuestionStep(
                    questionShortTitle: data.questionShortTitle,
                    data: stepState.drinkWaterWithMouth!,
                  );
                  _goToNextPage(context);
                }
              : null,
          onPreviousTap: () => context.initialState(),
          currentStepTitle: data.questionShortTitle,
        );
      },
    );
  }

  void _goToNextPage(BuildContext context) {
    if (context.drinkWaterWithMouth != null) {
      context.drinkWaterWithMouth?.map(
        yes: (_) => Navigator.of(context)
            .push(MaterialPageRoute(builder: (_) => const CheatingInExamQuestionPage())),
        no: (_) => Navigator.of(context)
            .push(MaterialPageRoute(builder: (_) => const LoveEmotionalMovieQuestionPage())),
      );
    }
  }
}
