import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nicode_charecter/application/question_steps/question_steps_bloc.dart';
import 'package:nicode_charecter/domain/question/entity/question_entity.dart';
import 'package:nicode_charecter/domain/question_steps/entity/question_steps_entity.dart';
import 'package:nicode_charecter/presentation/page/question_steps/widget/data_list/step_form_data_list.dart';
import 'package:nicode_charecter/presentation/page/question_steps/widget/data_list/step_form_data_list_item_with_icon.dart';
import 'package:nicode_charecter/presentation/page/question_steps/widget/step_scaffold.dart';
import 'package:nicode_charecter/presentation/page/test_result/test_result_page.dart';

import '../../../domain/question_steps/question_steps_state_manager.dart';

class SmokeCigarettesQuestionPage extends StatefulWidget {
  const SmokeCigarettesQuestionPage({Key? key}) : super(key: key);

  @override
  State<SmokeCigarettesQuestionPage> createState() => _SmokeCigarettesQuestionPageState();
}

class _SmokeCigarettesQuestionPageState extends State<SmokeCigarettesQuestionPage> {
  late final QuestionEntity data;

  @override
  void initState() {
    super.initState();

    data = QuestionEntity.smokeCigarettesQuestion();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<QuestionstepsBloc, QuestionStepsEntity>(
      buildWhen: (QuestionStepsEntity p, QuestionStepsEntity c) =>
          p.smokeCigarettes != c.smokeCigarettes,
      builder: (context, stepState) {
        return StepScaffold(
          formTitle: data.questionFullTitle,
          formWidget: StepFormDataList(
            dataLength: data.answers.length,
            itemBuilder: (index) {
              return StepFormDataListItemWithIcon(
                onItemTap: () => context.addSmokeCigarettesToQuestionStep(
                  questionShortTitle: data.questionShortTitle,
                  data: data.answers[index],
                ),
                goToPage: () => _goToNextPage(context),
                isSelected: stepState.smokeCigarettes == data.answers[index],
                text: data.answers[index].answersTypeMapper,
              );
            },
          ),
          stepperData: stepState.stepsData,
          onNextTap: stepState.smokeCigarettes != null
              ? () {
                  context.addSmokeCigarettesToQuestionStep(
                    questionShortTitle: data.questionShortTitle,
                    data: stepState.smokeCigarettes!,
                  );
                  _goToNextPage(context);
                }
              : null,
          onPreviousTap: () => context.removeSmokeCigarettesFromQuestionStep(),
          currentStepTitle: data.questionShortTitle,
        );
      },
    );
  }

  void _goToNextPage(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(builder: (_) => const TestResultPage()));
  }
}
