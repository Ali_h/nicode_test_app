import 'package:flutter/material.dart';
import 'package:nicode_charecter/presentation/page/question_steps/widget/step_next_and_previous_button.dart';
import 'package:nicode_charecter/presentation/page/question_steps/widget/step_title.dart';
import 'package:nicode_charecter/presentation/page/question_steps/widget/stepper/stepper_list.dart';
import 'package:nicode_charecter/presentation/shared/messages.dart';
import 'package:nicode_charecter/presentation/widget/custom_app_bar.dart';

class StepScaffold extends StatelessWidget {
  final String formTitle;
  final Widget formWidget;
  final VoidCallback? onNextTap;
  final VoidCallback onPreviousTap;
  final List<String> stepperData;
  final String currentStepTitle;

  const StepScaffold({
    Key? key,
    required this.stepperData,
    required this.currentStepTitle,
    required this.formTitle,
    required this.onNextTap,
    required this.onPreviousTap,
    required this.formWidget,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        onPreviousTap();
        return true;
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: const CustomAppBar(title: Messages.questionStepsAppbarTitle),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            StepperList(
              stepperData: stepperData,
              currentStepTitle: currentStepTitle,
            ),
            StepTitle(stepTitle: formTitle),
            Expanded(child: formWidget),
            StepNextAndPreviousButton(
              onNextTap: onNextTap,
              onPreviousTap: onPreviousTap,
            ),
          ],
        ),
      ),
    );
  }
}
