import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class StepItemPainter extends CustomPainter {
  final bool isFirstItem;
  final bool isLastItem;
  Path path = Path();
  final double triangleWidth = 12.0.w;
  final double roundedHeight = 4.h;
  final double roundedWidth = 3.w;

  StepItemPainter({
    this.isFirstItem = false,
    this.isLastItem = false,
  });

  @override
  void paint(Canvas canvas, Size size) {
    final Paint paint = Paint()
      ..color = Colors.grey.shade300
      ..shader = isLastItem
          ? ui.Gradient.linear(
              Offset.zero,
              Offset(size.width, size.height),
              [
                Colors.blue.shade900,
                Colors.blue,
              ],
            )
          : null;
    path.moveTo(size.width, 0);

    if (isFirstItem) {
      path.lineTo(size.width, size.height);
    } else {
      //create inner triangle
      path.lineTo(size.width - triangleWidth + roundedWidth, (size.height / 2) - roundedHeight);
      path.quadraticBezierTo(
        size.width - triangleWidth,
        size.height / 2,
        size.width - triangleWidth + roundedWidth,
        (size.height / 2) + roundedHeight,
      );
      path.lineTo(size.width, size.height);
    }

    path.lineTo(triangleWidth, size.height);

    //create outter triangle
    path.lineTo(roundedWidth, (size.height / 2) + roundedHeight);
    path.quadraticBezierTo(0, size.height / 2, roundedWidth, (size.height / 2) - roundedHeight);
    path.lineTo(triangleWidth, 0);

    path.lineTo(size.width, 0);

    //draw path
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => false;
}
