import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:nicode_charecter/presentation/page/question_steps/widget/stepper/step_item.dart';

class StepperList extends StatefulWidget {
  final List<String> stepperData;
  final String currentStepTitle;

  const StepperList({
    Key? key,
    required this.stepperData,
    required this.currentStepTitle,
  }) : super(key: key);

  @override
  State<StepperList> createState() => _StepperListState();
}

class _StepperListState extends State<StepperList> {
  late final ScrollController _scrollController;

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController();
    WidgetsBinding.instance?.addPostFrameCallback(
      (_) => _scrollController.jumpTo(
        _scrollController.position.maxScrollExtent,
      ),
    );
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 40.h,
      child: ListView.builder(
        padding: EdgeInsets.symmetric(vertical: 5.h, horizontal: 30.w),
        controller: _scrollController,
        itemCount: widget.stepperData.length + 1,
        physics: const BouncingScrollPhysics(),
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) {
          return StepItem(
            currentStepTitle: widget.currentStepTitle,
            stepperData: widget.stepperData,
            index: index,
          );
        },
      ),
    );
  }
}
