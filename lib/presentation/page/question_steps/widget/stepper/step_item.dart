import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:nicode_charecter/application/question_steps/question_steps_bloc.dart';
import 'package:nicode_charecter/presentation/page/question_steps/widget/stepper/step_item_painter.dart';

class StepItem extends StatelessWidget {
  final String currentStepTitle;
  final List<String> stepperData;
  final int index;

  const StepItem({
    Key? key,
    required this.currentStepTitle,
    required this.stepperData,
    required this.index,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: Colors.transparent,
      onTap: index != stepperData.length
          ? () {
              context.read<QuestionstepsBloc>().add(
                    QuestionstepsEvent.removeFromStepListWitnIndex(index: index),
                  );
              int count = 0;
              Navigator.popUntil(context, (route) {
                return count++ == stepperData.length - index;
              });
            }
          : null,
      child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(5.r)),
        child: CustomPaint(
          painter: StepItemPainter(
            isFirstItem: index == 0,
            isLastItem: index == stepperData.length,
          ),
          child: Center(
            child: Padding(
              padding: EdgeInsets.only(left: 13.w, right: index != 0 ? 20.w : 5.w),
              child: Text(
                index != stepperData.length ? stepperData[index] : currentStepTitle,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.bodyText2?.copyWith(
                      color: index == stepperData.length ? Colors.white : Colors.black,
                      fontWeight: FontWeight.bold,
                    ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
