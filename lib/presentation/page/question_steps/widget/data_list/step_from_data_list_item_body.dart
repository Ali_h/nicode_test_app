import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:nicode_charecter/presentation/widget/custom_button.dart';

class StepFormDataListItemBody extends StatefulWidget {
  final VoidCallback onItemTap;
  final VoidCallback goToPage;
  final Widget child;
  final bool isSelected;

  const StepFormDataListItemBody({
    Key? key,
    required this.onItemTap,
    required this.goToPage,
    required this.child,
    required this.isSelected,
  }) : super(key: key);

  @override
  State<StepFormDataListItemBody> createState() => _StepFormDataListItemBodyState();
}

class _StepFormDataListItemBodyState extends State<StepFormDataListItemBody> {
  bool active = true;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        bottom: 15.h,
        left: 30.w,
        right: 30.w,
      ),
      child: CustomButton(
        onSubmit: () async {
          if (active) {
            widget.onItemTap();
            active = false;
            await Future.delayed(
              const Duration(milliseconds: 70),
            );
            active = true;
            widget.goToPage();
          }
        },
        height: 60.h,
        buttonColor: widget.isSelected ? Colors.green : Colors.white,
        borderColor: widget.isSelected ? Colors.green.shade700 : Colors.black12,
        shadowColor: Colors.white24,
        elevation: 0,
        padding: EdgeInsets.zero,
        child: widget.child,
      ),
    );
  }
}
