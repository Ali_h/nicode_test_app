import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:nicode_charecter/presentation/page/question_steps/widget/data_list/step_form_data_list_item_text.dart';
import 'package:nicode_charecter/presentation/page/question_steps/widget/data_list/step_from_data_list_item_body.dart';

class StepFormDataListItem extends StatelessWidget {
  final String? data;
  final VoidCallback onItemTap;
  final VoidCallback goToPage;
  final bool isSelected;

  const StepFormDataListItem({
    Key? key,
    required this.data,
    required this.onItemTap,
    required this.goToPage,
    required this.isSelected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StepFormDataListItemBody(
      isSelected: isSelected,
      goToPage: goToPage,
      onItemTap: onItemTap,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 30.w),
        child: StepFormDataListItemText(
          text: data,
          isSelected: isSelected,
        ),
      ),
    );
  }
}
