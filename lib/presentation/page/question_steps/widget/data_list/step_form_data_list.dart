import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:nicode_charecter/presentation/shared/type_def.dart';
import 'package:nicode_charecter/presentation/widget/animated_listview_builder.dart';

class StepFormDataList extends StatelessWidget {
  final int dataLength;
  final ListItemBuilder itemBuilder;

  const StepFormDataList({
    Key? key,
    required this.dataLength,
    required this.itemBuilder,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scrollbar(
      child: AnimatedListviewBuilder(
        padding: EdgeInsets.only(bottom: 10.h),
        itemCount: dataLength,
        itemBuilder: itemBuilder,
      ),
    );
  }
}
