import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:nicode_charecter/presentation/page/question_steps/widget/data_list/step_form_data_list_item_text.dart';
import 'package:nicode_charecter/presentation/page/question_steps/widget/data_list/step_from_data_list_item_body.dart';
import 'package:nicode_charecter/presentation/widget/circle_check_box.dart';

class StepFormDataListItemWithIcon extends StatelessWidget {
  final String? text;
  final VoidCallback onItemTap;
  final VoidCallback goToPage;
  final bool isSelected;

  const StepFormDataListItemWithIcon({
    Key? key,
    required this.text,
    required this.onItemTap,
    required this.goToPage,
    required this.isSelected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StepFormDataListItemBody(
      isSelected: isSelected,
      goToPage: goToPage,
      onItemTap: onItemTap,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 15.w),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              flex: 8,
              child: Row(
                children: [
                  CircleCheckBox(isSelected: isSelected),
                  SizedBox(width: 10.w),
                  StepFormDataListItemText(
                    isSelected: isSelected,
                    text: text,
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
