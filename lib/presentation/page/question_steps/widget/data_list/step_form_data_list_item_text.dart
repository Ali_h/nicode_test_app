import 'package:flutter/material.dart';
import 'package:flutter_screenutil/src/size_extension.dart';

class StepFormDataListItemText extends StatelessWidget {
  final String? text;
  final bool isSelected;

  const StepFormDataListItemText({
    Key? key,
    required this.text,
    required this.isSelected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text ?? '-',
      style: TextStyle(color: isSelected ? Colors.white : Colors.black, fontSize: 17.sp),
    );
  }
}
