import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:nicode_charecter/presentation/widget/custom_button.dart';

class StepNextAndPreviousButton extends StatelessWidget {
  final VoidCallback? onNextTap;
  final VoidCallback? onPreviousTap;
  final bool? loading;
  final String? submitTitle;

  const StepNextAndPreviousButton({
    Key? key,
    required this.onNextTap,
    this.onPreviousTap,
    this.loading,
    this.submitTitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Padding(
        padding: EdgeInsets.only(left: 30.w, right: 30.w, top: 5.h, bottom: 30.h),
        child: Row(
          children: [
            Expanded(
              flex: 4,
              child: CustomButton(
                onSubmit: () {
                  if (onPreviousTap != null) {
                    onPreviousTap!();
                  }
                  Navigator.pop(context);
                },
                buttonColor: const Color(0xffdfe2ed),
                elevation: 0,
                childColor: Colors.black,
                height: 45.h,
                title: 'مرحله قبلی',
                textStyle: Theme.of(context).textTheme.bodyText2,
              ),
            ),
            SizedBox(width: 10.w),
            Expanded(
              flex: 6,
              child: CustomButton(
                onSubmit: onNextTap,
                loading: loading,
                buttonColor: Colors.blue.shade900,
                elevation: 0,
                childColor: Colors.white,
                height: 45.h,
                title: submitTitle ?? 'مرحله بعدی',
                textStyle: Theme.of(context).textTheme.bodyText2,
              ),
            )
          ],
        ),
      ),
    );
  }
}
