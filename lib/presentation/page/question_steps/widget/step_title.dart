import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class StepTitle extends StatelessWidget {
  final String stepTitle;

  const StepTitle({Key? key, required this.stepTitle}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        vertical: 20.h,
        horizontal: 30.w,
      ),
      child: Text(
        stepTitle,
        textAlign: TextAlign.center,
        style: Theme.of(context).textTheme.headline6?.copyWith(fontWeight: FontWeight.bold),
      ),
    );
  }
}
