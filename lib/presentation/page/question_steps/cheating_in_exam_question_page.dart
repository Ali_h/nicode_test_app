import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nicode_charecter/application/question_steps/question_steps_bloc.dart';
import 'package:nicode_charecter/domain/question/entity/question_entity.dart';
import 'package:nicode_charecter/domain/question_steps/entity/question_steps_entity.dart';
import 'package:nicode_charecter/presentation/page/question_steps/immortal_question_page.dart';
import 'package:nicode_charecter/presentation/page/question_steps/smoke_cigarettes_question_page.dart';
import 'package:nicode_charecter/presentation/page/question_steps/widget/data_list/step_form_data_list.dart';
import 'package:nicode_charecter/presentation/page/question_steps/widget/data_list/step_form_data_list_item_with_icon.dart';
import 'package:nicode_charecter/presentation/page/question_steps/widget/step_scaffold.dart';

import '../../../domain/question_steps/question_steps_state_manager.dart';

class CheatingInExamQuestionPage extends StatefulWidget {
  const CheatingInExamQuestionPage({Key? key}) : super(key: key);

  @override
  State<CheatingInExamQuestionPage> createState() => _CheatingInExamQuestionPageState();
}

class _CheatingInExamQuestionPageState extends State<CheatingInExamQuestionPage> {
  late final QuestionEntity data;

  @override
  void initState() {
    super.initState();

    data = QuestionEntity.cheatingInExamQuestion();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<QuestionstepsBloc, QuestionStepsEntity>(
      buildWhen: (QuestionStepsEntity p, QuestionStepsEntity c) =>
          p.cheatingInExam != c.cheatingInExam,
      builder: (context, stepState) {
        return StepScaffold(
          formTitle: data.questionFullTitle,
          formWidget: StepFormDataList(
            dataLength: data.answers.length,
            itemBuilder: (index) {
              return StepFormDataListItemWithIcon(
                onItemTap: () => context.addCheatingInExamToQuestionStep(
                  questionShortTitle: data.questionShortTitle,
                  data: data.answers[index],
                ),
                goToPage: () => _goToNextPage(context),
                isSelected: stepState.cheatingInExam == data.answers[index],
                text: data.answers[index].answersTypeMapper,
              );
            },
          ),
          stepperData: stepState.stepsData,
          onNextTap: stepState.cheatingInExam != null
              ? () {
                  context.addCheatingInExamToQuestionStep(
                    questionShortTitle: data.questionShortTitle,
                    data: stepState.cheatingInExam!,
                  );
                  _goToNextPage(context);
                }
              : null,
          onPreviousTap: () => context.removeCheatingInExamFromQuestionStep(),
          currentStepTitle: data.questionShortTitle,
        );
      },
    );
  }

  void _goToNextPage(BuildContext context) {
    if (context.cheatingInExam != null) {
      context.cheatingInExam?.map(
        yes: (_) => Navigator.of(context)
            .push(MaterialPageRoute(builder: (_) => const ImmortalQuestionPage())),
        no: (_) => Navigator.of(context)
            .push(MaterialPageRoute(builder: (_) => const SmokeCigarettesQuestionPage())),
      );
    }
  }
}
