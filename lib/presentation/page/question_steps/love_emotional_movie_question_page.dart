import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nicode_charecter/application/question_steps/question_steps_bloc.dart';
import 'package:nicode_charecter/domain/question/entity/question_entity.dart';
import 'package:nicode_charecter/domain/question_steps/entity/question_steps_entity.dart';
import 'package:nicode_charecter/presentation/page/question_steps/always_present_in_uni_question_page.dart';
import 'package:nicode_charecter/presentation/page/question_steps/cheating_in_exam_question_page.dart';
import 'package:nicode_charecter/presentation/page/question_steps/travel_without_permission_question_page.dart';
import 'package:nicode_charecter/presentation/page/question_steps/widget/data_list/step_form_data_list.dart';
import 'package:nicode_charecter/presentation/page/question_steps/widget/data_list/step_form_data_list_item_with_icon.dart';
import 'package:nicode_charecter/presentation/page/question_steps/widget/step_scaffold.dart';

import '../../../domain/question_steps/question_steps_state_manager.dart';

class LoveEmotionalMovieQuestionPage extends StatefulWidget {
  const LoveEmotionalMovieQuestionPage({Key? key}) : super(key: key);

  @override
  State<LoveEmotionalMovieQuestionPage> createState() => _LoveEmotionalMovieQuestionPageState();
}

class _LoveEmotionalMovieQuestionPageState extends State<LoveEmotionalMovieQuestionPage> {
  late final QuestionEntity data;

  @override
  void initState() {
    super.initState();

    data = QuestionEntity.loveTheEmotionalMovieQuestion();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<QuestionstepsBloc, QuestionStepsEntity>(
      buildWhen: (QuestionStepsEntity p, QuestionStepsEntity c) =>
          p.loveTheEmotionalMovie != c.loveTheEmotionalMovie,
      builder: (context, stepState) {
        return StepScaffold(
          formTitle: data.questionFullTitle,
          formWidget: StepFormDataList(
            dataLength: data.answers.length,
            itemBuilder: (index) {
              return StepFormDataListItemWithIcon(
                onItemTap: () => context.addLoveTheEmotionalMovieToQuestionStep(
                  questionShortTitle: data.questionShortTitle,
                  data: data.answers[index],
                ),
                goToPage: () => _goToNextPage(context),
                isSelected: stepState.loveTheEmotionalMovie == data.answers[index],
                text: data.answers[index].answersTypeMapper,
              );
            },
          ),
          stepperData: stepState.stepsData,
          onNextTap: stepState.loveTheEmotionalMovie != null
              ? () {
                  context.addLoveTheEmotionalMovieToQuestionStep(
                    questionShortTitle: data.questionShortTitle,
                    data: stepState.loveTheEmotionalMovie!,
                  );
                  _goToNextPage(context);
                }
              : null,
          onPreviousTap: () => context.removeLoveTheEmotionalMovieFromQuestionStep(),
          currentStepTitle: data.questionShortTitle,
        );
      },
    );
  }

  void _goToNextPage(BuildContext context) {
    if (context.loveTheEmotionalMovie != null) {
      context.loveTheEmotionalMovie?.map(
        yes: (_) => Navigator.of(context)
            .push(MaterialPageRoute(builder: (_) => const AlwaysPresentInUniQuestionPage())),
        no: (_) => Navigator.of(context)
            .push(MaterialPageRoute(builder: (_) => const TravelWithoutPermissionQuestionPage())),
      );
    }
  }
}
