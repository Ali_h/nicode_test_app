import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nicode_charecter/application/question_steps/question_steps_bloc.dart';
import 'package:nicode_charecter/domain/question/entity/question_entity.dart';
import 'package:nicode_charecter/domain/question_steps/entity/question_steps_entity.dart';
import 'package:nicode_charecter/presentation/page/question_steps/widget/data_list/step_form_data_list.dart';
import 'package:nicode_charecter/presentation/page/question_steps/widget/data_list/step_form_data_list_item_with_icon.dart';
import 'package:nicode_charecter/presentation/page/question_steps/widget/step_scaffold.dart';
import 'package:nicode_charecter/presentation/page/test_result/test_result_page.dart';

import '../../../domain/question_steps/question_steps_state_manager.dart';

class TravelWithoutPermissionQuestionPage extends StatefulWidget {
  const TravelWithoutPermissionQuestionPage({Key? key}) : super(key: key);

  @override
  State<TravelWithoutPermissionQuestionPage> createState() =>
      _TravelWithoutPermissionQuestionPageState();
}

class _TravelWithoutPermissionQuestionPageState extends State<TravelWithoutPermissionQuestionPage> {
  late final QuestionEntity data;

  @override
  void initState() {
    super.initState();

    data = QuestionEntity.travelWithoutPermissionQuestion();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<QuestionstepsBloc, QuestionStepsEntity>(
      buildWhen: (QuestionStepsEntity p, QuestionStepsEntity c) =>
          p.travelWithoutPermission != c.travelWithoutPermission,
      builder: (context, stepState) {
        return StepScaffold(
          formTitle: data.questionFullTitle,
          formWidget: StepFormDataList(
            dataLength: data.answers.length,
            itemBuilder: (index) {
              return StepFormDataListItemWithIcon(
                onItemTap: () => context.addTravelWithoutPermissionToQuestionStep(
                  questionShortTitle: data.questionShortTitle,
                  data: data.answers[index],
                ),
                goToPage: () => _goToNextPage(context),
                isSelected: stepState.travelWithoutPermission == data.answers[index],
                text: data.answers[index].answersTypeMapper,
              );
            },
          ),
          stepperData: stepState.stepsData,
          onNextTap: stepState.travelWithoutPermission != null
              ? () {
                  context.addTravelWithoutPermissionToQuestionStep(
                    questionShortTitle: data.questionShortTitle,
                    data: stepState.travelWithoutPermission!,
                  );
                  _goToNextPage(context);
                }
              : null,
          onPreviousTap: () => context.removeTravelWithoutPermissionFromQuestionStep(),
          currentStepTitle: data.questionShortTitle,
        );
      },
    );
  }

  void _goToNextPage(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(builder: (_) => const TestResultPage()));
  }
}
