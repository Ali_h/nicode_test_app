import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nicode_charecter/application/profile/get_profile_bloc.dart';
import 'package:nicode_charecter/application/shared/core_state.dart';
import 'package:nicode_charecter/domain/profile/entity/user_profile_entity.dart';
import 'package:nicode_charecter/presentation/page/intro/intro_page.dart';

import '../auth/login/login_page.dart';

class SplashPage extends StatelessWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocConsumer<GetProfileBloc, CoreState<UserProfileEntity>>(
        listener: (context, CoreState<UserProfileEntity> loginState) {
          loginState.maybeMap(
            orElse: () => null,
            fetchDataSuccessfully: (_) => Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (context) => const IntroPage(),
              ),
            ),
            fetchDataFailure: (_) => Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (context) => const LoginPage(),
              ),
            ),
          );
        },
        builder: (context, CoreState<UserProfileEntity> loginState) {
          return loginState.maybeMap(
            orElse: () => const SizedBox.shrink(),
            loadInProgress: (_) => const Center(child: CircularProgressIndicator()),
          );
        },
      ),
    );
  }
}
