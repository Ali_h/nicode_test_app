import 'package:flutter/material.dart';

class AuthScaffold extends StatelessWidget {
  final Widget body;

  const AuthScaffold({Key? key, required this.body}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: body,
    );
  }
}
