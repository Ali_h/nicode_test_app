import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AuthTitleAndSubtitle extends StatelessWidget {
  final String title;
  final String subtitle;

  const AuthTitleAndSubtitle({
    Key? key,
    required this.title,
    required this.subtitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: Theme.of(context).textTheme.headline4?.copyWith(
                color: Colors.black,
                fontWeight: FontWeight.bold,
              ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 15.h, bottom: 30.h),
          child: Text(
            subtitle,
            style: TextStyle(
              fontSize: 17.sp,
            ),
          ),
        )
      ],
    );
  }
}
