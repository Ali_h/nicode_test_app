import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class OtpCustomTextField extends StatelessWidget {
  final FocusNode focusNode;
  final TextEditingController controller;
  final int index;
  final int pinsNumber;
  final ValueChanged<String>? onSubmitted;
  final ValueChanged<String>? onChanged;

  const OtpCustomTextField({
    Key? key,
    required this.controller,
    required this.focusNode,
    required this.index,
    required this.pinsNumber,
    required this.onSubmitted,
    required this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextField(
      textAlign: TextAlign.center,
      style: TextStyle(
        color: Colors.black,
        fontSize: 18.sp,
      ),
      cursorColor: Colors.black,
      cursorRadius: const Radius.circular(50),
      inputFormatters: [
        FilteringTextInputFormatter.digitsOnly,
      ],
      maxLength: 1,
      focusNode: focusNode,
      controller: controller,
      textInputAction: index + 1 != pinsNumber ? TextInputAction.next : TextInputAction.go,
      onChanged: onChanged,
      onSubmitted: onSubmitted,
      keyboardType: const TextInputType.numberWithOptions(),
      decoration: InputDecoration(
        hintText: (index + 1).toString(),
        hintStyle: TextStyle(color: Colors.grey.shade400),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8.r),
          borderSide: const BorderSide(
            width: 1.5,
          ),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8.r),
          borderSide: const BorderSide(color: Colors.grey),
        ),
        contentPadding: EdgeInsets.symmetric(
          vertical: 5.h,
        ),
        counter: const SizedBox.shrink(),
        filled: true,
        fillColor: Colors.white,
        counterText: '',
      ),
    );
  }
}
