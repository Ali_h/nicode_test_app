import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:nicode_charecter/application/auth/auth_bloc.dart';
import 'package:nicode_charecter/application/auth/otp_code_timer/otp_code_timer_cubit.dart';

import './otp_custom_text_field.dart';

class OtpFiledContainer extends StatefulWidget {
  const OtpFiledContainer({Key? key}) : super(key: key);

  @override
  _OtpFiledContainerState createState() => _OtpFiledContainerState();
}

class _OtpFiledContainerState extends State<OtpFiledContainer> {
  final int pinsNumber = 6;
  late int emptyPinIndex;
  late List<Widget> _textFields;
  late List<String> _pinsCode;
  late List<FocusNode> _pinsFocus;
  late List<TextEditingController> _pinsController;

  @override
  void initState() {
    super.initState();

    _pinsController = List<TextEditingController>.generate(
      pinsNumber,
      (index) => TextEditingController(),
    );

    _pinsFocus = List<FocusNode>.generate(
      pinsNumber,
      (index) => FocusNode(),
    );

    _textFields = List.generate(
      pinsNumber,
      (index) => buildTextField(context, index),
    );

    _pinsCode = List<String>.generate(
      pinsNumber,
      (index) => '',
    );
  }

  @override
  void dispose() {
    for (final TextEditingController controller in _pinsController) {
      controller.dispose();
    }

    for (final FocusNode focusNode in _pinsFocus) {
      focusNode.dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<OtpCodeTimerCubit, int>(
      listener: (context, timerState) {
        if (timerState == 0) {
          for (final TextEditingController controller in _pinsController) {
            controller.clear();
          }
        }
      },
      child: Directionality(
        textDirection: TextDirection.ltr,
        child: Padding(
          padding: EdgeInsets.only(bottom: 15.h),
          child: Row(
            children: _textFields,
          ),
        ),
      ),
    );
  }

  Widget buildTextField(BuildContext context, int index) {
    return Expanded(
      child: Padding(
        padding: EdgeInsets.only(left: 5.w),
        child: OtpCustomTextField(
          controller: _pinsController[index],
          focusNode: _pinsFocus[index],
          index: index,
          pinsNumber: pinsNumber,
          onChanged: (String value) {
            _pinsCode[index] = value;
            final StringBuffer code = StringBuffer();
            for (final element in _pinsController) {
              code.write(element.text);
            }
            if (index + 1 != pinsNumber) {
              if (value.length == 1) {
                FocusScope.of(context).requestFocus(_pinsFocus[index + 1]);
              }
            }
            context.read<AuthBloc>().add(
                  AuthEvent.otpCodeChange(
                    otpCode: code.toString(),
                  ),
                );
          },
          onSubmitted: (value) {
            if ((index + 1) != pinsNumber) {
              _pinsFocus[index].unfocus();
              FocusScope.of(context).requestFocus(_pinsFocus[index + 1]);
            }
          },
        ),
      ),
    );
  }
}
