import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:nicode_charecter/application/auth/auth_bloc.dart';
import 'package:nicode_charecter/application/auth/otp_code_timer/otp_code_timer_cubit.dart';
import 'package:nicode_charecter/presentation/widget/custom_button.dart';

class OtpCodeTimer extends StatefulWidget {
  const OtpCodeTimer({
    Key? key,
  }) : super(key: key);

  @override
  _OtpCodeTimerState createState() => _OtpCodeTimerState();
}

class _OtpCodeTimerState extends State<OtpCodeTimer> {
  String timerStr = '00 : 00';
  int minute = 0;
  int second = 0;
  Timer? _timer;

  @override
  void initState() {
    super.initState();
    _startTimer();
  }

  @override
  void dispose() {
    _timer?.cancel();
    super.dispose();
  }

  void _startTimer() {
    if (_timer == null || !_timer!.isActive) {
      _timer = Timer.periodic(
        const Duration(seconds: 1),
        (Timer timer) {
          if (context.read<OtpCodeTimerCubit>().state == 0) {
            timerStr =
                '${second.toString().padLeft(2, '0')} : ${minute.toString().padLeft(2, '0')}';
            timer.cancel();
          } else {
            context.read<OtpCodeTimerCubit>().tick();
            minute = context.read<OtpCodeTimerCubit>().state ~/ 60;
            second = context.read<OtpCodeTimerCubit>().state - (minute * 60);
            timerStr =
                '${second.toString().padLeft(2, '0')} : ${minute.toString().padLeft(2, '0')}';
          }
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<OtpCodeTimerCubit, int>(
      buildWhen: (int p, int c) => p != c,
      listenWhen: (int p, int c) => c == context.read<OtpCodeTimerCubit>().initialTimerInSecond,
      listener: (context, int state) {
        if (state == context.read<OtpCodeTimerCubit>().initialTimerInSecond) {
          _startTimer();
        }
      },
      builder: (context, int duration) {
        return CustomButton(
          onSubmit: duration == 0
              ? () {
                  context.read<AuthBloc>().add(
                        const AuthEvent.otpCodeChange(otpCode: ''),
                      );
                  context.read<OtpCodeTimerCubit>().initialTimer();
                  context.read<AuthBloc>().add(
                        const AuthEvent.sendPhoneNumber(),
                      );
                }
              : () {},
          buttonColor: Colors.transparent,
          size: Size(1.sw, 45.h),
          elevation: 0,
          title: duration == 0 ? 'ارسال دوباره کد' : timerStr,
        );
      },
    );
  }
}
