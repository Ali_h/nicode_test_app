import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:nicode_charecter/application/auth/auth_bloc.dart';
import 'package:nicode_charecter/presentation/page/auth/widget/auth_title_and_subtitle.dart';
import 'package:nicode_charecter/presentation/widget/custom_button.dart';

import 'otp_code_timer.dart';
import 'otp_filed_container.dart';

class OtpPageBody extends StatelessWidget {
  const OtpPageBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: 25.w,
        vertical: 20.h,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          AuthTitleAndSubtitle(
            title: 'کد را وارد کنید',
            subtitle:
                'کد ارسالی به شماره ${context.read<AuthBloc>().state.phone.getOrElse('-')} را وارد کنید',
          ),
          Container(
            padding: EdgeInsets.only(left: 15.w, right: 15.w, top: 25.h, bottom: 20.h),
            decoration: BoxDecoration(
              color: Colors.grey.shade200,
              borderRadius: BorderRadius.circular(8.r),
            ),
            child: Column(
              children: [
                const OtpFiledContainer(),
                BlocBuilder<AuthBloc, AuthState>(
                  builder: (context, state) {
                    return CustomButton(
                      title: 'ارسال کد',
                      elevation: 0,
                      loading: state.isSubmitting,
                      buttonColor: Colors.blue.shade800,
                      childColor: Colors.white,
                      size: Size(1.sw, 50.h),
                      onSubmit: state.otpCode.isValid()
                          ? () => context.read<AuthBloc>().add(
                                const AuthEvent.sendOtpCode(),
                              )
                          : null,
                    );
                  },
                ),
                SizedBox(height: 10.h),
                const OtpCodeTimer(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
