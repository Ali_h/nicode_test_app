import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nicode_charecter/application/auth/auth_bloc.dart';
import 'package:nicode_charecter/application/auth/otp_code_timer/otp_code_timer_cubit.dart';
import 'package:nicode_charecter/application/profile/get_profile_bloc.dart';
import 'package:nicode_charecter/presentation/page/auth/otp/widget/otp_page_body.dart';
import 'package:nicode_charecter/presentation/page/auth/widget/auth_scaffold.dart';
import 'package:nicode_charecter/presentation/page/intro/intro_page.dart';
import 'package:nicode_charecter/presentation/widget/flushybar.dart';

import '../../../../injection.dart';

class OtpPage extends StatelessWidget {
  const OtpPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => OtpCodeTimerCubit(initialTimerInSecond: 120),
      child: BlocListener<AuthBloc, AuthState>(
        listener: (context, AuthState state) {
          state.otpFailureOrSuccessOption.fold(
            () {},
            (either) => either.fold(
              (failure) {
                getIt<FlushyBar>().showFlushyBar(
                  context: context,
                  title: failure.message,
                );
              },
              (_) {
                context.read<GetProfileBloc>().getUserProfile();
                context.read<AuthBloc>().add(const AuthEvent.initial());
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const IntroPage(),
                  ),
                );
              },
            ),
          );
        },
        child: const AuthScaffold(
          body: OtpPageBody(),
        ),
      ),
    );
  }
}
