import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:nicode_charecter/application/auth/auth_bloc.dart';
import 'package:nicode_charecter/presentation/page/auth/widget/auth_title_and_subtitle.dart';
import 'package:nicode_charecter/presentation/widget/custom_button.dart';
import 'package:nicode_charecter/presentation/widget/custom_form_text_field.dart';

class LoginBody extends StatelessWidget {
  const LoginBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: 25.w,
        vertical: 20.h,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const AuthTitleAndSubtitle(
            title: 'خوش آمدید',
            subtitle: 'برای استفاده لطفا شماره موبایل خود را وارد نمایید',
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 20.h),
            decoration: BoxDecoration(
              color: Colors.grey.shade200,
              borderRadius: BorderRadius.circular(5.r),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(bottom: 20.h),
                  child: Text(
                    'شماره تلفن همراه',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ),
                CustomFormTextField(
                  labelText: 'مثلا ۰۹۱۳۱۲۳۴۵۶۷',
                  maxLength: 13,
                  textInputType: TextInputType.phone,
                  autoFocus: true,
                  onSubmit: () => context.read<AuthBloc>().add(
                        const AuthEvent.sendPhoneNumber(),
                      ),
                  onChange: (phoneNumber) => context.read<AuthBloc>().add(
                        AuthEvent.phoneNumberChanged(phone: phoneNumber),
                      ),
                ),
                BlocBuilder<AuthBloc, AuthState>(
                  builder: (context, state) {
                    return CustomButton(
                      title: 'ارسال کد',
                      elevation: 0,
                      loading: state.isSubmitting,
                      buttonColor: Colors.blue.shade800,
                      childColor: Colors.white,
                      size: Size(1.sw, 50.h),
                      onSubmit: state.phone.isValid()
                          ? () => context.read<AuthBloc>().add(
                                const AuthEvent.sendPhoneNumber(),
                              )
                          : null,
                    );
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
