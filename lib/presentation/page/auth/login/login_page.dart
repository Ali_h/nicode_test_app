import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nicode_charecter/application/auth/auth_bloc.dart';
import 'package:nicode_charecter/presentation/page/auth/login/widget/login_body.dart';
import 'package:nicode_charecter/presentation/page/auth/otp/otp_page.dart';
import 'package:nicode_charecter/presentation/page/auth/widget/auth_scaffold.dart';
import 'package:nicode_charecter/presentation/widget/flushybar.dart';

import '../../../../injection.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthBloc, AuthState>(
      listener: (context, state) {
        state.authFailureOrSuccessOption.fold(
          () {},
          (either) => either.fold(
            (failure) {
              getIt<FlushyBar>().showFlushyBar(
                context: context,
                title: failure.message,
              );
            },
            (_) {
              Navigator.of(context).pushReplacement(
                MaterialPageRoute(
                  builder: (_) => const OtpPage(),
                ),
              );
            },
          ),
        );
      },
      child: const AuthScaffold(
        body: LoginBody(),
      ),
    );
  }
}
