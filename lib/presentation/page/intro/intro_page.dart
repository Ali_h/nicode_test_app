import 'package:flutter/material.dart';
import 'package:nicode_charecter/presentation/page/intro/widget/intro_body.dart';
import 'package:nicode_charecter/presentation/page/intro/widget/intro_bubble.dart';
import 'package:nicode_charecter/presentation/page/intro/widget/logout_button.dart';

class IntroPage extends StatelessWidget {
  const IntroPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: const [
          IntroBubble(),
          LogoutButton(),
          IntroBody(),
        ],
      ),
    );
  }
}
