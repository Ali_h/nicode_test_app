import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:nicode_charecter/presentation/page/question_steps/drink_water_question_page.dart';
import 'package:nicode_charecter/presentation/shared/messages.dart';
import 'package:nicode_charecter/presentation/widget/custom_button.dart';

class IntroBody extends StatelessWidget {
  const IntroBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 30.w),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            'assets/image/intro_icon.png',
            scale: 2,
          ),
          SizedBox(height: 20.h),
          Text(
            Messages.introMessage,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 17.sp,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(height: 30.h),
          CustomButton(
            onSubmit: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => const DrinkWaterQuestionPage(),
              ),
            ),
            title: 'شروع تست  🏃‍ ',
            buttonColor: Colors.black,
            width: 1.sw,
          )
        ],
      ),
    );
  }
}
