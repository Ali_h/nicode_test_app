import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class IntroBubble extends StatelessWidget {
  const IntroBubble({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: -125,
      right: -125,
      child: Container(
        height: 350.r,
        width: 350.r,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.red.withOpacity(0.9),
        ),
      ),
    );
  }
}
