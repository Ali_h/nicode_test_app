import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nicode_charecter/application/profile/get_profile_bloc.dart';
import 'package:nicode_charecter/presentation/page/splash/splash_page.dart';

class LogoutButton extends StatelessWidget {
  const LogoutButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 25.h, right: 5.w),
      child: Align(
        alignment: Alignment.topRight,
        child: IconButton(
          onPressed: () {
            context.read<GetProfileBloc>().logoutUser();
            Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => const SplashPage(),
              ),
              (route) => false,
            );
          },
          icon: const Icon(Icons.logout),
        ),
      ),
    );
  }
}
