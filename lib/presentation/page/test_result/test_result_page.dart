import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:nicode_charecter/application/question_steps/question_steps_bloc.dart';
import 'package:nicode_charecter/application/shared/core_state.dart';
import 'package:nicode_charecter/application/test_result/test_result_cubit.dart';
import 'package:nicode_charecter/domain/question_steps/question_steps_state_manager.dart';
import 'package:nicode_charecter/domain/test_result/entity/test_result_entity.dart';
import 'package:nicode_charecter/injection.dart';
import 'package:nicode_charecter/presentation/page/test_result/widget/test_result_actions.dart';
import 'package:nicode_charecter/presentation/page/test_result/widget/test_result_description.dart';
import 'package:nicode_charecter/presentation/page/test_result/widget/test_result_loading.dart';
import 'package:nicode_charecter/presentation/page/test_result/widget/test_result_title.dart';

class TestResultPage extends StatelessWidget {
  const TestResultPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => getIt<TestResultCubit>()
        ..getRasultData(
          data: context.read<QuestionstepsBloc>().state,
        ),
      child: WillPopScope(
        onWillPop: () async {
          context.removeAnswerInStepDataList();
          return true;
        },
        child: Scaffold(
          body: BlocBuilder<TestResultCubit, CoreState<TestResultEntity>>(
            builder: (context, state) {
              return state.map(
                initial: (_) => const SizedBox.shrink(),
                fetchDataFailure: (_) => const SizedBox.shrink(),
                loadInProgress: (_) => const TestResultLoading(),
                fetchDataSuccessfully: (result) => Stack(
                  fit: StackFit.expand,
                  children: [
                    Image.asset(
                      result.data.image,
                      fit: BoxFit.fill,
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20.w),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          TestResultTitle(title: result.data.title),
                          TestResultDescription(description: result.data.description),
                          const TestResultActions(),
                        ],
                      ),
                    )
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
