import 'package:flutter/material.dart';
import 'package:flutter_screenutil/src/size_extension.dart';

class TestResultDescription extends StatelessWidget {
  final String description;

  const TestResultDescription({
    Key? key,
    required this.description,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      description,
      textAlign: TextAlign.center,
      style: TextStyle(
        color: Colors.white70,
        fontSize: 18.sp,
      ),
    );
  }
}
