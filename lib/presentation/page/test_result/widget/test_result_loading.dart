import 'package:flutter/material.dart';

class TestResultLoading extends StatelessWidget {
  const TestResultLoading({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: CircularProgressIndicator(
        backgroundColor: Colors.black,
        color: Colors.amber,
      ),
    );
  }
}
