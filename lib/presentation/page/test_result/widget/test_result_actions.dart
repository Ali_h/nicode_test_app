import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/src/size_extension.dart';
import 'package:nicode_charecter/application/question_steps/question_steps_bloc.dart';
import 'package:nicode_charecter/domain/question_steps/question_steps_state_manager.dart';
import 'package:nicode_charecter/presentation/page/intro/intro_page.dart';
import 'package:nicode_charecter/presentation/widget/custom_button.dart';

class TestResultActions extends StatelessWidget {
  const TestResultActions({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 20.h),
      child: Row(
        children: [
          Expanded(
            child: CustomButton(
              onSubmit: () {
                context.read<QuestionstepsBloc>().add(const QuestionstepsEvent.initial());
                Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const IntroPage(),
                  ),
                  (route) => false,
                );
              },
              title: 'تست مجدد',
              padding: EdgeInsets.zero,
              height: 45.h,
              buttonColor: Colors.black,
              borderColor: Colors.white12,
              textStyle:
                  Theme.of(context).textTheme.subtitle2?.copyWith(fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(width: 10.w),
          Expanded(
            child: CustomButton(
              onSubmit: () {
                showDialog(
                  context: context,
                  barrierColor: Colors.black87,
                  builder: (context) => Dialog(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: context.stepsData
                          .map(
                            (element) => Padding(
                              padding: EdgeInsets.only(top: 10.h),
                              child: Text(
                                element,
                                style: Theme.of(context).textTheme.bodyText1?.copyWith(
                                      fontWeight: FontWeight.bold,
                                    ),
                              ),
                            ),
                          )
                          .toList(),
                    ),
                  ),
                );
              },
              title: 'مشاهده جواب ها',
              padding: EdgeInsets.zero,
              height: 45.h,
              textStyle:
                  Theme.of(context).textTheme.subtitle2?.copyWith(fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),
    );
  }
}
