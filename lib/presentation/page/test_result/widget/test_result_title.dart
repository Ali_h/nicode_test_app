import 'package:flutter/material.dart';
import 'package:flutter_screenutil/src/size_extension.dart';

class TestResultTitle extends StatelessWidget {
  final String title;

  const TestResultTitle({
    Key? key,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      style: TextStyle(
        color: Colors.white,
        fontSize: 40.sp,
        fontWeight: FontWeight.bold,
      ),
    );
  }
}
