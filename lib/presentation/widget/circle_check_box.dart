import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CircleCheckBox extends StatelessWidget {
  final bool isSelected;

  const CircleCheckBox({
    Key? key,
    required this.isSelected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 22.h,
      width: 22.h,
      decoration: BoxDecoration(
        color: isSelected ? Colors.green : Colors.white,
        shape: BoxShape.circle,
        border: Border.all(
          color: isSelected ? Colors.green.shade800 : Colors.black,
        ),
      ),
      child: isSelected
          ? Center(
              child: Icon(
                Icons.check,
                size: 16.r,
                color: Colors.white,
              ),
            )
          : const SizedBox.shrink(),
    );
  }
}
