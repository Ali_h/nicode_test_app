import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

@lazySingleton
class FlushyBar {
  Flushbar _flushbar;

  FlushyBar(this._flushbar);

  void showFlushyBar({
    required BuildContext context,
    required String title,
    int showTimeInMilliSecond = 3000,
    bool isError = true,
  }) {
    if (_flushbar.isShowing()) {
      _flushbar.dismiss(context);
    }
    _flushbar = Flushbar(
      flushbarPosition: FlushbarPosition.TOP,
      blockBackgroundInteraction: true,
      duration: Duration(milliseconds: showTimeInMilliSecond),
      borderRadius: BorderRadius.circular(5.r),
      icon: Icon(
        isError ? Icons.error_outline_rounded : Icons.check_circle_rounded,
        color: Colors.white,
      ),
      backgroundColor: isError ? Colors.red.shade900 : Colors.green,
      dismissDirection: FlushbarDismissDirection.HORIZONTAL,
      margin: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
      animationDuration: const Duration(milliseconds: 500),
      messageText: Text(
        title,
        textAlign: TextAlign.right,
        style: Theme.of(context).textTheme.bodyText2?.copyWith(
              height: 1.5,
              color: Colors.white,
              fontWeight: FontWeight.w500,
            ),
      ),
    );
    _flushbar.show(context);
  }
}
