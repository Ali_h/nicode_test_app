import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:nicode_charecter/presentation/shared/type_def.dart';

class AnimatedListviewBuilder extends StatelessWidget {
  final int itemCount;
  final ListItemBuilder itemBuilder;
  final EdgeInsets? padding;
  final ScrollController? controller;

  const AnimatedListviewBuilder({
    Key? key,
    required this.itemCount,
    required this.itemBuilder,
    this.padding,
    this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimationLimiter(
      child: ListView.builder(
        controller: controller,
        physics: const BouncingScrollPhysics(),
        padding: padding ?? EdgeInsets.zero,
        itemCount: itemCount,
        itemBuilder: (BuildContext context, int index) {
          return AnimationConfiguration.staggeredList(
            position: index,
            duration: const Duration(milliseconds: 250),
            child: SlideAnimation(
              verticalOffset: 50.0.h,
              child: FadeInAnimation(
                child: itemBuilder(index),
              ),
            ),
          );
        },
      ),
    );
  }
}
