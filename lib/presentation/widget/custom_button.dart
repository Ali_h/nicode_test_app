import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CustomButton extends StatelessWidget {
  final VoidCallback? onSubmit;
  final String? title;
  final Widget? child;
  final bool? loading;
  final Color buttonColor;
  final Color disableButtonColor;
  final Color? childColor;
  final Color disableChildColor;
  final Color? borderColor;
  final Color? shadowColor;
  final TextStyle? textStyle;
  final EdgeInsets? padding;
  final double elevation;
  final double? radius;
  final double? height;
  final double? width;
  final Size? size;
  final Alignment? childAlignment;

  const CustomButton({
    Key? key,
    required this.onSubmit,
    this.title,
    this.child,
    this.loading,
    this.buttonColor = Colors.white,
    this.disableButtonColor = const Color(0xffdfe2ed),
    this.disableChildColor = Colors.grey,
    this.shadowColor,
    this.childColor,
    this.borderColor,
    this.textStyle,
    this.padding,
    this.elevation = 3,
    this.radius,
    this.size,
    this.height,
    this.width,
    this.childAlignment,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: onSubmit,
      style: TextButton.styleFrom(
        backgroundColor: onSubmit == null ? disableButtonColor : buttonColor,
        primary: onSubmit == null
            ? disableChildColor
            : childColor != null
                ? childColor
                : (buttonColor == Colors.blue.shade900 ||
                        buttonColor == Colors.blue ||
                        buttonColor == Colors.black)
                    ? Colors.white
                    : Colors.black,
        enableFeedback: true,
        elevation: elevation,
        padding: padding ?? EdgeInsets.all(12.r),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(radius != null ? radius!.r : 6.r),
        ),
        textStyle: textStyle ?? Theme.of(context).textTheme.headline6,
        fixedSize: height != null || width != null
            ? Size(width ?? double.infinity, height ?? double.infinity)
            : size ?? const Size(double.infinity, double.infinity),
        side: borderColor != null ? BorderSide(color: borderColor!) : null,
        shadowColor: shadowColor,
        alignment: childAlignment,
      ),
      child: loading == null
          ? child ?? FittedBox(child: Text(title!))
          : AnimatedSwitcher(
              duration: const Duration(milliseconds: 300),
              transitionBuilder: (Widget child, Animation<double> animation) {
                return ScaleTransition(
                  scale: animation,
                  child: child,
                );
              },
              child: loading!
                  ? SizedBox(
                      height: 15.r,
                      width: 15.r,
                      child: const CircularProgressIndicator(
                        backgroundColor: Colors.black,
                        color: Colors.white,
                      ),
                    )
                  : child ??
                      FittedBox(
                        child: Text(title!),
                      ),
            ),
    );
  }
}
