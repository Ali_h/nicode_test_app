import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

enum TextFieldSize { small, medium, big }

class CustomFormTextField extends StatelessWidget {
  final ValueChanged<String> onChange;
  final VoidCallback? onSubmit;
  final FormFieldValidator<String>? validator;
  final String labelText;
  final String? initialValue;
  final int? maxLine;
  final TextInputType textInputType;
  final FocusNode? currentFocusNode;
  final FocusNode? nextFocusNode;
  final int? maxLength;
  final bool? enabled;
  final bool autoFocus;
  final bool disableCounterText;
  final TextFieldSize size;
  final double bottomPadding;

  const CustomFormTextField({
    Key? key,
    required this.labelText,
    required this.onChange,
    this.validator,
    this.nextFocusNode,
    this.initialValue,
    this.currentFocusNode,
    this.maxLine,
    this.onSubmit,
    this.maxLength,
    this.textInputType = TextInputType.text,
    this.enabled,
    this.bottomPadding = 10,
    this.size = TextFieldSize.small,
    this.autoFocus = false,
    this.disableCounterText = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: bottomPadding.h),
      child: TextFormField(
        initialValue: initialValue,
        maxLines: maxLine ?? 1,
        maxLength: maxLength,
        style: TextStyle(
          fontSize: 17.sp,
          letterSpacing: 1.5,
        ),
        cursorColor: Colors.black,
        textAlign: TextAlign.right,
        validator: validator,
        enabled: enabled,
        autofocus: autoFocus,
        textDirection: TextDirection.rtl,
        autocorrect: false,
        focusNode: currentFocusNode,
        keyboardType: textInputType,
        onChanged: onChange,
        textInputAction: nextFocusNode == null ? TextInputAction.done : TextInputAction.next,
        onFieldSubmitted: (_) {
          if (nextFocusNode != null) {
            nextFocusNode!.requestFocus();
          } else {
            currentFocusNode?.unfocus();
            if (onSubmit != null) {
              onSubmit!();
            }
          }
        },
        inputFormatters: textInputType == TextInputType.number
            ? [
                FilteringTextInputFormatter.digitsOnly,
              ]
            : null,
        cursorRadius: const Radius.circular(50),
        decoration: InputDecoration(
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8.r),
            borderSide: const BorderSide(
              width: 1.5,
            ),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8.r),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8.r),
            borderSide: const BorderSide(color: Colors.red),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8.r),
            borderSide: const BorderSide(color: Colors.red, width: 1.5),
          ),
          disabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8.r),
          ),
          contentPadding: EdgeInsets.symmetric(
            horizontal: 15.w,
            vertical: _getTextFiledSize(),
          ),
          errorStyle: Theme.of(context).textTheme.bodyText2?.copyWith(color: Colors.red),
          errorMaxLines: 2,
          labelText: labelText,
          filled: true,
          fillColor: Colors.white,
          counter: disableCounterText ? const SizedBox.shrink() : null,
          labelStyle: Theme.of(context).textTheme.subtitle2?.copyWith(
                color: Colors.grey,
                fontWeight: FontWeight.w700,
              ),
        ),
      ),
    );
  }

  double _getTextFiledSize() {
    switch (size) {
      case TextFieldSize.small:
        return 13.h;
      case TextFieldSize.medium:
        return 20.h;
      case TextFieldSize.big:
        return 25.h;
    }
  }
}
