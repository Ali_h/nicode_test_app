import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nicode_charecter/application/auth/auth_bloc.dart';
import 'package:nicode_charecter/application/profile/get_profile_bloc.dart';
import 'package:nicode_charecter/application/question_steps/question_steps_bloc.dart';

import '../injection.dart';

class GlobalProviderData extends StatelessWidget {
  final Widget child;

  const GlobalProviderData({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          lazy: true,
          create: (context) => QuestionstepsBloc(),
        ),
        BlocProvider(
          lazy: true,
          create: (context) => getIt<AuthBloc>(),
        ),
        BlocProvider(
          lazy: false,
          create: (context) => getIt<GetProfileBloc>()..getUserProfile(),
        ),
      ],
      child: child,
    );
  }
}
