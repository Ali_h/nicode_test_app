// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'answers_type.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$AnswersTypeTearOff {
  const _$AnswersTypeTearOff();

  _Yes yes() {
    return const _Yes();
  }

  _No no() {
    return const _No();
  }
}

/// @nodoc
const $AnswersType = _$AnswersTypeTearOff();

/// @nodoc
mixin _$AnswersType {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() yes,
    required TResult Function() no,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? yes,
    TResult Function()? no,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? yes,
    TResult Function()? no,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Yes value) yes,
    required TResult Function(_No value) no,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Yes value)? yes,
    TResult Function(_No value)? no,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Yes value)? yes,
    TResult Function(_No value)? no,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AnswersTypeCopyWith<$Res> {
  factory $AnswersTypeCopyWith(
          AnswersType value, $Res Function(AnswersType) then) =
      _$AnswersTypeCopyWithImpl<$Res>;
}

/// @nodoc
class _$AnswersTypeCopyWithImpl<$Res> implements $AnswersTypeCopyWith<$Res> {
  _$AnswersTypeCopyWithImpl(this._value, this._then);

  final AnswersType _value;
  // ignore: unused_field
  final $Res Function(AnswersType) _then;
}

/// @nodoc
abstract class _$YesCopyWith<$Res> {
  factory _$YesCopyWith(_Yes value, $Res Function(_Yes) then) =
      __$YesCopyWithImpl<$Res>;
}

/// @nodoc
class __$YesCopyWithImpl<$Res> extends _$AnswersTypeCopyWithImpl<$Res>
    implements _$YesCopyWith<$Res> {
  __$YesCopyWithImpl(_Yes _value, $Res Function(_Yes) _then)
      : super(_value, (v) => _then(v as _Yes));

  @override
  _Yes get _value => super._value as _Yes;
}

/// @nodoc

class _$_Yes extends _Yes {
  const _$_Yes() : super._();

  @override
  String toString() {
    return 'AnswersType.yes()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _Yes);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() yes,
    required TResult Function() no,
  }) {
    return yes();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? yes,
    TResult Function()? no,
  }) {
    return yes?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? yes,
    TResult Function()? no,
    required TResult orElse(),
  }) {
    if (yes != null) {
      return yes();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Yes value) yes,
    required TResult Function(_No value) no,
  }) {
    return yes(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Yes value)? yes,
    TResult Function(_No value)? no,
  }) {
    return yes?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Yes value)? yes,
    TResult Function(_No value)? no,
    required TResult orElse(),
  }) {
    if (yes != null) {
      return yes(this);
    }
    return orElse();
  }
}

abstract class _Yes extends AnswersType {
  const factory _Yes() = _$_Yes;
  const _Yes._() : super._();
}

/// @nodoc
abstract class _$NoCopyWith<$Res> {
  factory _$NoCopyWith(_No value, $Res Function(_No) then) =
      __$NoCopyWithImpl<$Res>;
}

/// @nodoc
class __$NoCopyWithImpl<$Res> extends _$AnswersTypeCopyWithImpl<$Res>
    implements _$NoCopyWith<$Res> {
  __$NoCopyWithImpl(_No _value, $Res Function(_No) _then)
      : super(_value, (v) => _then(v as _No));

  @override
  _No get _value => super._value as _No;
}

/// @nodoc

class _$_No extends _No {
  const _$_No() : super._();

  @override
  String toString() {
    return 'AnswersType.no()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _No);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() yes,
    required TResult Function() no,
  }) {
    return no();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? yes,
    TResult Function()? no,
  }) {
    return no?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? yes,
    TResult Function()? no,
    required TResult orElse(),
  }) {
    if (no != null) {
      return no();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Yes value) yes,
    required TResult Function(_No value) no,
  }) {
    return no(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Yes value)? yes,
    TResult Function(_No value)? no,
  }) {
    return no?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Yes value)? yes,
    TResult Function(_No value)? no,
    required TResult orElse(),
  }) {
    if (no != null) {
      return no(this);
    }
    return orElse();
  }
}

abstract class _No extends AnswersType {
  const factory _No() = _$_No;
  const _No._() : super._();
}
