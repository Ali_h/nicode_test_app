import 'package:freezed_annotation/freezed_annotation.dart';

part 'answers_type.freezed.dart';

@freezed
class AnswersType with _$AnswersType {
  const AnswersType._();

  const factory AnswersType.yes() = _Yes;

  const factory AnswersType.no() = _No;

  String get answersTypeMapper => map(
        yes: (_) => 'بله',
        no: (_) => 'خیر',
      );
}
