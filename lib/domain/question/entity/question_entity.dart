import 'package:nicode_charecter/domain/question/type/answers_type.dart';

class QuestionEntity {
  final String questionFullTitle;
  final String questionShortTitle;
  final List<AnswersType> answers;

  QuestionEntity({
    required this.questionFullTitle,
    required this.questionShortTitle,
    required this.answers,
  });

  factory QuestionEntity.drinkWaterQuestion() {
    return QuestionEntity(
      answers: [
        const AnswersType.yes(),
        const AnswersType.no(),
      ],
      questionFullTitle: 'توی خونه جرات داری با دهن از تنگ آب بخوری 😱 ؟؟',
      questionShortTitle: 'آب خوردن با دهن',
    );
  }

  factory QuestionEntity.cheatingInExamQuestion() {
    return QuestionEntity(
      answers: [
        const AnswersType.yes(),
        const AnswersType.no(),
      ],
      questionFullTitle: 'تا حالا توی امتحانای دانشگاه تقلب کردی 😏 ؟؟',
      questionShortTitle: 'تقلب تو امتحان',
    );
  }

  factory QuestionEntity.loveTheEmotionalMovieQuestion() {
    return QuestionEntity(
      answers: [
        const AnswersType.yes(),
        const AnswersType.no(),
      ],
      questionFullTitle: 'فیلمای با ژانر عاطفی-احساسی رو دوست داری 😍 ؟؟',
      questionShortTitle: 'دیدن فیلم عاطفی',
    );
  }

  factory QuestionEntity.toBeImmortalQuestion() {
    return QuestionEntity(
      answers: [
        const AnswersType.yes(),
        const AnswersType.no(),
      ],
      questionFullTitle: 'دوست داری همیشه زنده باشی و هیچ وقت نمیری ☠️ ؟؟',
      questionShortTitle: 'نامیرا بودن',
    );
  }

  factory QuestionEntity.smokeCigarettesQuestion() {
    return QuestionEntity(
      answers: [
        const AnswersType.yes(),
        const AnswersType.no(),
      ],
      questionFullTitle: 'راستش رو بگو سیگار میکشی یا نه 🚬 ؟؟',
      questionShortTitle: 'سیگار کشیدن',
    );
  }

  factory QuestionEntity.alwaysPresentInUniQuestion() {
    return QuestionEntity(
      answers: [
        const AnswersType.yes(),
        const AnswersType.no(),
      ],
      questionFullTitle: 'سر تمام کلاسای دانشگاه همیشه حاظر بودی و جلو میشستی 🤓 ؟؟',
      questionShortTitle: 'همیشه حاظر تو کلاس',
    );
  }

  factory QuestionEntity.travelWithoutPermissionQuestion() {
    return QuestionEntity(
      answers: [
        const AnswersType.yes(),
        const AnswersType.no(),
      ],
      questionFullTitle: 'میتونی بدون اجازه از خونوادت با رفیقات بری مسافرت 🏕 ؟؟',
      questionShortTitle: 'مسافرت بدون اجازه',
    );
  }
}
