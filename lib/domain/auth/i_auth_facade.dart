import 'package:dartz/dartz.dart';
import 'package:nicode_charecter/domain/core/failure/failure.dart';
import 'package:nicode_charecter/domain/core/value_objects.dart';

abstract class IAuthFacade {
  Future<Either<Failure, Unit>> sendPhoneNumber({
    required PhoneNumber phoneNumber,
  });

  Future<Either<Failure, Unit>> sendOtpCode({
    required OtpCode otpCode,
    required PhoneNumber phoneNumber,
  });
}
