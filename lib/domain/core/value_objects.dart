import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';
import 'package:nicode_charecter/domain/core/value_validators.dart';
import 'package:nicode_charecter/presentation/shared/messages.dart';

import 'errors.dart';
import 'failure/failures.dart';

@immutable
abstract class ValueObject<T> {
  const ValueObject();

  Either<ValueFailure<String>, T> get value;

  T getOrCrash() {
    return value.fold((f) => throw UnexpectedValueError(f), id);
  }

  T getOrElse(T dflt) {
    return value.getOrElse(() => dflt);
  }

  Either<ValueFailure<dynamic>, Unit> get failureOrUnit {
    return value.fold(
      (l) => left(l),
      (r) => right(unit),
    );
  }

  bool isValid() {
    return value.isRight();
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
    return o is ValueObject<T> && o.value == value;
  }

  @override
  int get hashCode => value.hashCode;

  @override
  String toString() => 'Value($value)';
}

class PhoneNumber extends ValueObject<String> {
  @override
  final Either<ValueFailure<String>, String> value;

  factory PhoneNumber(String input) {
    return PhoneNumber._(
      phoneNumberValidation(phone: input),
    );
  }

  const PhoneNumber._(this.value);
}

class OtpCode extends ValueObject<String> {
  @override
  final Either<ValueFailure<String>, String> value;

  factory OtpCode(String input) {
    return OtpCode._(
      otpCodeValidation(code: input),
    );
  }

  const OtpCode._(this.value);
}
