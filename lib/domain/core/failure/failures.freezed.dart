// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'failures.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$ValueFailureTearOff {
  const _$ValueFailureTearOff();

  EmptyField<T> emptyField<T>({required T failedValue}) {
    return EmptyField<T>(
      failedValue: failedValue,
    );
  }

  ProvinceAndCityEmpty<T> provinceAndCityEmpty<T>({required T failedValue}) {
    return ProvinceAndCityEmpty<T>(
      failedValue: failedValue,
    );
  }

  ExceedingLength<T> exceedingLength<T>(
      {required T failedValue, required int max}) {
    return ExceedingLength<T>(
      failedValue: failedValue,
      max: max,
    );
  }

  InvalidPhoneNumber<T> invalidPhoneNumber<T>({required T failedValue}) {
    return InvalidPhoneNumber<T>(
      failedValue: failedValue,
    );
  }

  InvalidBirthDay<T> invalidBirthDay<T>({required T failedValue}) {
    return InvalidBirthDay<T>(
      failedValue: failedValue,
    );
  }

  InvalidEmail<T> invalidEmail<T>({required T failedValue}) {
    return InvalidEmail<T>(
      failedValue: failedValue,
    );
  }

  InvalidOtpCode<T> invalidOtpCode<T>({required T failedValue}) {
    return InvalidOtpCode<T>(
      failedValue: failedValue,
    );
  }

  InvalidNationalCodeAndPostalCode<T> invalidNationalCodeAndPostalCode<T>(
      {required T failedValue}) {
    return InvalidNationalCodeAndPostalCode<T>(
      failedValue: failedValue,
    );
  }
}

/// @nodoc
const $ValueFailure = _$ValueFailureTearOff();

/// @nodoc
mixin _$ValueFailure<T> {
  T get failedValue => throw _privateConstructorUsedError;

  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T failedValue) emptyField,
    required TResult Function(T failedValue) provinceAndCityEmpty,
    required TResult Function(T failedValue, int max) exceedingLength,
    required TResult Function(T failedValue) invalidPhoneNumber,
    required TResult Function(T failedValue) invalidBirthDay,
    required TResult Function(T failedValue) invalidEmail,
    required TResult Function(T failedValue) invalidOtpCode,
    required TResult Function(T failedValue) invalidNationalCodeAndPostalCode,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(T failedValue)? emptyField,
    TResult Function(T failedValue)? provinceAndCityEmpty,
    TResult Function(T failedValue, int max)? exceedingLength,
    TResult Function(T failedValue)? invalidPhoneNumber,
    TResult Function(T failedValue)? invalidBirthDay,
    TResult Function(T failedValue)? invalidEmail,
    TResult Function(T failedValue)? invalidOtpCode,
    TResult Function(T failedValue)? invalidNationalCodeAndPostalCode,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T failedValue)? emptyField,
    TResult Function(T failedValue)? provinceAndCityEmpty,
    TResult Function(T failedValue, int max)? exceedingLength,
    TResult Function(T failedValue)? invalidPhoneNumber,
    TResult Function(T failedValue)? invalidBirthDay,
    TResult Function(T failedValue)? invalidEmail,
    TResult Function(T failedValue)? invalidOtpCode,
    TResult Function(T failedValue)? invalidNationalCodeAndPostalCode,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(EmptyField<T> value) emptyField,
    required TResult Function(ProvinceAndCityEmpty<T> value)
        provinceAndCityEmpty,
    required TResult Function(ExceedingLength<T> value) exceedingLength,
    required TResult Function(InvalidPhoneNumber<T> value) invalidPhoneNumber,
    required TResult Function(InvalidBirthDay<T> value) invalidBirthDay,
    required TResult Function(InvalidEmail<T> value) invalidEmail,
    required TResult Function(InvalidOtpCode<T> value) invalidOtpCode,
    required TResult Function(InvalidNationalCodeAndPostalCode<T> value)
        invalidNationalCodeAndPostalCode,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(EmptyField<T> value)? emptyField,
    TResult Function(ProvinceAndCityEmpty<T> value)? provinceAndCityEmpty,
    TResult Function(ExceedingLength<T> value)? exceedingLength,
    TResult Function(InvalidPhoneNumber<T> value)? invalidPhoneNumber,
    TResult Function(InvalidBirthDay<T> value)? invalidBirthDay,
    TResult Function(InvalidEmail<T> value)? invalidEmail,
    TResult Function(InvalidOtpCode<T> value)? invalidOtpCode,
    TResult Function(InvalidNationalCodeAndPostalCode<T> value)?
        invalidNationalCodeAndPostalCode,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(EmptyField<T> value)? emptyField,
    TResult Function(ProvinceAndCityEmpty<T> value)? provinceAndCityEmpty,
    TResult Function(ExceedingLength<T> value)? exceedingLength,
    TResult Function(InvalidPhoneNumber<T> value)? invalidPhoneNumber,
    TResult Function(InvalidBirthDay<T> value)? invalidBirthDay,
    TResult Function(InvalidEmail<T> value)? invalidEmail,
    TResult Function(InvalidOtpCode<T> value)? invalidOtpCode,
    TResult Function(InvalidNationalCodeAndPostalCode<T> value)?
        invalidNationalCodeAndPostalCode,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ValueFailureCopyWith<T, ValueFailure<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ValueFailureCopyWith<T, $Res> {
  factory $ValueFailureCopyWith(
          ValueFailure<T> value, $Res Function(ValueFailure<T>) then) =
      _$ValueFailureCopyWithImpl<T, $Res>;
  $Res call({T failedValue});
}

/// @nodoc
class _$ValueFailureCopyWithImpl<T, $Res>
    implements $ValueFailureCopyWith<T, $Res> {
  _$ValueFailureCopyWithImpl(this._value, this._then);

  final ValueFailure<T> _value;
  // ignore: unused_field
  final $Res Function(ValueFailure<T>) _then;

  @override
  $Res call({
    Object? failedValue = freezed,
  }) {
    return _then(_value.copyWith(
      failedValue: failedValue == freezed
          ? _value.failedValue
          : failedValue // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc
abstract class $EmptyFieldCopyWith<T, $Res>
    implements $ValueFailureCopyWith<T, $Res> {
  factory $EmptyFieldCopyWith(
          EmptyField<T> value, $Res Function(EmptyField<T>) then) =
      _$EmptyFieldCopyWithImpl<T, $Res>;
  @override
  $Res call({T failedValue});
}

/// @nodoc
class _$EmptyFieldCopyWithImpl<T, $Res>
    extends _$ValueFailureCopyWithImpl<T, $Res>
    implements $EmptyFieldCopyWith<T, $Res> {
  _$EmptyFieldCopyWithImpl(
      EmptyField<T> _value, $Res Function(EmptyField<T>) _then)
      : super(_value, (v) => _then(v as EmptyField<T>));

  @override
  EmptyField<T> get _value => super._value as EmptyField<T>;

  @override
  $Res call({
    Object? failedValue = freezed,
  }) {
    return _then(EmptyField<T>(
      failedValue: failedValue == freezed
          ? _value.failedValue
          : failedValue // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$EmptyField<T> with DiagnosticableTreeMixin implements EmptyField<T> {
  const _$EmptyField({required this.failedValue});

  @override
  final T failedValue;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ValueFailure<$T>.emptyField(failedValue: $failedValue)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ValueFailure<$T>.emptyField'))
      ..add(DiagnosticsProperty('failedValue', failedValue));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is EmptyField<T> &&
            const DeepCollectionEquality()
                .equals(other.failedValue, failedValue));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(failedValue));

  @JsonKey(ignore: true)
  @override
  $EmptyFieldCopyWith<T, EmptyField<T>> get copyWith =>
      _$EmptyFieldCopyWithImpl<T, EmptyField<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T failedValue) emptyField,
    required TResult Function(T failedValue) provinceAndCityEmpty,
    required TResult Function(T failedValue, int max) exceedingLength,
    required TResult Function(T failedValue) invalidPhoneNumber,
    required TResult Function(T failedValue) invalidBirthDay,
    required TResult Function(T failedValue) invalidEmail,
    required TResult Function(T failedValue) invalidOtpCode,
    required TResult Function(T failedValue) invalidNationalCodeAndPostalCode,
  }) {
    return emptyField(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(T failedValue)? emptyField,
    TResult Function(T failedValue)? provinceAndCityEmpty,
    TResult Function(T failedValue, int max)? exceedingLength,
    TResult Function(T failedValue)? invalidPhoneNumber,
    TResult Function(T failedValue)? invalidBirthDay,
    TResult Function(T failedValue)? invalidEmail,
    TResult Function(T failedValue)? invalidOtpCode,
    TResult Function(T failedValue)? invalidNationalCodeAndPostalCode,
  }) {
    return emptyField?.call(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T failedValue)? emptyField,
    TResult Function(T failedValue)? provinceAndCityEmpty,
    TResult Function(T failedValue, int max)? exceedingLength,
    TResult Function(T failedValue)? invalidPhoneNumber,
    TResult Function(T failedValue)? invalidBirthDay,
    TResult Function(T failedValue)? invalidEmail,
    TResult Function(T failedValue)? invalidOtpCode,
    TResult Function(T failedValue)? invalidNationalCodeAndPostalCode,
    required TResult orElse(),
  }) {
    if (emptyField != null) {
      return emptyField(failedValue);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(EmptyField<T> value) emptyField,
    required TResult Function(ProvinceAndCityEmpty<T> value)
        provinceAndCityEmpty,
    required TResult Function(ExceedingLength<T> value) exceedingLength,
    required TResult Function(InvalidPhoneNumber<T> value) invalidPhoneNumber,
    required TResult Function(InvalidBirthDay<T> value) invalidBirthDay,
    required TResult Function(InvalidEmail<T> value) invalidEmail,
    required TResult Function(InvalidOtpCode<T> value) invalidOtpCode,
    required TResult Function(InvalidNationalCodeAndPostalCode<T> value)
        invalidNationalCodeAndPostalCode,
  }) {
    return emptyField(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(EmptyField<T> value)? emptyField,
    TResult Function(ProvinceAndCityEmpty<T> value)? provinceAndCityEmpty,
    TResult Function(ExceedingLength<T> value)? exceedingLength,
    TResult Function(InvalidPhoneNumber<T> value)? invalidPhoneNumber,
    TResult Function(InvalidBirthDay<T> value)? invalidBirthDay,
    TResult Function(InvalidEmail<T> value)? invalidEmail,
    TResult Function(InvalidOtpCode<T> value)? invalidOtpCode,
    TResult Function(InvalidNationalCodeAndPostalCode<T> value)?
        invalidNationalCodeAndPostalCode,
  }) {
    return emptyField?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(EmptyField<T> value)? emptyField,
    TResult Function(ProvinceAndCityEmpty<T> value)? provinceAndCityEmpty,
    TResult Function(ExceedingLength<T> value)? exceedingLength,
    TResult Function(InvalidPhoneNumber<T> value)? invalidPhoneNumber,
    TResult Function(InvalidBirthDay<T> value)? invalidBirthDay,
    TResult Function(InvalidEmail<T> value)? invalidEmail,
    TResult Function(InvalidOtpCode<T> value)? invalidOtpCode,
    TResult Function(InvalidNationalCodeAndPostalCode<T> value)?
        invalidNationalCodeAndPostalCode,
    required TResult orElse(),
  }) {
    if (emptyField != null) {
      return emptyField(this);
    }
    return orElse();
  }
}

abstract class EmptyField<T> implements ValueFailure<T> {
  const factory EmptyField({required T failedValue}) = _$EmptyField<T>;

  @override
  T get failedValue;
  @override
  @JsonKey(ignore: true)
  $EmptyFieldCopyWith<T, EmptyField<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProvinceAndCityEmptyCopyWith<T, $Res>
    implements $ValueFailureCopyWith<T, $Res> {
  factory $ProvinceAndCityEmptyCopyWith(ProvinceAndCityEmpty<T> value,
          $Res Function(ProvinceAndCityEmpty<T>) then) =
      _$ProvinceAndCityEmptyCopyWithImpl<T, $Res>;
  @override
  $Res call({T failedValue});
}

/// @nodoc
class _$ProvinceAndCityEmptyCopyWithImpl<T, $Res>
    extends _$ValueFailureCopyWithImpl<T, $Res>
    implements $ProvinceAndCityEmptyCopyWith<T, $Res> {
  _$ProvinceAndCityEmptyCopyWithImpl(ProvinceAndCityEmpty<T> _value,
      $Res Function(ProvinceAndCityEmpty<T>) _then)
      : super(_value, (v) => _then(v as ProvinceAndCityEmpty<T>));

  @override
  ProvinceAndCityEmpty<T> get _value => super._value as ProvinceAndCityEmpty<T>;

  @override
  $Res call({
    Object? failedValue = freezed,
  }) {
    return _then(ProvinceAndCityEmpty<T>(
      failedValue: failedValue == freezed
          ? _value.failedValue
          : failedValue // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$ProvinceAndCityEmpty<T>
    with DiagnosticableTreeMixin
    implements ProvinceAndCityEmpty<T> {
  const _$ProvinceAndCityEmpty({required this.failedValue});

  @override
  final T failedValue;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ValueFailure<$T>.provinceAndCityEmpty(failedValue: $failedValue)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(
          DiagnosticsProperty('type', 'ValueFailure<$T>.provinceAndCityEmpty'))
      ..add(DiagnosticsProperty('failedValue', failedValue));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is ProvinceAndCityEmpty<T> &&
            const DeepCollectionEquality()
                .equals(other.failedValue, failedValue));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(failedValue));

  @JsonKey(ignore: true)
  @override
  $ProvinceAndCityEmptyCopyWith<T, ProvinceAndCityEmpty<T>> get copyWith =>
      _$ProvinceAndCityEmptyCopyWithImpl<T, ProvinceAndCityEmpty<T>>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T failedValue) emptyField,
    required TResult Function(T failedValue) provinceAndCityEmpty,
    required TResult Function(T failedValue, int max) exceedingLength,
    required TResult Function(T failedValue) invalidPhoneNumber,
    required TResult Function(T failedValue) invalidBirthDay,
    required TResult Function(T failedValue) invalidEmail,
    required TResult Function(T failedValue) invalidOtpCode,
    required TResult Function(T failedValue) invalidNationalCodeAndPostalCode,
  }) {
    return provinceAndCityEmpty(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(T failedValue)? emptyField,
    TResult Function(T failedValue)? provinceAndCityEmpty,
    TResult Function(T failedValue, int max)? exceedingLength,
    TResult Function(T failedValue)? invalidPhoneNumber,
    TResult Function(T failedValue)? invalidBirthDay,
    TResult Function(T failedValue)? invalidEmail,
    TResult Function(T failedValue)? invalidOtpCode,
    TResult Function(T failedValue)? invalidNationalCodeAndPostalCode,
  }) {
    return provinceAndCityEmpty?.call(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T failedValue)? emptyField,
    TResult Function(T failedValue)? provinceAndCityEmpty,
    TResult Function(T failedValue, int max)? exceedingLength,
    TResult Function(T failedValue)? invalidPhoneNumber,
    TResult Function(T failedValue)? invalidBirthDay,
    TResult Function(T failedValue)? invalidEmail,
    TResult Function(T failedValue)? invalidOtpCode,
    TResult Function(T failedValue)? invalidNationalCodeAndPostalCode,
    required TResult orElse(),
  }) {
    if (provinceAndCityEmpty != null) {
      return provinceAndCityEmpty(failedValue);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(EmptyField<T> value) emptyField,
    required TResult Function(ProvinceAndCityEmpty<T> value)
        provinceAndCityEmpty,
    required TResult Function(ExceedingLength<T> value) exceedingLength,
    required TResult Function(InvalidPhoneNumber<T> value) invalidPhoneNumber,
    required TResult Function(InvalidBirthDay<T> value) invalidBirthDay,
    required TResult Function(InvalidEmail<T> value) invalidEmail,
    required TResult Function(InvalidOtpCode<T> value) invalidOtpCode,
    required TResult Function(InvalidNationalCodeAndPostalCode<T> value)
        invalidNationalCodeAndPostalCode,
  }) {
    return provinceAndCityEmpty(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(EmptyField<T> value)? emptyField,
    TResult Function(ProvinceAndCityEmpty<T> value)? provinceAndCityEmpty,
    TResult Function(ExceedingLength<T> value)? exceedingLength,
    TResult Function(InvalidPhoneNumber<T> value)? invalidPhoneNumber,
    TResult Function(InvalidBirthDay<T> value)? invalidBirthDay,
    TResult Function(InvalidEmail<T> value)? invalidEmail,
    TResult Function(InvalidOtpCode<T> value)? invalidOtpCode,
    TResult Function(InvalidNationalCodeAndPostalCode<T> value)?
        invalidNationalCodeAndPostalCode,
  }) {
    return provinceAndCityEmpty?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(EmptyField<T> value)? emptyField,
    TResult Function(ProvinceAndCityEmpty<T> value)? provinceAndCityEmpty,
    TResult Function(ExceedingLength<T> value)? exceedingLength,
    TResult Function(InvalidPhoneNumber<T> value)? invalidPhoneNumber,
    TResult Function(InvalidBirthDay<T> value)? invalidBirthDay,
    TResult Function(InvalidEmail<T> value)? invalidEmail,
    TResult Function(InvalidOtpCode<T> value)? invalidOtpCode,
    TResult Function(InvalidNationalCodeAndPostalCode<T> value)?
        invalidNationalCodeAndPostalCode,
    required TResult orElse(),
  }) {
    if (provinceAndCityEmpty != null) {
      return provinceAndCityEmpty(this);
    }
    return orElse();
  }
}

abstract class ProvinceAndCityEmpty<T> implements ValueFailure<T> {
  const factory ProvinceAndCityEmpty({required T failedValue}) =
      _$ProvinceAndCityEmpty<T>;

  @override
  T get failedValue;
  @override
  @JsonKey(ignore: true)
  $ProvinceAndCityEmptyCopyWith<T, ProvinceAndCityEmpty<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ExceedingLengthCopyWith<T, $Res>
    implements $ValueFailureCopyWith<T, $Res> {
  factory $ExceedingLengthCopyWith(
          ExceedingLength<T> value, $Res Function(ExceedingLength<T>) then) =
      _$ExceedingLengthCopyWithImpl<T, $Res>;
  @override
  $Res call({T failedValue, int max});
}

/// @nodoc
class _$ExceedingLengthCopyWithImpl<T, $Res>
    extends _$ValueFailureCopyWithImpl<T, $Res>
    implements $ExceedingLengthCopyWith<T, $Res> {
  _$ExceedingLengthCopyWithImpl(
      ExceedingLength<T> _value, $Res Function(ExceedingLength<T>) _then)
      : super(_value, (v) => _then(v as ExceedingLength<T>));

  @override
  ExceedingLength<T> get _value => super._value as ExceedingLength<T>;

  @override
  $Res call({
    Object? failedValue = freezed,
    Object? max = freezed,
  }) {
    return _then(ExceedingLength<T>(
      failedValue: failedValue == freezed
          ? _value.failedValue
          : failedValue // ignore: cast_nullable_to_non_nullable
              as T,
      max: max == freezed
          ? _value.max
          : max // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$ExceedingLength<T>
    with DiagnosticableTreeMixin
    implements ExceedingLength<T> {
  const _$ExceedingLength({required this.failedValue, required this.max});

  @override
  final T failedValue;
  @override
  final int max;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ValueFailure<$T>.exceedingLength(failedValue: $failedValue, max: $max)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ValueFailure<$T>.exceedingLength'))
      ..add(DiagnosticsProperty('failedValue', failedValue))
      ..add(DiagnosticsProperty('max', max));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is ExceedingLength<T> &&
            const DeepCollectionEquality()
                .equals(other.failedValue, failedValue) &&
            const DeepCollectionEquality().equals(other.max, max));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(failedValue),
      const DeepCollectionEquality().hash(max));

  @JsonKey(ignore: true)
  @override
  $ExceedingLengthCopyWith<T, ExceedingLength<T>> get copyWith =>
      _$ExceedingLengthCopyWithImpl<T, ExceedingLength<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T failedValue) emptyField,
    required TResult Function(T failedValue) provinceAndCityEmpty,
    required TResult Function(T failedValue, int max) exceedingLength,
    required TResult Function(T failedValue) invalidPhoneNumber,
    required TResult Function(T failedValue) invalidBirthDay,
    required TResult Function(T failedValue) invalidEmail,
    required TResult Function(T failedValue) invalidOtpCode,
    required TResult Function(T failedValue) invalidNationalCodeAndPostalCode,
  }) {
    return exceedingLength(failedValue, max);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(T failedValue)? emptyField,
    TResult Function(T failedValue)? provinceAndCityEmpty,
    TResult Function(T failedValue, int max)? exceedingLength,
    TResult Function(T failedValue)? invalidPhoneNumber,
    TResult Function(T failedValue)? invalidBirthDay,
    TResult Function(T failedValue)? invalidEmail,
    TResult Function(T failedValue)? invalidOtpCode,
    TResult Function(T failedValue)? invalidNationalCodeAndPostalCode,
  }) {
    return exceedingLength?.call(failedValue, max);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T failedValue)? emptyField,
    TResult Function(T failedValue)? provinceAndCityEmpty,
    TResult Function(T failedValue, int max)? exceedingLength,
    TResult Function(T failedValue)? invalidPhoneNumber,
    TResult Function(T failedValue)? invalidBirthDay,
    TResult Function(T failedValue)? invalidEmail,
    TResult Function(T failedValue)? invalidOtpCode,
    TResult Function(T failedValue)? invalidNationalCodeAndPostalCode,
    required TResult orElse(),
  }) {
    if (exceedingLength != null) {
      return exceedingLength(failedValue, max);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(EmptyField<T> value) emptyField,
    required TResult Function(ProvinceAndCityEmpty<T> value)
        provinceAndCityEmpty,
    required TResult Function(ExceedingLength<T> value) exceedingLength,
    required TResult Function(InvalidPhoneNumber<T> value) invalidPhoneNumber,
    required TResult Function(InvalidBirthDay<T> value) invalidBirthDay,
    required TResult Function(InvalidEmail<T> value) invalidEmail,
    required TResult Function(InvalidOtpCode<T> value) invalidOtpCode,
    required TResult Function(InvalidNationalCodeAndPostalCode<T> value)
        invalidNationalCodeAndPostalCode,
  }) {
    return exceedingLength(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(EmptyField<T> value)? emptyField,
    TResult Function(ProvinceAndCityEmpty<T> value)? provinceAndCityEmpty,
    TResult Function(ExceedingLength<T> value)? exceedingLength,
    TResult Function(InvalidPhoneNumber<T> value)? invalidPhoneNumber,
    TResult Function(InvalidBirthDay<T> value)? invalidBirthDay,
    TResult Function(InvalidEmail<T> value)? invalidEmail,
    TResult Function(InvalidOtpCode<T> value)? invalidOtpCode,
    TResult Function(InvalidNationalCodeAndPostalCode<T> value)?
        invalidNationalCodeAndPostalCode,
  }) {
    return exceedingLength?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(EmptyField<T> value)? emptyField,
    TResult Function(ProvinceAndCityEmpty<T> value)? provinceAndCityEmpty,
    TResult Function(ExceedingLength<T> value)? exceedingLength,
    TResult Function(InvalidPhoneNumber<T> value)? invalidPhoneNumber,
    TResult Function(InvalidBirthDay<T> value)? invalidBirthDay,
    TResult Function(InvalidEmail<T> value)? invalidEmail,
    TResult Function(InvalidOtpCode<T> value)? invalidOtpCode,
    TResult Function(InvalidNationalCodeAndPostalCode<T> value)?
        invalidNationalCodeAndPostalCode,
    required TResult orElse(),
  }) {
    if (exceedingLength != null) {
      return exceedingLength(this);
    }
    return orElse();
  }
}

abstract class ExceedingLength<T> implements ValueFailure<T> {
  const factory ExceedingLength({required T failedValue, required int max}) =
      _$ExceedingLength<T>;

  @override
  T get failedValue;
  int get max;
  @override
  @JsonKey(ignore: true)
  $ExceedingLengthCopyWith<T, ExceedingLength<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $InvalidPhoneNumberCopyWith<T, $Res>
    implements $ValueFailureCopyWith<T, $Res> {
  factory $InvalidPhoneNumberCopyWith(InvalidPhoneNumber<T> value,
          $Res Function(InvalidPhoneNumber<T>) then) =
      _$InvalidPhoneNumberCopyWithImpl<T, $Res>;
  @override
  $Res call({T failedValue});
}

/// @nodoc
class _$InvalidPhoneNumberCopyWithImpl<T, $Res>
    extends _$ValueFailureCopyWithImpl<T, $Res>
    implements $InvalidPhoneNumberCopyWith<T, $Res> {
  _$InvalidPhoneNumberCopyWithImpl(
      InvalidPhoneNumber<T> _value, $Res Function(InvalidPhoneNumber<T>) _then)
      : super(_value, (v) => _then(v as InvalidPhoneNumber<T>));

  @override
  InvalidPhoneNumber<T> get _value => super._value as InvalidPhoneNumber<T>;

  @override
  $Res call({
    Object? failedValue = freezed,
  }) {
    return _then(InvalidPhoneNumber<T>(
      failedValue: failedValue == freezed
          ? _value.failedValue
          : failedValue // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$InvalidPhoneNumber<T>
    with DiagnosticableTreeMixin
    implements InvalidPhoneNumber<T> {
  const _$InvalidPhoneNumber({required this.failedValue});

  @override
  final T failedValue;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ValueFailure<$T>.invalidPhoneNumber(failedValue: $failedValue)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ValueFailure<$T>.invalidPhoneNumber'))
      ..add(DiagnosticsProperty('failedValue', failedValue));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is InvalidPhoneNumber<T> &&
            const DeepCollectionEquality()
                .equals(other.failedValue, failedValue));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(failedValue));

  @JsonKey(ignore: true)
  @override
  $InvalidPhoneNumberCopyWith<T, InvalidPhoneNumber<T>> get copyWith =>
      _$InvalidPhoneNumberCopyWithImpl<T, InvalidPhoneNumber<T>>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T failedValue) emptyField,
    required TResult Function(T failedValue) provinceAndCityEmpty,
    required TResult Function(T failedValue, int max) exceedingLength,
    required TResult Function(T failedValue) invalidPhoneNumber,
    required TResult Function(T failedValue) invalidBirthDay,
    required TResult Function(T failedValue) invalidEmail,
    required TResult Function(T failedValue) invalidOtpCode,
    required TResult Function(T failedValue) invalidNationalCodeAndPostalCode,
  }) {
    return invalidPhoneNumber(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(T failedValue)? emptyField,
    TResult Function(T failedValue)? provinceAndCityEmpty,
    TResult Function(T failedValue, int max)? exceedingLength,
    TResult Function(T failedValue)? invalidPhoneNumber,
    TResult Function(T failedValue)? invalidBirthDay,
    TResult Function(T failedValue)? invalidEmail,
    TResult Function(T failedValue)? invalidOtpCode,
    TResult Function(T failedValue)? invalidNationalCodeAndPostalCode,
  }) {
    return invalidPhoneNumber?.call(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T failedValue)? emptyField,
    TResult Function(T failedValue)? provinceAndCityEmpty,
    TResult Function(T failedValue, int max)? exceedingLength,
    TResult Function(T failedValue)? invalidPhoneNumber,
    TResult Function(T failedValue)? invalidBirthDay,
    TResult Function(T failedValue)? invalidEmail,
    TResult Function(T failedValue)? invalidOtpCode,
    TResult Function(T failedValue)? invalidNationalCodeAndPostalCode,
    required TResult orElse(),
  }) {
    if (invalidPhoneNumber != null) {
      return invalidPhoneNumber(failedValue);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(EmptyField<T> value) emptyField,
    required TResult Function(ProvinceAndCityEmpty<T> value)
        provinceAndCityEmpty,
    required TResult Function(ExceedingLength<T> value) exceedingLength,
    required TResult Function(InvalidPhoneNumber<T> value) invalidPhoneNumber,
    required TResult Function(InvalidBirthDay<T> value) invalidBirthDay,
    required TResult Function(InvalidEmail<T> value) invalidEmail,
    required TResult Function(InvalidOtpCode<T> value) invalidOtpCode,
    required TResult Function(InvalidNationalCodeAndPostalCode<T> value)
        invalidNationalCodeAndPostalCode,
  }) {
    return invalidPhoneNumber(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(EmptyField<T> value)? emptyField,
    TResult Function(ProvinceAndCityEmpty<T> value)? provinceAndCityEmpty,
    TResult Function(ExceedingLength<T> value)? exceedingLength,
    TResult Function(InvalidPhoneNumber<T> value)? invalidPhoneNumber,
    TResult Function(InvalidBirthDay<T> value)? invalidBirthDay,
    TResult Function(InvalidEmail<T> value)? invalidEmail,
    TResult Function(InvalidOtpCode<T> value)? invalidOtpCode,
    TResult Function(InvalidNationalCodeAndPostalCode<T> value)?
        invalidNationalCodeAndPostalCode,
  }) {
    return invalidPhoneNumber?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(EmptyField<T> value)? emptyField,
    TResult Function(ProvinceAndCityEmpty<T> value)? provinceAndCityEmpty,
    TResult Function(ExceedingLength<T> value)? exceedingLength,
    TResult Function(InvalidPhoneNumber<T> value)? invalidPhoneNumber,
    TResult Function(InvalidBirthDay<T> value)? invalidBirthDay,
    TResult Function(InvalidEmail<T> value)? invalidEmail,
    TResult Function(InvalidOtpCode<T> value)? invalidOtpCode,
    TResult Function(InvalidNationalCodeAndPostalCode<T> value)?
        invalidNationalCodeAndPostalCode,
    required TResult orElse(),
  }) {
    if (invalidPhoneNumber != null) {
      return invalidPhoneNumber(this);
    }
    return orElse();
  }
}

abstract class InvalidPhoneNumber<T> implements ValueFailure<T> {
  const factory InvalidPhoneNumber({required T failedValue}) =
      _$InvalidPhoneNumber<T>;

  @override
  T get failedValue;
  @override
  @JsonKey(ignore: true)
  $InvalidPhoneNumberCopyWith<T, InvalidPhoneNumber<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $InvalidBirthDayCopyWith<T, $Res>
    implements $ValueFailureCopyWith<T, $Res> {
  factory $InvalidBirthDayCopyWith(
          InvalidBirthDay<T> value, $Res Function(InvalidBirthDay<T>) then) =
      _$InvalidBirthDayCopyWithImpl<T, $Res>;
  @override
  $Res call({T failedValue});
}

/// @nodoc
class _$InvalidBirthDayCopyWithImpl<T, $Res>
    extends _$ValueFailureCopyWithImpl<T, $Res>
    implements $InvalidBirthDayCopyWith<T, $Res> {
  _$InvalidBirthDayCopyWithImpl(
      InvalidBirthDay<T> _value, $Res Function(InvalidBirthDay<T>) _then)
      : super(_value, (v) => _then(v as InvalidBirthDay<T>));

  @override
  InvalidBirthDay<T> get _value => super._value as InvalidBirthDay<T>;

  @override
  $Res call({
    Object? failedValue = freezed,
  }) {
    return _then(InvalidBirthDay<T>(
      failedValue: failedValue == freezed
          ? _value.failedValue
          : failedValue // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$InvalidBirthDay<T>
    with DiagnosticableTreeMixin
    implements InvalidBirthDay<T> {
  const _$InvalidBirthDay({required this.failedValue});

  @override
  final T failedValue;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ValueFailure<$T>.invalidBirthDay(failedValue: $failedValue)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ValueFailure<$T>.invalidBirthDay'))
      ..add(DiagnosticsProperty('failedValue', failedValue));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is InvalidBirthDay<T> &&
            const DeepCollectionEquality()
                .equals(other.failedValue, failedValue));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(failedValue));

  @JsonKey(ignore: true)
  @override
  $InvalidBirthDayCopyWith<T, InvalidBirthDay<T>> get copyWith =>
      _$InvalidBirthDayCopyWithImpl<T, InvalidBirthDay<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T failedValue) emptyField,
    required TResult Function(T failedValue) provinceAndCityEmpty,
    required TResult Function(T failedValue, int max) exceedingLength,
    required TResult Function(T failedValue) invalidPhoneNumber,
    required TResult Function(T failedValue) invalidBirthDay,
    required TResult Function(T failedValue) invalidEmail,
    required TResult Function(T failedValue) invalidOtpCode,
    required TResult Function(T failedValue) invalidNationalCodeAndPostalCode,
  }) {
    return invalidBirthDay(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(T failedValue)? emptyField,
    TResult Function(T failedValue)? provinceAndCityEmpty,
    TResult Function(T failedValue, int max)? exceedingLength,
    TResult Function(T failedValue)? invalidPhoneNumber,
    TResult Function(T failedValue)? invalidBirthDay,
    TResult Function(T failedValue)? invalidEmail,
    TResult Function(T failedValue)? invalidOtpCode,
    TResult Function(T failedValue)? invalidNationalCodeAndPostalCode,
  }) {
    return invalidBirthDay?.call(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T failedValue)? emptyField,
    TResult Function(T failedValue)? provinceAndCityEmpty,
    TResult Function(T failedValue, int max)? exceedingLength,
    TResult Function(T failedValue)? invalidPhoneNumber,
    TResult Function(T failedValue)? invalidBirthDay,
    TResult Function(T failedValue)? invalidEmail,
    TResult Function(T failedValue)? invalidOtpCode,
    TResult Function(T failedValue)? invalidNationalCodeAndPostalCode,
    required TResult orElse(),
  }) {
    if (invalidBirthDay != null) {
      return invalidBirthDay(failedValue);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(EmptyField<T> value) emptyField,
    required TResult Function(ProvinceAndCityEmpty<T> value)
        provinceAndCityEmpty,
    required TResult Function(ExceedingLength<T> value) exceedingLength,
    required TResult Function(InvalidPhoneNumber<T> value) invalidPhoneNumber,
    required TResult Function(InvalidBirthDay<T> value) invalidBirthDay,
    required TResult Function(InvalidEmail<T> value) invalidEmail,
    required TResult Function(InvalidOtpCode<T> value) invalidOtpCode,
    required TResult Function(InvalidNationalCodeAndPostalCode<T> value)
        invalidNationalCodeAndPostalCode,
  }) {
    return invalidBirthDay(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(EmptyField<T> value)? emptyField,
    TResult Function(ProvinceAndCityEmpty<T> value)? provinceAndCityEmpty,
    TResult Function(ExceedingLength<T> value)? exceedingLength,
    TResult Function(InvalidPhoneNumber<T> value)? invalidPhoneNumber,
    TResult Function(InvalidBirthDay<T> value)? invalidBirthDay,
    TResult Function(InvalidEmail<T> value)? invalidEmail,
    TResult Function(InvalidOtpCode<T> value)? invalidOtpCode,
    TResult Function(InvalidNationalCodeAndPostalCode<T> value)?
        invalidNationalCodeAndPostalCode,
  }) {
    return invalidBirthDay?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(EmptyField<T> value)? emptyField,
    TResult Function(ProvinceAndCityEmpty<T> value)? provinceAndCityEmpty,
    TResult Function(ExceedingLength<T> value)? exceedingLength,
    TResult Function(InvalidPhoneNumber<T> value)? invalidPhoneNumber,
    TResult Function(InvalidBirthDay<T> value)? invalidBirthDay,
    TResult Function(InvalidEmail<T> value)? invalidEmail,
    TResult Function(InvalidOtpCode<T> value)? invalidOtpCode,
    TResult Function(InvalidNationalCodeAndPostalCode<T> value)?
        invalidNationalCodeAndPostalCode,
    required TResult orElse(),
  }) {
    if (invalidBirthDay != null) {
      return invalidBirthDay(this);
    }
    return orElse();
  }
}

abstract class InvalidBirthDay<T> implements ValueFailure<T> {
  const factory InvalidBirthDay({required T failedValue}) =
      _$InvalidBirthDay<T>;

  @override
  T get failedValue;
  @override
  @JsonKey(ignore: true)
  $InvalidBirthDayCopyWith<T, InvalidBirthDay<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $InvalidEmailCopyWith<T, $Res>
    implements $ValueFailureCopyWith<T, $Res> {
  factory $InvalidEmailCopyWith(
          InvalidEmail<T> value, $Res Function(InvalidEmail<T>) then) =
      _$InvalidEmailCopyWithImpl<T, $Res>;
  @override
  $Res call({T failedValue});
}

/// @nodoc
class _$InvalidEmailCopyWithImpl<T, $Res>
    extends _$ValueFailureCopyWithImpl<T, $Res>
    implements $InvalidEmailCopyWith<T, $Res> {
  _$InvalidEmailCopyWithImpl(
      InvalidEmail<T> _value, $Res Function(InvalidEmail<T>) _then)
      : super(_value, (v) => _then(v as InvalidEmail<T>));

  @override
  InvalidEmail<T> get _value => super._value as InvalidEmail<T>;

  @override
  $Res call({
    Object? failedValue = freezed,
  }) {
    return _then(InvalidEmail<T>(
      failedValue: failedValue == freezed
          ? _value.failedValue
          : failedValue // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$InvalidEmail<T>
    with DiagnosticableTreeMixin
    implements InvalidEmail<T> {
  const _$InvalidEmail({required this.failedValue});

  @override
  final T failedValue;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ValueFailure<$T>.invalidEmail(failedValue: $failedValue)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ValueFailure<$T>.invalidEmail'))
      ..add(DiagnosticsProperty('failedValue', failedValue));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is InvalidEmail<T> &&
            const DeepCollectionEquality()
                .equals(other.failedValue, failedValue));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(failedValue));

  @JsonKey(ignore: true)
  @override
  $InvalidEmailCopyWith<T, InvalidEmail<T>> get copyWith =>
      _$InvalidEmailCopyWithImpl<T, InvalidEmail<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T failedValue) emptyField,
    required TResult Function(T failedValue) provinceAndCityEmpty,
    required TResult Function(T failedValue, int max) exceedingLength,
    required TResult Function(T failedValue) invalidPhoneNumber,
    required TResult Function(T failedValue) invalidBirthDay,
    required TResult Function(T failedValue) invalidEmail,
    required TResult Function(T failedValue) invalidOtpCode,
    required TResult Function(T failedValue) invalidNationalCodeAndPostalCode,
  }) {
    return invalidEmail(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(T failedValue)? emptyField,
    TResult Function(T failedValue)? provinceAndCityEmpty,
    TResult Function(T failedValue, int max)? exceedingLength,
    TResult Function(T failedValue)? invalidPhoneNumber,
    TResult Function(T failedValue)? invalidBirthDay,
    TResult Function(T failedValue)? invalidEmail,
    TResult Function(T failedValue)? invalidOtpCode,
    TResult Function(T failedValue)? invalidNationalCodeAndPostalCode,
  }) {
    return invalidEmail?.call(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T failedValue)? emptyField,
    TResult Function(T failedValue)? provinceAndCityEmpty,
    TResult Function(T failedValue, int max)? exceedingLength,
    TResult Function(T failedValue)? invalidPhoneNumber,
    TResult Function(T failedValue)? invalidBirthDay,
    TResult Function(T failedValue)? invalidEmail,
    TResult Function(T failedValue)? invalidOtpCode,
    TResult Function(T failedValue)? invalidNationalCodeAndPostalCode,
    required TResult orElse(),
  }) {
    if (invalidEmail != null) {
      return invalidEmail(failedValue);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(EmptyField<T> value) emptyField,
    required TResult Function(ProvinceAndCityEmpty<T> value)
        provinceAndCityEmpty,
    required TResult Function(ExceedingLength<T> value) exceedingLength,
    required TResult Function(InvalidPhoneNumber<T> value) invalidPhoneNumber,
    required TResult Function(InvalidBirthDay<T> value) invalidBirthDay,
    required TResult Function(InvalidEmail<T> value) invalidEmail,
    required TResult Function(InvalidOtpCode<T> value) invalidOtpCode,
    required TResult Function(InvalidNationalCodeAndPostalCode<T> value)
        invalidNationalCodeAndPostalCode,
  }) {
    return invalidEmail(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(EmptyField<T> value)? emptyField,
    TResult Function(ProvinceAndCityEmpty<T> value)? provinceAndCityEmpty,
    TResult Function(ExceedingLength<T> value)? exceedingLength,
    TResult Function(InvalidPhoneNumber<T> value)? invalidPhoneNumber,
    TResult Function(InvalidBirthDay<T> value)? invalidBirthDay,
    TResult Function(InvalidEmail<T> value)? invalidEmail,
    TResult Function(InvalidOtpCode<T> value)? invalidOtpCode,
    TResult Function(InvalidNationalCodeAndPostalCode<T> value)?
        invalidNationalCodeAndPostalCode,
  }) {
    return invalidEmail?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(EmptyField<T> value)? emptyField,
    TResult Function(ProvinceAndCityEmpty<T> value)? provinceAndCityEmpty,
    TResult Function(ExceedingLength<T> value)? exceedingLength,
    TResult Function(InvalidPhoneNumber<T> value)? invalidPhoneNumber,
    TResult Function(InvalidBirthDay<T> value)? invalidBirthDay,
    TResult Function(InvalidEmail<T> value)? invalidEmail,
    TResult Function(InvalidOtpCode<T> value)? invalidOtpCode,
    TResult Function(InvalidNationalCodeAndPostalCode<T> value)?
        invalidNationalCodeAndPostalCode,
    required TResult orElse(),
  }) {
    if (invalidEmail != null) {
      return invalidEmail(this);
    }
    return orElse();
  }
}

abstract class InvalidEmail<T> implements ValueFailure<T> {
  const factory InvalidEmail({required T failedValue}) = _$InvalidEmail<T>;

  @override
  T get failedValue;
  @override
  @JsonKey(ignore: true)
  $InvalidEmailCopyWith<T, InvalidEmail<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $InvalidOtpCodeCopyWith<T, $Res>
    implements $ValueFailureCopyWith<T, $Res> {
  factory $InvalidOtpCodeCopyWith(
          InvalidOtpCode<T> value, $Res Function(InvalidOtpCode<T>) then) =
      _$InvalidOtpCodeCopyWithImpl<T, $Res>;
  @override
  $Res call({T failedValue});
}

/// @nodoc
class _$InvalidOtpCodeCopyWithImpl<T, $Res>
    extends _$ValueFailureCopyWithImpl<T, $Res>
    implements $InvalidOtpCodeCopyWith<T, $Res> {
  _$InvalidOtpCodeCopyWithImpl(
      InvalidOtpCode<T> _value, $Res Function(InvalidOtpCode<T>) _then)
      : super(_value, (v) => _then(v as InvalidOtpCode<T>));

  @override
  InvalidOtpCode<T> get _value => super._value as InvalidOtpCode<T>;

  @override
  $Res call({
    Object? failedValue = freezed,
  }) {
    return _then(InvalidOtpCode<T>(
      failedValue: failedValue == freezed
          ? _value.failedValue
          : failedValue // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$InvalidOtpCode<T>
    with DiagnosticableTreeMixin
    implements InvalidOtpCode<T> {
  const _$InvalidOtpCode({required this.failedValue});

  @override
  final T failedValue;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ValueFailure<$T>.invalidOtpCode(failedValue: $failedValue)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ValueFailure<$T>.invalidOtpCode'))
      ..add(DiagnosticsProperty('failedValue', failedValue));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is InvalidOtpCode<T> &&
            const DeepCollectionEquality()
                .equals(other.failedValue, failedValue));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(failedValue));

  @JsonKey(ignore: true)
  @override
  $InvalidOtpCodeCopyWith<T, InvalidOtpCode<T>> get copyWith =>
      _$InvalidOtpCodeCopyWithImpl<T, InvalidOtpCode<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T failedValue) emptyField,
    required TResult Function(T failedValue) provinceAndCityEmpty,
    required TResult Function(T failedValue, int max) exceedingLength,
    required TResult Function(T failedValue) invalidPhoneNumber,
    required TResult Function(T failedValue) invalidBirthDay,
    required TResult Function(T failedValue) invalidEmail,
    required TResult Function(T failedValue) invalidOtpCode,
    required TResult Function(T failedValue) invalidNationalCodeAndPostalCode,
  }) {
    return invalidOtpCode(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(T failedValue)? emptyField,
    TResult Function(T failedValue)? provinceAndCityEmpty,
    TResult Function(T failedValue, int max)? exceedingLength,
    TResult Function(T failedValue)? invalidPhoneNumber,
    TResult Function(T failedValue)? invalidBirthDay,
    TResult Function(T failedValue)? invalidEmail,
    TResult Function(T failedValue)? invalidOtpCode,
    TResult Function(T failedValue)? invalidNationalCodeAndPostalCode,
  }) {
    return invalidOtpCode?.call(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T failedValue)? emptyField,
    TResult Function(T failedValue)? provinceAndCityEmpty,
    TResult Function(T failedValue, int max)? exceedingLength,
    TResult Function(T failedValue)? invalidPhoneNumber,
    TResult Function(T failedValue)? invalidBirthDay,
    TResult Function(T failedValue)? invalidEmail,
    TResult Function(T failedValue)? invalidOtpCode,
    TResult Function(T failedValue)? invalidNationalCodeAndPostalCode,
    required TResult orElse(),
  }) {
    if (invalidOtpCode != null) {
      return invalidOtpCode(failedValue);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(EmptyField<T> value) emptyField,
    required TResult Function(ProvinceAndCityEmpty<T> value)
        provinceAndCityEmpty,
    required TResult Function(ExceedingLength<T> value) exceedingLength,
    required TResult Function(InvalidPhoneNumber<T> value) invalidPhoneNumber,
    required TResult Function(InvalidBirthDay<T> value) invalidBirthDay,
    required TResult Function(InvalidEmail<T> value) invalidEmail,
    required TResult Function(InvalidOtpCode<T> value) invalidOtpCode,
    required TResult Function(InvalidNationalCodeAndPostalCode<T> value)
        invalidNationalCodeAndPostalCode,
  }) {
    return invalidOtpCode(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(EmptyField<T> value)? emptyField,
    TResult Function(ProvinceAndCityEmpty<T> value)? provinceAndCityEmpty,
    TResult Function(ExceedingLength<T> value)? exceedingLength,
    TResult Function(InvalidPhoneNumber<T> value)? invalidPhoneNumber,
    TResult Function(InvalidBirthDay<T> value)? invalidBirthDay,
    TResult Function(InvalidEmail<T> value)? invalidEmail,
    TResult Function(InvalidOtpCode<T> value)? invalidOtpCode,
    TResult Function(InvalidNationalCodeAndPostalCode<T> value)?
        invalidNationalCodeAndPostalCode,
  }) {
    return invalidOtpCode?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(EmptyField<T> value)? emptyField,
    TResult Function(ProvinceAndCityEmpty<T> value)? provinceAndCityEmpty,
    TResult Function(ExceedingLength<T> value)? exceedingLength,
    TResult Function(InvalidPhoneNumber<T> value)? invalidPhoneNumber,
    TResult Function(InvalidBirthDay<T> value)? invalidBirthDay,
    TResult Function(InvalidEmail<T> value)? invalidEmail,
    TResult Function(InvalidOtpCode<T> value)? invalidOtpCode,
    TResult Function(InvalidNationalCodeAndPostalCode<T> value)?
        invalidNationalCodeAndPostalCode,
    required TResult orElse(),
  }) {
    if (invalidOtpCode != null) {
      return invalidOtpCode(this);
    }
    return orElse();
  }
}

abstract class InvalidOtpCode<T> implements ValueFailure<T> {
  const factory InvalidOtpCode({required T failedValue}) = _$InvalidOtpCode<T>;

  @override
  T get failedValue;
  @override
  @JsonKey(ignore: true)
  $InvalidOtpCodeCopyWith<T, InvalidOtpCode<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $InvalidNationalCodeAndPostalCodeCopyWith<T, $Res>
    implements $ValueFailureCopyWith<T, $Res> {
  factory $InvalidNationalCodeAndPostalCodeCopyWith(
          InvalidNationalCodeAndPostalCode<T> value,
          $Res Function(InvalidNationalCodeAndPostalCode<T>) then) =
      _$InvalidNationalCodeAndPostalCodeCopyWithImpl<T, $Res>;
  @override
  $Res call({T failedValue});
}

/// @nodoc
class _$InvalidNationalCodeAndPostalCodeCopyWithImpl<T, $Res>
    extends _$ValueFailureCopyWithImpl<T, $Res>
    implements $InvalidNationalCodeAndPostalCodeCopyWith<T, $Res> {
  _$InvalidNationalCodeAndPostalCodeCopyWithImpl(
      InvalidNationalCodeAndPostalCode<T> _value,
      $Res Function(InvalidNationalCodeAndPostalCode<T>) _then)
      : super(_value, (v) => _then(v as InvalidNationalCodeAndPostalCode<T>));

  @override
  InvalidNationalCodeAndPostalCode<T> get _value =>
      super._value as InvalidNationalCodeAndPostalCode<T>;

  @override
  $Res call({
    Object? failedValue = freezed,
  }) {
    return _then(InvalidNationalCodeAndPostalCode<T>(
      failedValue: failedValue == freezed
          ? _value.failedValue
          : failedValue // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$InvalidNationalCodeAndPostalCode<T>
    with DiagnosticableTreeMixin
    implements InvalidNationalCodeAndPostalCode<T> {
  const _$InvalidNationalCodeAndPostalCode({required this.failedValue});

  @override
  final T failedValue;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ValueFailure<$T>.invalidNationalCodeAndPostalCode(failedValue: $failedValue)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty(
          'type', 'ValueFailure<$T>.invalidNationalCodeAndPostalCode'))
      ..add(DiagnosticsProperty('failedValue', failedValue));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is InvalidNationalCodeAndPostalCode<T> &&
            const DeepCollectionEquality()
                .equals(other.failedValue, failedValue));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(failedValue));

  @JsonKey(ignore: true)
  @override
  $InvalidNationalCodeAndPostalCodeCopyWith<T,
          InvalidNationalCodeAndPostalCode<T>>
      get copyWith => _$InvalidNationalCodeAndPostalCodeCopyWithImpl<T,
          InvalidNationalCodeAndPostalCode<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T failedValue) emptyField,
    required TResult Function(T failedValue) provinceAndCityEmpty,
    required TResult Function(T failedValue, int max) exceedingLength,
    required TResult Function(T failedValue) invalidPhoneNumber,
    required TResult Function(T failedValue) invalidBirthDay,
    required TResult Function(T failedValue) invalidEmail,
    required TResult Function(T failedValue) invalidOtpCode,
    required TResult Function(T failedValue) invalidNationalCodeAndPostalCode,
  }) {
    return invalidNationalCodeAndPostalCode(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(T failedValue)? emptyField,
    TResult Function(T failedValue)? provinceAndCityEmpty,
    TResult Function(T failedValue, int max)? exceedingLength,
    TResult Function(T failedValue)? invalidPhoneNumber,
    TResult Function(T failedValue)? invalidBirthDay,
    TResult Function(T failedValue)? invalidEmail,
    TResult Function(T failedValue)? invalidOtpCode,
    TResult Function(T failedValue)? invalidNationalCodeAndPostalCode,
  }) {
    return invalidNationalCodeAndPostalCode?.call(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T failedValue)? emptyField,
    TResult Function(T failedValue)? provinceAndCityEmpty,
    TResult Function(T failedValue, int max)? exceedingLength,
    TResult Function(T failedValue)? invalidPhoneNumber,
    TResult Function(T failedValue)? invalidBirthDay,
    TResult Function(T failedValue)? invalidEmail,
    TResult Function(T failedValue)? invalidOtpCode,
    TResult Function(T failedValue)? invalidNationalCodeAndPostalCode,
    required TResult orElse(),
  }) {
    if (invalidNationalCodeAndPostalCode != null) {
      return invalidNationalCodeAndPostalCode(failedValue);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(EmptyField<T> value) emptyField,
    required TResult Function(ProvinceAndCityEmpty<T> value)
        provinceAndCityEmpty,
    required TResult Function(ExceedingLength<T> value) exceedingLength,
    required TResult Function(InvalidPhoneNumber<T> value) invalidPhoneNumber,
    required TResult Function(InvalidBirthDay<T> value) invalidBirthDay,
    required TResult Function(InvalidEmail<T> value) invalidEmail,
    required TResult Function(InvalidOtpCode<T> value) invalidOtpCode,
    required TResult Function(InvalidNationalCodeAndPostalCode<T> value)
        invalidNationalCodeAndPostalCode,
  }) {
    return invalidNationalCodeAndPostalCode(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(EmptyField<T> value)? emptyField,
    TResult Function(ProvinceAndCityEmpty<T> value)? provinceAndCityEmpty,
    TResult Function(ExceedingLength<T> value)? exceedingLength,
    TResult Function(InvalidPhoneNumber<T> value)? invalidPhoneNumber,
    TResult Function(InvalidBirthDay<T> value)? invalidBirthDay,
    TResult Function(InvalidEmail<T> value)? invalidEmail,
    TResult Function(InvalidOtpCode<T> value)? invalidOtpCode,
    TResult Function(InvalidNationalCodeAndPostalCode<T> value)?
        invalidNationalCodeAndPostalCode,
  }) {
    return invalidNationalCodeAndPostalCode?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(EmptyField<T> value)? emptyField,
    TResult Function(ProvinceAndCityEmpty<T> value)? provinceAndCityEmpty,
    TResult Function(ExceedingLength<T> value)? exceedingLength,
    TResult Function(InvalidPhoneNumber<T> value)? invalidPhoneNumber,
    TResult Function(InvalidBirthDay<T> value)? invalidBirthDay,
    TResult Function(InvalidEmail<T> value)? invalidEmail,
    TResult Function(InvalidOtpCode<T> value)? invalidOtpCode,
    TResult Function(InvalidNationalCodeAndPostalCode<T> value)?
        invalidNationalCodeAndPostalCode,
    required TResult orElse(),
  }) {
    if (invalidNationalCodeAndPostalCode != null) {
      return invalidNationalCodeAndPostalCode(this);
    }
    return orElse();
  }
}

abstract class InvalidNationalCodeAndPostalCode<T> implements ValueFailure<T> {
  const factory InvalidNationalCodeAndPostalCode({required T failedValue}) =
      _$InvalidNationalCodeAndPostalCode<T>;

  @override
  T get failedValue;
  @override
  @JsonKey(ignore: true)
  $InvalidNationalCodeAndPostalCodeCopyWith<T,
          InvalidNationalCodeAndPostalCode<T>>
      get copyWith => throw _privateConstructorUsedError;
}
