import 'failure/failures.dart';

class UnexpectedValueError extends Error {
  final ValueFailure valueFailure;

  UnexpectedValueError(this.valueFailure);

  @override
  String toString() {
    const explanation = 'جایی یک خطا رخ داده که قابل پیش بینی نبوده مانند getOrCrash()';
    return Error.safeToString('$explanation Failure was: $valueFailure');
  }
}
