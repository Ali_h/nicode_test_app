import 'package:dartz/dartz.dart';

import 'failure/failures.dart';


Either<ValueFailure<String>, String> phoneNumberValidation({required String phone}) {
  if (phone.length < 11 || phone.length > 15) {
    return left(ValueFailure.invalidPhoneNumber(failedValue: phone));
  } else {
    return right(phone);
  }
}

Either<ValueFailure<String>, String> otpCodeValidation({required String code}) {
  if (code.length < 4) {
    return left(ValueFailure.invalidOtpCode(failedValue: code));
  } else {
    return right(code);
  }
}
