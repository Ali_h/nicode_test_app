import 'package:dartz/dartz.dart';
import 'package:nicode_charecter/domain/core/failure/failure.dart';
import 'entity/user_profile_entity.dart';

abstract class IProfileFacade {
  Future<Either<Failure, UserProfileEntity>> getUserProfileData();
}
