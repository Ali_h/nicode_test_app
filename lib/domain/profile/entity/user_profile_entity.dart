class UserProfileEntity {
  final String id;

  UserProfileEntity({
    required this.id,
  });

  factory UserProfileEntity.fromJson(String json) {
    return UserProfileEntity(id: json);
  }
}
