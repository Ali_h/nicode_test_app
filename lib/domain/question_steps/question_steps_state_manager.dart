import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nicode_charecter/application/question_steps/question_steps_bloc.dart';
import 'package:nicode_charecter/domain/question/type/answers_type.dart';

extension AddToQuestionStepX on BuildContext {
  void addDrinkWaterWithMouthToQuestionStep({
    required AnswersType data,
    required String questionShortTitle,
  }) {
    read<QuestionstepsBloc>().add(QuestionstepsEvent.drinkWaterWithMouthChange(answer: data));
    addAnswerInStepsDataList(data: '$questionShortTitle : ${data.answersTypeMapper}');
  }

  void addCheatingInExamToQuestionStep({
    required AnswersType data,
    required String questionShortTitle,
  }) {
    read<QuestionstepsBloc>().add(QuestionstepsEvent.cheatingInExamChange(answer: data));
    addAnswerInStepsDataList(data: '$questionShortTitle : ${data.answersTypeMapper}');
  }

  void addLoveTheEmotionalMovieToQuestionStep({
    required AnswersType data,
    required String questionShortTitle,
  }) {
    read<QuestionstepsBloc>().add(QuestionstepsEvent.loveTheEmotionalMovieChange(answer: data));
    addAnswerInStepsDataList(data: '$questionShortTitle : ${data.answersTypeMapper}');
  }

  void addToBeImmortalToQuestionStep({
    required AnswersType data,
    required String questionShortTitle,
  }) {
    read<QuestionstepsBloc>().add(QuestionstepsEvent.toBeImmortalChange(answer: data));
    addAnswerInStepsDataList(data: '$questionShortTitle : ${data.answersTypeMapper}');
  }

  void addSmokeCigarettesToQuestionStep({
    required AnswersType data,
    required String questionShortTitle,
  }) {
    read<QuestionstepsBloc>().add(QuestionstepsEvent.smokeCigarettesChange(answer: data));
    addAnswerInStepsDataList(data: '$questionShortTitle : ${data.answersTypeMapper}');
  }

  void addAlwaysPresentInUniToQuestionStep({
    required AnswersType data,
    required String questionShortTitle,
  }) {
    read<QuestionstepsBloc>().add(QuestionstepsEvent.alwaysPresentInUniChange(answer: data));
    addAnswerInStepsDataList(data: '$questionShortTitle : ${data.answersTypeMapper}');
  }

  void addTravelWithoutPermissionToQuestionStep({
    required AnswersType data,
    required String questionShortTitle,
  }) {
    read<QuestionstepsBloc>().add(QuestionstepsEvent.travelWithoutPermissionChange(answer: data));
    addAnswerInStepsDataList(data: '$questionShortTitle : ${data.answersTypeMapper}');
  }

  void addAnswerInStepsDataList({required String data}) {
    read<QuestionstepsBloc>().add(QuestionstepsEvent.addDataInStepList(data: data));
  }
}

extension RemoveFromQuestionStepX on BuildContext {
  void initialState() => read<QuestionstepsBloc>().add(const QuestionstepsEvent.initial());

  void removeDrinkWaterWithMouthFromQuestionStep() {
    read<QuestionstepsBloc>().add(const QuestionstepsEvent.drinkWaterWithMouthChange(answer: null));
    removeAnswerInStepDataList();
  }

  void removeCheatingInExamFromQuestionStep() {
    read<QuestionstepsBloc>().add(const QuestionstepsEvent.cheatingInExamChange(answer: null));
    removeAnswerInStepDataList();
  }

  void removeLoveTheEmotionalMovieFromQuestionStep() {
    read<QuestionstepsBloc>()
        .add(const QuestionstepsEvent.loveTheEmotionalMovieChange(answer: null));
    removeAnswerInStepDataList();
  }

  void removeToBeImmortalFromQuestionStep() {
    read<QuestionstepsBloc>().add(const QuestionstepsEvent.toBeImmortalChange(answer: null));
    removeAnswerInStepDataList();
  }

  void removeSmokeCigarettesFromQuestionStep() {
    read<QuestionstepsBloc>().add(const QuestionstepsEvent.smokeCigarettesChange(answer: null));
    removeAnswerInStepDataList();
  }

  void removeAlwaysPresentInUniFromQuestionStep() {
    read<QuestionstepsBloc>().add(const QuestionstepsEvent.alwaysPresentInUniChange(answer: null));
    removeAnswerInStepDataList();
  }

  void removeTravelWithoutPermissionFromQuestionStep() {
    read<QuestionstepsBloc>()
        .add(const QuestionstepsEvent.travelWithoutPermissionChange(answer: null));
    removeAnswerInStepDataList();
  }

  void removeAnswerInStepDataList() {
    final stepsDataLength = stepsData.length;

    read<QuestionstepsBloc>().add(
      QuestionstepsEvent.removeFromStepListWitnIndex(index: stepsDataLength - 1),
    );
  }
}

extension ReadQuestionStepX on BuildContext {
  AnswersType? get drinkWaterWithMouth => read<QuestionstepsBloc>().state.drinkWaterWithMouth;

  AnswersType? get alwaysPresentInUni => read<QuestionstepsBloc>().state.alwaysPresentInUni;

  AnswersType? get smokeCigarettes => read<QuestionstepsBloc>().state.smokeCigarettes;

  AnswersType? get toBeImmortal => read<QuestionstepsBloc>().state.toBeImmortal;

  AnswersType? get loveTheEmotionalMovie => read<QuestionstepsBloc>().state.loveTheEmotionalMovie;

  AnswersType? get cheatingInExam => read<QuestionstepsBloc>().state.cheatingInExam;

  AnswersType? get travelWithoutPermission =>
      read<QuestionstepsBloc>().state.travelWithoutPermission;

  List<String> get stepsData => read<QuestionstepsBloc>().state.stepsData;
}
