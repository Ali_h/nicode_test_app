// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'question_steps_entity.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$QuestionStepsEntityTearOff {
  const _$QuestionStepsEntityTearOff();

  _QuestionStepsEntity call(
      {required AnswersType? drinkWaterWithMouth,
      required AnswersType? cheatingInExam,
      required AnswersType? loveTheEmotionalMovie,
      required AnswersType? toBeImmortal,
      required AnswersType? smokeCigarettes,
      required AnswersType? alwaysPresentInUni,
      required AnswersType? travelWithoutPermission,
      required List<String> stepsData}) {
    return _QuestionStepsEntity(
      drinkWaterWithMouth: drinkWaterWithMouth,
      cheatingInExam: cheatingInExam,
      loveTheEmotionalMovie: loveTheEmotionalMovie,
      toBeImmortal: toBeImmortal,
      smokeCigarettes: smokeCigarettes,
      alwaysPresentInUni: alwaysPresentInUni,
      travelWithoutPermission: travelWithoutPermission,
      stepsData: stepsData,
    );
  }
}

/// @nodoc
const $QuestionStepsEntity = _$QuestionStepsEntityTearOff();

/// @nodoc
mixin _$QuestionStepsEntity {
  AnswersType? get drinkWaterWithMouth => throw _privateConstructorUsedError;
  AnswersType? get cheatingInExam => throw _privateConstructorUsedError;
  AnswersType? get loveTheEmotionalMovie => throw _privateConstructorUsedError;
  AnswersType? get toBeImmortal => throw _privateConstructorUsedError;
  AnswersType? get smokeCigarettes => throw _privateConstructorUsedError;
  AnswersType? get alwaysPresentInUni => throw _privateConstructorUsedError;
  AnswersType? get travelWithoutPermission =>
      throw _privateConstructorUsedError;
  List<String> get stepsData => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $QuestionStepsEntityCopyWith<QuestionStepsEntity> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $QuestionStepsEntityCopyWith<$Res> {
  factory $QuestionStepsEntityCopyWith(
          QuestionStepsEntity value, $Res Function(QuestionStepsEntity) then) =
      _$QuestionStepsEntityCopyWithImpl<$Res>;
  $Res call(
      {AnswersType? drinkWaterWithMouth,
      AnswersType? cheatingInExam,
      AnswersType? loveTheEmotionalMovie,
      AnswersType? toBeImmortal,
      AnswersType? smokeCigarettes,
      AnswersType? alwaysPresentInUni,
      AnswersType? travelWithoutPermission,
      List<String> stepsData});

  $AnswersTypeCopyWith<$Res>? get drinkWaterWithMouth;
  $AnswersTypeCopyWith<$Res>? get cheatingInExam;
  $AnswersTypeCopyWith<$Res>? get loveTheEmotionalMovie;
  $AnswersTypeCopyWith<$Res>? get toBeImmortal;
  $AnswersTypeCopyWith<$Res>? get smokeCigarettes;
  $AnswersTypeCopyWith<$Res>? get alwaysPresentInUni;
  $AnswersTypeCopyWith<$Res>? get travelWithoutPermission;
}

/// @nodoc
class _$QuestionStepsEntityCopyWithImpl<$Res>
    implements $QuestionStepsEntityCopyWith<$Res> {
  _$QuestionStepsEntityCopyWithImpl(this._value, this._then);

  final QuestionStepsEntity _value;
  // ignore: unused_field
  final $Res Function(QuestionStepsEntity) _then;

  @override
  $Res call({
    Object? drinkWaterWithMouth = freezed,
    Object? cheatingInExam = freezed,
    Object? loveTheEmotionalMovie = freezed,
    Object? toBeImmortal = freezed,
    Object? smokeCigarettes = freezed,
    Object? alwaysPresentInUni = freezed,
    Object? travelWithoutPermission = freezed,
    Object? stepsData = freezed,
  }) {
    return _then(_value.copyWith(
      drinkWaterWithMouth: drinkWaterWithMouth == freezed
          ? _value.drinkWaterWithMouth
          : drinkWaterWithMouth // ignore: cast_nullable_to_non_nullable
              as AnswersType?,
      cheatingInExam: cheatingInExam == freezed
          ? _value.cheatingInExam
          : cheatingInExam // ignore: cast_nullable_to_non_nullable
              as AnswersType?,
      loveTheEmotionalMovie: loveTheEmotionalMovie == freezed
          ? _value.loveTheEmotionalMovie
          : loveTheEmotionalMovie // ignore: cast_nullable_to_non_nullable
              as AnswersType?,
      toBeImmortal: toBeImmortal == freezed
          ? _value.toBeImmortal
          : toBeImmortal // ignore: cast_nullable_to_non_nullable
              as AnswersType?,
      smokeCigarettes: smokeCigarettes == freezed
          ? _value.smokeCigarettes
          : smokeCigarettes // ignore: cast_nullable_to_non_nullable
              as AnswersType?,
      alwaysPresentInUni: alwaysPresentInUni == freezed
          ? _value.alwaysPresentInUni
          : alwaysPresentInUni // ignore: cast_nullable_to_non_nullable
              as AnswersType?,
      travelWithoutPermission: travelWithoutPermission == freezed
          ? _value.travelWithoutPermission
          : travelWithoutPermission // ignore: cast_nullable_to_non_nullable
              as AnswersType?,
      stepsData: stepsData == freezed
          ? _value.stepsData
          : stepsData // ignore: cast_nullable_to_non_nullable
              as List<String>,
    ));
  }

  @override
  $AnswersTypeCopyWith<$Res>? get drinkWaterWithMouth {
    if (_value.drinkWaterWithMouth == null) {
      return null;
    }

    return $AnswersTypeCopyWith<$Res>(_value.drinkWaterWithMouth!, (value) {
      return _then(_value.copyWith(drinkWaterWithMouth: value));
    });
  }

  @override
  $AnswersTypeCopyWith<$Res>? get cheatingInExam {
    if (_value.cheatingInExam == null) {
      return null;
    }

    return $AnswersTypeCopyWith<$Res>(_value.cheatingInExam!, (value) {
      return _then(_value.copyWith(cheatingInExam: value));
    });
  }

  @override
  $AnswersTypeCopyWith<$Res>? get loveTheEmotionalMovie {
    if (_value.loveTheEmotionalMovie == null) {
      return null;
    }

    return $AnswersTypeCopyWith<$Res>(_value.loveTheEmotionalMovie!, (value) {
      return _then(_value.copyWith(loveTheEmotionalMovie: value));
    });
  }

  @override
  $AnswersTypeCopyWith<$Res>? get toBeImmortal {
    if (_value.toBeImmortal == null) {
      return null;
    }

    return $AnswersTypeCopyWith<$Res>(_value.toBeImmortal!, (value) {
      return _then(_value.copyWith(toBeImmortal: value));
    });
  }

  @override
  $AnswersTypeCopyWith<$Res>? get smokeCigarettes {
    if (_value.smokeCigarettes == null) {
      return null;
    }

    return $AnswersTypeCopyWith<$Res>(_value.smokeCigarettes!, (value) {
      return _then(_value.copyWith(smokeCigarettes: value));
    });
  }

  @override
  $AnswersTypeCopyWith<$Res>? get alwaysPresentInUni {
    if (_value.alwaysPresentInUni == null) {
      return null;
    }

    return $AnswersTypeCopyWith<$Res>(_value.alwaysPresentInUni!, (value) {
      return _then(_value.copyWith(alwaysPresentInUni: value));
    });
  }

  @override
  $AnswersTypeCopyWith<$Res>? get travelWithoutPermission {
    if (_value.travelWithoutPermission == null) {
      return null;
    }

    return $AnswersTypeCopyWith<$Res>(_value.travelWithoutPermission!, (value) {
      return _then(_value.copyWith(travelWithoutPermission: value));
    });
  }
}

/// @nodoc
abstract class _$QuestionStepsEntityCopyWith<$Res>
    implements $QuestionStepsEntityCopyWith<$Res> {
  factory _$QuestionStepsEntityCopyWith(_QuestionStepsEntity value,
          $Res Function(_QuestionStepsEntity) then) =
      __$QuestionStepsEntityCopyWithImpl<$Res>;
  @override
  $Res call(
      {AnswersType? drinkWaterWithMouth,
      AnswersType? cheatingInExam,
      AnswersType? loveTheEmotionalMovie,
      AnswersType? toBeImmortal,
      AnswersType? smokeCigarettes,
      AnswersType? alwaysPresentInUni,
      AnswersType? travelWithoutPermission,
      List<String> stepsData});

  @override
  $AnswersTypeCopyWith<$Res>? get drinkWaterWithMouth;
  @override
  $AnswersTypeCopyWith<$Res>? get cheatingInExam;
  @override
  $AnswersTypeCopyWith<$Res>? get loveTheEmotionalMovie;
  @override
  $AnswersTypeCopyWith<$Res>? get toBeImmortal;
  @override
  $AnswersTypeCopyWith<$Res>? get smokeCigarettes;
  @override
  $AnswersTypeCopyWith<$Res>? get alwaysPresentInUni;
  @override
  $AnswersTypeCopyWith<$Res>? get travelWithoutPermission;
}

/// @nodoc
class __$QuestionStepsEntityCopyWithImpl<$Res>
    extends _$QuestionStepsEntityCopyWithImpl<$Res>
    implements _$QuestionStepsEntityCopyWith<$Res> {
  __$QuestionStepsEntityCopyWithImpl(
      _QuestionStepsEntity _value, $Res Function(_QuestionStepsEntity) _then)
      : super(_value, (v) => _then(v as _QuestionStepsEntity));

  @override
  _QuestionStepsEntity get _value => super._value as _QuestionStepsEntity;

  @override
  $Res call({
    Object? drinkWaterWithMouth = freezed,
    Object? cheatingInExam = freezed,
    Object? loveTheEmotionalMovie = freezed,
    Object? toBeImmortal = freezed,
    Object? smokeCigarettes = freezed,
    Object? alwaysPresentInUni = freezed,
    Object? travelWithoutPermission = freezed,
    Object? stepsData = freezed,
  }) {
    return _then(_QuestionStepsEntity(
      drinkWaterWithMouth: drinkWaterWithMouth == freezed
          ? _value.drinkWaterWithMouth
          : drinkWaterWithMouth // ignore: cast_nullable_to_non_nullable
              as AnswersType?,
      cheatingInExam: cheatingInExam == freezed
          ? _value.cheatingInExam
          : cheatingInExam // ignore: cast_nullable_to_non_nullable
              as AnswersType?,
      loveTheEmotionalMovie: loveTheEmotionalMovie == freezed
          ? _value.loveTheEmotionalMovie
          : loveTheEmotionalMovie // ignore: cast_nullable_to_non_nullable
              as AnswersType?,
      toBeImmortal: toBeImmortal == freezed
          ? _value.toBeImmortal
          : toBeImmortal // ignore: cast_nullable_to_non_nullable
              as AnswersType?,
      smokeCigarettes: smokeCigarettes == freezed
          ? _value.smokeCigarettes
          : smokeCigarettes // ignore: cast_nullable_to_non_nullable
              as AnswersType?,
      alwaysPresentInUni: alwaysPresentInUni == freezed
          ? _value.alwaysPresentInUni
          : alwaysPresentInUni // ignore: cast_nullable_to_non_nullable
              as AnswersType?,
      travelWithoutPermission: travelWithoutPermission == freezed
          ? _value.travelWithoutPermission
          : travelWithoutPermission // ignore: cast_nullable_to_non_nullable
              as AnswersType?,
      stepsData: stepsData == freezed
          ? _value.stepsData
          : stepsData // ignore: cast_nullable_to_non_nullable
              as List<String>,
    ));
  }
}

/// @nodoc

class _$_QuestionStepsEntity extends _QuestionStepsEntity {
  const _$_QuestionStepsEntity(
      {required this.drinkWaterWithMouth,
      required this.cheatingInExam,
      required this.loveTheEmotionalMovie,
      required this.toBeImmortal,
      required this.smokeCigarettes,
      required this.alwaysPresentInUni,
      required this.travelWithoutPermission,
      required this.stepsData})
      : super._();

  @override
  final AnswersType? drinkWaterWithMouth;
  @override
  final AnswersType? cheatingInExam;
  @override
  final AnswersType? loveTheEmotionalMovie;
  @override
  final AnswersType? toBeImmortal;
  @override
  final AnswersType? smokeCigarettes;
  @override
  final AnswersType? alwaysPresentInUni;
  @override
  final AnswersType? travelWithoutPermission;
  @override
  final List<String> stepsData;

  @override
  String toString() {
    return 'QuestionStepsEntity(drinkWaterWithMouth: $drinkWaterWithMouth, cheatingInExam: $cheatingInExam, loveTheEmotionalMovie: $loveTheEmotionalMovie, toBeImmortal: $toBeImmortal, smokeCigarettes: $smokeCigarettes, alwaysPresentInUni: $alwaysPresentInUni, travelWithoutPermission: $travelWithoutPermission, stepsData: $stepsData)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _QuestionStepsEntity &&
            const DeepCollectionEquality()
                .equals(other.drinkWaterWithMouth, drinkWaterWithMouth) &&
            const DeepCollectionEquality()
                .equals(other.cheatingInExam, cheatingInExam) &&
            const DeepCollectionEquality()
                .equals(other.loveTheEmotionalMovie, loveTheEmotionalMovie) &&
            const DeepCollectionEquality()
                .equals(other.toBeImmortal, toBeImmortal) &&
            const DeepCollectionEquality()
                .equals(other.smokeCigarettes, smokeCigarettes) &&
            const DeepCollectionEquality()
                .equals(other.alwaysPresentInUni, alwaysPresentInUni) &&
            const DeepCollectionEquality().equals(
                other.travelWithoutPermission, travelWithoutPermission) &&
            const DeepCollectionEquality().equals(other.stepsData, stepsData));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(drinkWaterWithMouth),
      const DeepCollectionEquality().hash(cheatingInExam),
      const DeepCollectionEquality().hash(loveTheEmotionalMovie),
      const DeepCollectionEquality().hash(toBeImmortal),
      const DeepCollectionEquality().hash(smokeCigarettes),
      const DeepCollectionEquality().hash(alwaysPresentInUni),
      const DeepCollectionEquality().hash(travelWithoutPermission),
      const DeepCollectionEquality().hash(stepsData));

  @JsonKey(ignore: true)
  @override
  _$QuestionStepsEntityCopyWith<_QuestionStepsEntity> get copyWith =>
      __$QuestionStepsEntityCopyWithImpl<_QuestionStepsEntity>(
          this, _$identity);
}

abstract class _QuestionStepsEntity extends QuestionStepsEntity {
  const factory _QuestionStepsEntity(
      {required AnswersType? drinkWaterWithMouth,
      required AnswersType? cheatingInExam,
      required AnswersType? loveTheEmotionalMovie,
      required AnswersType? toBeImmortal,
      required AnswersType? smokeCigarettes,
      required AnswersType? alwaysPresentInUni,
      required AnswersType? travelWithoutPermission,
      required List<String> stepsData}) = _$_QuestionStepsEntity;
  const _QuestionStepsEntity._() : super._();

  @override
  AnswersType? get drinkWaterWithMouth;
  @override
  AnswersType? get cheatingInExam;
  @override
  AnswersType? get loveTheEmotionalMovie;
  @override
  AnswersType? get toBeImmortal;
  @override
  AnswersType? get smokeCigarettes;
  @override
  AnswersType? get alwaysPresentInUni;
  @override
  AnswersType? get travelWithoutPermission;
  @override
  List<String> get stepsData;
  @override
  @JsonKey(ignore: true)
  _$QuestionStepsEntityCopyWith<_QuestionStepsEntity> get copyWith =>
      throw _privateConstructorUsedError;
}
