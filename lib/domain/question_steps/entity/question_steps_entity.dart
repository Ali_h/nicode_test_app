import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:nicode_charecter/domain/question/type/answers_type.dart';

part 'question_steps_entity.freezed.dart';

@freezed
class QuestionStepsEntity with _$QuestionStepsEntity {
  const factory QuestionStepsEntity({
    required AnswersType? drinkWaterWithMouth,
    required AnswersType? cheatingInExam,
    required AnswersType? loveTheEmotionalMovie,
    required AnswersType? toBeImmortal,
    required AnswersType? smokeCigarettes,
    required AnswersType? alwaysPresentInUni,
    required AnswersType? travelWithoutPermission,
    required List<String> stepsData,
  }) = _QuestionStepsEntity;

  const QuestionStepsEntity._();

  factory QuestionStepsEntity.initial() => const QuestionStepsEntity(
        drinkWaterWithMouth: null,
        cheatingInExam: null,
        loveTheEmotionalMovie: null,
        toBeImmortal: null,
        smokeCigarettes: null,
        alwaysPresentInUni: null,
        travelWithoutPermission: null,
        stepsData: [],
      );
}
