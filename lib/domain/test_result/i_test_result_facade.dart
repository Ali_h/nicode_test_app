import 'package:nicode_charecter/domain/question_steps/entity/question_steps_entity.dart';
import 'package:nicode_charecter/domain/test_result/entity/test_result_entity.dart';

abstract class ITestResultFacade {
  TestResultEntity getTestResult({
    required QuestionStepsEntity answersData,
  });
}
