class TestResultEntity {
  final String title;
  final String description;
  final String image;

  TestResultEntity({
    required this.title,
    required this.description,
    required this.image,
  });

  factory TestResultEntity.superman() {
    return TestResultEntity(
      title: 'Superman',
      description:
          'تو یه شخصیت خیلی شجاع و نترس داری که همیشه دنبال این هستی که بتونی راز جاودانگی رو پیدا کنی',
      image: 'assets/image/superman.jpg',
    );
  }

  factory TestResultEntity.batman() {
    return TestResultEntity(
      title: 'Batman',
      description:
          'آدم نترسی هستی و خودتو همیشه تو موقعیت های خطرناک قرار میدی. شخصیت یکم تاریکی داری و خیلی اوقات از زندگی خسته میشی.',
      image: 'assets/image/batman.jpg',
    );
  }

  factory TestResultEntity.youLoose() {
    return TestResultEntity(
      title: 'تو باختی !!',
      description:
          'تنها هدف ساخت این تست این بود که ببینم سیگار میکشی یا نه. الان هم متاسفانه جواب تستت داره واسه خونوادت ارسال میشه :))',
      image: 'assets/image/loose.jpg',
    );
  }

  factory TestResultEntity.daredevil() {
    return TestResultEntity(
      title: 'Daredevil',
      description:
          'تو یه آدم درستکار و شجاعی هستی که سعی میکنی همیشه بر اساس قانون عمل کنی و هیچ وقت کار بدی نکنی. آفرین به تو :)',
      image: 'assets/image/daredevil.jpg',
    );
  }

  factory TestResultEntity.nelsonMandela() {
    return TestResultEntity(
      title: 'Nelson Mandela',
      description:
          'تو یه آدم احساساتی و مهربون هستی که همیشه سر کلاسا اون جلو میشستی و به حرفای استاد با دقت گوش میکردی. بدون اجازه خونوادت هم هیچ کاری نمیکنی که این یه نقطه ضعفه برات :)',
      image: 'assets/image/nelson.jpg',
    );
  }

  factory TestResultEntity.kungfuPanda() {
    return TestResultEntity(
      title: 'Kung Fu Panda',
      description:
          'شخصیت عاطفی داری و اکثر اوقات هم مهربونی. خیلی دوست داری که هی غذا بخوری ولی مامانت جلوتو میگیره و به درس خوندن هم اونقد اهمیت نمیدی و همش دوست داری تفریح کنی :)',
      image: 'assets/image/panda.jpg',
    );
  }

  factory TestResultEntity.joker() {
    return TestResultEntity(
      title: 'Joker',
      description:
          'تو آدمی هستی که دوست داری شرور باشی ولی نمیتونی :)). تو زندگیت از مامان و بابات بیشتر از همه میترسی و سعی میکنی که کارات رو زیرزیرکی انجام بدی تا اونا نفهمن. عاشق اینم هستی که با رفیقات بری شمال جوج بزنی با نوشابه. نوش جونت جای ما هم خالی :)',
      image: 'assets/image/joker.jpg',
    );
  }

  factory TestResultEntity.hannibal() {
    return TestResultEntity(
      title: 'Lecter Hannibal',
      description: 'تو آدمی هستی که خیلی گوشه گیری و بدون اجازه خونوادت هیچ کاری نمیتونی انجام بدی. تو باید بیشتر تلاش کنی تا دچار افسردگی نشی چون اگه همین جوری پیش بره به یه قاتل سریالی تبدیل میشی :((',
      image: 'assets/image/lecterhannibal.jpg',
    );
  }
}
