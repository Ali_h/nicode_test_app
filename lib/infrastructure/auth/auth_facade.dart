import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:nicode_charecter/domain/auth/i_auth_facade.dart';
import 'package:nicode_charecter/domain/core/value_objects.dart';
import 'package:nicode_charecter/infrastructure/storage/shared_pref.dart';

import '../../domain/core/failure/failure.dart';

@LazySingleton(as: IAuthFacade)
class AuthFacade implements IAuthFacade {
  final Dio _dio;
  final SharedPref _pref;

  AuthFacade(this._dio, this._pref);

  @override
  Future<Either<Failure, Unit>> sendOtpCode({
    required OtpCode otpCode,
    required PhoneNumber phoneNumber,
  }) async {
    final phoneNumberStr = phoneNumber.getOrCrash();
    final otpCodeStr = otpCode.getOrCrash();
    try {
      final res = await _dio.post(
        'v1/signin/verify',
        data: {
          'phoneNumber': phoneNumberStr,
          'code': otpCodeStr,
          'sessionId': 1,
        },
      );
      final Map<String, dynamic> data = res.data as Map<String, dynamic>;
      await _pref.singIn(token: data['data']['token']['value'] as String);
      return right(unit);
    } on DioError catch (err) {
      return left(Failure(message: err.response?.data['message'] as String));
    }
  }

  @override
  Future<Either<Failure, Unit>> sendPhoneNumber({required PhoneNumber phoneNumber}) async {
    final phoneNumberStr = phoneNumber.getOrCrash();
    try {
      await _dio.post(
        'v1/signin',
        data: {'phoneNumber': phoneNumberStr},
      );
      return right(unit);
    } on DioError catch (err) {
      return left(Failure(message: err.response?.data['message'] as String));
    }
  }
}
