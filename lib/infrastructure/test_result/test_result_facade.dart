import 'package:injectable/injectable.dart';
import 'package:nicode_charecter/domain/question/type/answers_type.dart';
import 'package:nicode_charecter/domain/question_steps/entity/question_steps_entity.dart';
import 'package:nicode_charecter/domain/test_result/entity/test_result_entity.dart';
import 'package:nicode_charecter/domain/test_result/i_test_result_facade.dart';

@LazySingleton(as: ITestResultFacade)
class TestResultFacade implements ITestResultFacade {
  TestResultFacade();

  @override
  TestResultEntity getTestResult({required QuestionStepsEntity answersData}) {
    if (answersData.drinkWaterWithMouth == const AnswersType.yes() &&
        answersData.cheatingInExam == const AnswersType.yes() &&
        answersData.toBeImmortal == const AnswersType.yes()) {
      return TestResultEntity.superman();
    } else if (answersData.drinkWaterWithMouth == const AnswersType.yes() &&
        answersData.cheatingInExam == const AnswersType.yes() &&
        answersData.toBeImmortal == const AnswersType.no()) {
      return TestResultEntity.batman();
    } else if (answersData.drinkWaterWithMouth == const AnswersType.yes() &&
        answersData.cheatingInExam == const AnswersType.no() &&
        answersData.smokeCigarettes == const AnswersType.yes()) {
      return TestResultEntity.youLoose();
    } else if (answersData.drinkWaterWithMouth == const AnswersType.yes() &&
        answersData.cheatingInExam == const AnswersType.no() &&
        answersData.smokeCigarettes == const AnswersType.no()) {
      return TestResultEntity.daredevil();
    } else if (answersData.drinkWaterWithMouth == const AnswersType.no() &&
        answersData.loveTheEmotionalMovie == const AnswersType.yes() &&
        answersData.alwaysPresentInUni == const AnswersType.yes()) {
      return TestResultEntity.nelsonMandela();
    } else if (answersData.drinkWaterWithMouth == const AnswersType.no() &&
        answersData.loveTheEmotionalMovie == const AnswersType.yes() &&
        answersData.alwaysPresentInUni == const AnswersType.no()) {
      return TestResultEntity.kungfuPanda();
    } else if (answersData.drinkWaterWithMouth == const AnswersType.no() &&
        answersData.loveTheEmotionalMovie == const AnswersType.no() &&
        answersData.travelWithoutPermission == const AnswersType.yes()) {
      return TestResultEntity.joker();
    } else {
      return TestResultEntity.hannibal();
    }
  }
}
