import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';

@lazySingleton
class SharedPref {
  final String _tokenKey = 'token';
  final SharedPreferences _pref;

  SharedPref(this._pref);

  Future<String> getToken() async {
    return _pref.getString(_tokenKey) ?? '';
  }

  Future<void> setToken({required String token}) async {
    await _pref.setString(_tokenKey, token);
  }

  Future<void> singIn({
    required String token,
  }) async {
    await signOut();
    await setToken(token: token);
  }

  Future<void> signOut() async {
    await _pref.remove(_tokenKey);
  }
}
