import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:nicode_charecter/domain/core/failure/failure.dart';
import 'package:nicode_charecter/domain/profile/entity/user_profile_entity.dart';
import 'package:nicode_charecter/domain/profile/i_profile_facade.dart';
import 'package:nicode_charecter/infrastructure/core/dio_options.dart';
import 'package:nicode_charecter/infrastructure/storage/shared_pref.dart';
import 'package:nicode_charecter/presentation/shared/messages.dart';

@LazySingleton(as: IProfileFacade)
class ProfileFacade implements IProfileFacade {
  final Dio _dio;
  final SharedPref _pref;

  ProfileFacade(this._dio, this._pref);

  @override
  Future<Either<Failure, UserProfileEntity>> getUserProfileData() async {
    final String token = await _pref.getToken();
    if (token.isNotEmpty) {
      try {
        final res = await _dio.get(
          'v1/signin/user',
          options: Options(
            extra: {
              DioOptions.needAuthentication: true,
            },
          ),
        );

        return right(UserProfileEntity.fromJson(res.data as String));
      } on DioError catch (err) {
        return left(Failure(message: err.response?.data['message'] as String));
      }
    } else {
      return left(const Failure(message: Messages.notAuthenticatedErrorMessage, statusCode: 401));
    }
  }
}
