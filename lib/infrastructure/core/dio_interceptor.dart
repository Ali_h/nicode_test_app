import 'dart:async';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:nicode_charecter/infrastructure/storage/shared_pref.dart';

import 'dio_options.dart';

@LazySingleton()
class CheckRequestErrorInterceptor extends Interceptor {
  final SharedPref _sharedPref;

  CheckRequestErrorInterceptor(this._sharedPref);

  @override
  Future onRequest(RequestOptions options, RequestInterceptorHandler handler) async {
    if (options.extra[DioOptions.needAuthentication] != null) {
      final bool needAuthentication = options.extra[DioOptions.needAuthentication] as bool;
      if (needAuthentication) {
        final String token = await _sharedPref.getToken();
        options.headers['Authorization'] = 'Bearer $token';
      }
    }
    return handler.next(options);
  }

  @override
  Future onError(DioError err, ErrorInterceptorHandler handler) async {
    return handler.next(err);
  }

  @override
  Future onResponse(Response response, ResponseInterceptorHandler handler) async {
    return handler.next(response);
  }
}
