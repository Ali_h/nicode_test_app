import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';

import '../../injection.dart';
import 'dio_interceptor.dart';

@lazySingleton
class DioOptions {
  final Dio dio;
  static const String needAuthentication = 'needAuthentication';

  DioOptions(this.dio);

  void setOptionsForDio() {
    dio.options.baseUrl = 'http://nikotest.nicode.org/';
    dio.interceptors.addAll([
      getIt<CheckRequestErrorInterceptor>(),
    ]);
  }
}
