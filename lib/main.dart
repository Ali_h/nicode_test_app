import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:injectable/injectable.dart';
import 'package:nicode_charecter/presentation/app_widget.dart';

import 'infrastructure/core/dio_options.dart';
import 'injection.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  //set portrait
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  //set injection
  await configureInjection(Environment.prod);

  //set defualt config for dio
  getIt<DioOptions>().setOptionsForDio();

  //run the application
  runApp(const AppWidget());
}
