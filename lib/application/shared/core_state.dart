import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:nicode_charecter/domain/core/failure/failure.dart';

part 'core_state.freezed.dart';

@freezed
class CoreState<T> with _$CoreState<T> {
  const factory CoreState.initial() = CoreStateInitial;

  const factory CoreState.loadInProgress() = LoadInProgress;

  const factory CoreState.fetchDataFailure({
    required Failure dataFailure,
  }) = FetchDataFailure;

  const factory CoreState.fetchDataSuccessfully({
    required T data,
  }) = FetchDataSuccessfully<T>;
}
