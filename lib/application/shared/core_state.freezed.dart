// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'core_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$CoreStateTearOff {
  const _$CoreStateTearOff();

  CoreStateInitial<T> initial<T>() {
    return CoreStateInitial<T>();
  }

  LoadInProgress<T> loadInProgress<T>() {
    return LoadInProgress<T>();
  }

  FetchDataFailure<T> fetchDataFailure<T>({required Failure dataFailure}) {
    return FetchDataFailure<T>(
      dataFailure: dataFailure,
    );
  }

  FetchDataSuccessfully<T> fetchDataSuccessfully<T>({required T data}) {
    return FetchDataSuccessfully<T>(
      data: data,
    );
  }
}

/// @nodoc
const $CoreState = _$CoreStateTearOff();

/// @nodoc
mixin _$CoreState<T> {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loadInProgress,
    required TResult Function(Failure dataFailure) fetchDataFailure,
    required TResult Function(T data) fetchDataSuccessfully,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loadInProgress,
    TResult Function(Failure dataFailure)? fetchDataFailure,
    TResult Function(T data)? fetchDataSuccessfully,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loadInProgress,
    TResult Function(Failure dataFailure)? fetchDataFailure,
    TResult Function(T data)? fetchDataSuccessfully,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CoreStateInitial<T> value) initial,
    required TResult Function(LoadInProgress<T> value) loadInProgress,
    required TResult Function(FetchDataFailure<T> value) fetchDataFailure,
    required TResult Function(FetchDataSuccessfully<T> value)
        fetchDataSuccessfully,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(CoreStateInitial<T> value)? initial,
    TResult Function(LoadInProgress<T> value)? loadInProgress,
    TResult Function(FetchDataFailure<T> value)? fetchDataFailure,
    TResult Function(FetchDataSuccessfully<T> value)? fetchDataSuccessfully,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CoreStateInitial<T> value)? initial,
    TResult Function(LoadInProgress<T> value)? loadInProgress,
    TResult Function(FetchDataFailure<T> value)? fetchDataFailure,
    TResult Function(FetchDataSuccessfully<T> value)? fetchDataSuccessfully,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CoreStateCopyWith<T, $Res> {
  factory $CoreStateCopyWith(
          CoreState<T> value, $Res Function(CoreState<T>) then) =
      _$CoreStateCopyWithImpl<T, $Res>;
}

/// @nodoc
class _$CoreStateCopyWithImpl<T, $Res> implements $CoreStateCopyWith<T, $Res> {
  _$CoreStateCopyWithImpl(this._value, this._then);

  final CoreState<T> _value;
  // ignore: unused_field
  final $Res Function(CoreState<T>) _then;
}

/// @nodoc
abstract class $CoreStateInitialCopyWith<T, $Res> {
  factory $CoreStateInitialCopyWith(
          CoreStateInitial<T> value, $Res Function(CoreStateInitial<T>) then) =
      _$CoreStateInitialCopyWithImpl<T, $Res>;
}

/// @nodoc
class _$CoreStateInitialCopyWithImpl<T, $Res>
    extends _$CoreStateCopyWithImpl<T, $Res>
    implements $CoreStateInitialCopyWith<T, $Res> {
  _$CoreStateInitialCopyWithImpl(
      CoreStateInitial<T> _value, $Res Function(CoreStateInitial<T>) _then)
      : super(_value, (v) => _then(v as CoreStateInitial<T>));

  @override
  CoreStateInitial<T> get _value => super._value as CoreStateInitial<T>;
}

/// @nodoc

class _$CoreStateInitial<T> implements CoreStateInitial<T> {
  const _$CoreStateInitial();

  @override
  String toString() {
    return 'CoreState<$T>.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is CoreStateInitial<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loadInProgress,
    required TResult Function(Failure dataFailure) fetchDataFailure,
    required TResult Function(T data) fetchDataSuccessfully,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loadInProgress,
    TResult Function(Failure dataFailure)? fetchDataFailure,
    TResult Function(T data)? fetchDataSuccessfully,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loadInProgress,
    TResult Function(Failure dataFailure)? fetchDataFailure,
    TResult Function(T data)? fetchDataSuccessfully,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CoreStateInitial<T> value) initial,
    required TResult Function(LoadInProgress<T> value) loadInProgress,
    required TResult Function(FetchDataFailure<T> value) fetchDataFailure,
    required TResult Function(FetchDataSuccessfully<T> value)
        fetchDataSuccessfully,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(CoreStateInitial<T> value)? initial,
    TResult Function(LoadInProgress<T> value)? loadInProgress,
    TResult Function(FetchDataFailure<T> value)? fetchDataFailure,
    TResult Function(FetchDataSuccessfully<T> value)? fetchDataSuccessfully,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CoreStateInitial<T> value)? initial,
    TResult Function(LoadInProgress<T> value)? loadInProgress,
    TResult Function(FetchDataFailure<T> value)? fetchDataFailure,
    TResult Function(FetchDataSuccessfully<T> value)? fetchDataSuccessfully,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class CoreStateInitial<T> implements CoreState<T> {
  const factory CoreStateInitial() = _$CoreStateInitial<T>;
}

/// @nodoc
abstract class $LoadInProgressCopyWith<T, $Res> {
  factory $LoadInProgressCopyWith(
          LoadInProgress<T> value, $Res Function(LoadInProgress<T>) then) =
      _$LoadInProgressCopyWithImpl<T, $Res>;
}

/// @nodoc
class _$LoadInProgressCopyWithImpl<T, $Res>
    extends _$CoreStateCopyWithImpl<T, $Res>
    implements $LoadInProgressCopyWith<T, $Res> {
  _$LoadInProgressCopyWithImpl(
      LoadInProgress<T> _value, $Res Function(LoadInProgress<T>) _then)
      : super(_value, (v) => _then(v as LoadInProgress<T>));

  @override
  LoadInProgress<T> get _value => super._value as LoadInProgress<T>;
}

/// @nodoc

class _$LoadInProgress<T> implements LoadInProgress<T> {
  const _$LoadInProgress();

  @override
  String toString() {
    return 'CoreState<$T>.loadInProgress()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is LoadInProgress<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loadInProgress,
    required TResult Function(Failure dataFailure) fetchDataFailure,
    required TResult Function(T data) fetchDataSuccessfully,
  }) {
    return loadInProgress();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loadInProgress,
    TResult Function(Failure dataFailure)? fetchDataFailure,
    TResult Function(T data)? fetchDataSuccessfully,
  }) {
    return loadInProgress?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loadInProgress,
    TResult Function(Failure dataFailure)? fetchDataFailure,
    TResult Function(T data)? fetchDataSuccessfully,
    required TResult orElse(),
  }) {
    if (loadInProgress != null) {
      return loadInProgress();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CoreStateInitial<T> value) initial,
    required TResult Function(LoadInProgress<T> value) loadInProgress,
    required TResult Function(FetchDataFailure<T> value) fetchDataFailure,
    required TResult Function(FetchDataSuccessfully<T> value)
        fetchDataSuccessfully,
  }) {
    return loadInProgress(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(CoreStateInitial<T> value)? initial,
    TResult Function(LoadInProgress<T> value)? loadInProgress,
    TResult Function(FetchDataFailure<T> value)? fetchDataFailure,
    TResult Function(FetchDataSuccessfully<T> value)? fetchDataSuccessfully,
  }) {
    return loadInProgress?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CoreStateInitial<T> value)? initial,
    TResult Function(LoadInProgress<T> value)? loadInProgress,
    TResult Function(FetchDataFailure<T> value)? fetchDataFailure,
    TResult Function(FetchDataSuccessfully<T> value)? fetchDataSuccessfully,
    required TResult orElse(),
  }) {
    if (loadInProgress != null) {
      return loadInProgress(this);
    }
    return orElse();
  }
}

abstract class LoadInProgress<T> implements CoreState<T> {
  const factory LoadInProgress() = _$LoadInProgress<T>;
}

/// @nodoc
abstract class $FetchDataFailureCopyWith<T, $Res> {
  factory $FetchDataFailureCopyWith(
          FetchDataFailure<T> value, $Res Function(FetchDataFailure<T>) then) =
      _$FetchDataFailureCopyWithImpl<T, $Res>;
  $Res call({Failure dataFailure});

  $FailureCopyWith<$Res> get dataFailure;
}

/// @nodoc
class _$FetchDataFailureCopyWithImpl<T, $Res>
    extends _$CoreStateCopyWithImpl<T, $Res>
    implements $FetchDataFailureCopyWith<T, $Res> {
  _$FetchDataFailureCopyWithImpl(
      FetchDataFailure<T> _value, $Res Function(FetchDataFailure<T>) _then)
      : super(_value, (v) => _then(v as FetchDataFailure<T>));

  @override
  FetchDataFailure<T> get _value => super._value as FetchDataFailure<T>;

  @override
  $Res call({
    Object? dataFailure = freezed,
  }) {
    return _then(FetchDataFailure<T>(
      dataFailure: dataFailure == freezed
          ? _value.dataFailure
          : dataFailure // ignore: cast_nullable_to_non_nullable
              as Failure,
    ));
  }

  @override
  $FailureCopyWith<$Res> get dataFailure {
    return $FailureCopyWith<$Res>(_value.dataFailure, (value) {
      return _then(_value.copyWith(dataFailure: value));
    });
  }
}

/// @nodoc

class _$FetchDataFailure<T> implements FetchDataFailure<T> {
  const _$FetchDataFailure({required this.dataFailure});

  @override
  final Failure dataFailure;

  @override
  String toString() {
    return 'CoreState<$T>.fetchDataFailure(dataFailure: $dataFailure)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is FetchDataFailure<T> &&
            const DeepCollectionEquality()
                .equals(other.dataFailure, dataFailure));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(dataFailure));

  @JsonKey(ignore: true)
  @override
  $FetchDataFailureCopyWith<T, FetchDataFailure<T>> get copyWith =>
      _$FetchDataFailureCopyWithImpl<T, FetchDataFailure<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loadInProgress,
    required TResult Function(Failure dataFailure) fetchDataFailure,
    required TResult Function(T data) fetchDataSuccessfully,
  }) {
    return fetchDataFailure(dataFailure);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loadInProgress,
    TResult Function(Failure dataFailure)? fetchDataFailure,
    TResult Function(T data)? fetchDataSuccessfully,
  }) {
    return fetchDataFailure?.call(dataFailure);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loadInProgress,
    TResult Function(Failure dataFailure)? fetchDataFailure,
    TResult Function(T data)? fetchDataSuccessfully,
    required TResult orElse(),
  }) {
    if (fetchDataFailure != null) {
      return fetchDataFailure(dataFailure);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CoreStateInitial<T> value) initial,
    required TResult Function(LoadInProgress<T> value) loadInProgress,
    required TResult Function(FetchDataFailure<T> value) fetchDataFailure,
    required TResult Function(FetchDataSuccessfully<T> value)
        fetchDataSuccessfully,
  }) {
    return fetchDataFailure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(CoreStateInitial<T> value)? initial,
    TResult Function(LoadInProgress<T> value)? loadInProgress,
    TResult Function(FetchDataFailure<T> value)? fetchDataFailure,
    TResult Function(FetchDataSuccessfully<T> value)? fetchDataSuccessfully,
  }) {
    return fetchDataFailure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CoreStateInitial<T> value)? initial,
    TResult Function(LoadInProgress<T> value)? loadInProgress,
    TResult Function(FetchDataFailure<T> value)? fetchDataFailure,
    TResult Function(FetchDataSuccessfully<T> value)? fetchDataSuccessfully,
    required TResult orElse(),
  }) {
    if (fetchDataFailure != null) {
      return fetchDataFailure(this);
    }
    return orElse();
  }
}

abstract class FetchDataFailure<T> implements CoreState<T> {
  const factory FetchDataFailure({required Failure dataFailure}) =
      _$FetchDataFailure<T>;

  Failure get dataFailure;
  @JsonKey(ignore: true)
  $FetchDataFailureCopyWith<T, FetchDataFailure<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FetchDataSuccessfullyCopyWith<T, $Res> {
  factory $FetchDataSuccessfullyCopyWith(FetchDataSuccessfully<T> value,
          $Res Function(FetchDataSuccessfully<T>) then) =
      _$FetchDataSuccessfullyCopyWithImpl<T, $Res>;
  $Res call({T data});
}

/// @nodoc
class _$FetchDataSuccessfullyCopyWithImpl<T, $Res>
    extends _$CoreStateCopyWithImpl<T, $Res>
    implements $FetchDataSuccessfullyCopyWith<T, $Res> {
  _$FetchDataSuccessfullyCopyWithImpl(FetchDataSuccessfully<T> _value,
      $Res Function(FetchDataSuccessfully<T>) _then)
      : super(_value, (v) => _then(v as FetchDataSuccessfully<T>));

  @override
  FetchDataSuccessfully<T> get _value =>
      super._value as FetchDataSuccessfully<T>;

  @override
  $Res call({
    Object? data = freezed,
  }) {
    return _then(FetchDataSuccessfully<T>(
      data: data == freezed
          ? _value.data
          : data // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$FetchDataSuccessfully<T> implements FetchDataSuccessfully<T> {
  const _$FetchDataSuccessfully({required this.data});

  @override
  final T data;

  @override
  String toString() {
    return 'CoreState<$T>.fetchDataSuccessfully(data: $data)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is FetchDataSuccessfully<T> &&
            const DeepCollectionEquality().equals(other.data, data));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(data));

  @JsonKey(ignore: true)
  @override
  $FetchDataSuccessfullyCopyWith<T, FetchDataSuccessfully<T>> get copyWith =>
      _$FetchDataSuccessfullyCopyWithImpl<T, FetchDataSuccessfully<T>>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loadInProgress,
    required TResult Function(Failure dataFailure) fetchDataFailure,
    required TResult Function(T data) fetchDataSuccessfully,
  }) {
    return fetchDataSuccessfully(data);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loadInProgress,
    TResult Function(Failure dataFailure)? fetchDataFailure,
    TResult Function(T data)? fetchDataSuccessfully,
  }) {
    return fetchDataSuccessfully?.call(data);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loadInProgress,
    TResult Function(Failure dataFailure)? fetchDataFailure,
    TResult Function(T data)? fetchDataSuccessfully,
    required TResult orElse(),
  }) {
    if (fetchDataSuccessfully != null) {
      return fetchDataSuccessfully(data);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CoreStateInitial<T> value) initial,
    required TResult Function(LoadInProgress<T> value) loadInProgress,
    required TResult Function(FetchDataFailure<T> value) fetchDataFailure,
    required TResult Function(FetchDataSuccessfully<T> value)
        fetchDataSuccessfully,
  }) {
    return fetchDataSuccessfully(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(CoreStateInitial<T> value)? initial,
    TResult Function(LoadInProgress<T> value)? loadInProgress,
    TResult Function(FetchDataFailure<T> value)? fetchDataFailure,
    TResult Function(FetchDataSuccessfully<T> value)? fetchDataSuccessfully,
  }) {
    return fetchDataSuccessfully?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CoreStateInitial<T> value)? initial,
    TResult Function(LoadInProgress<T> value)? loadInProgress,
    TResult Function(FetchDataFailure<T> value)? fetchDataFailure,
    TResult Function(FetchDataSuccessfully<T> value)? fetchDataSuccessfully,
    required TResult orElse(),
  }) {
    if (fetchDataSuccessfully != null) {
      return fetchDataSuccessfully(this);
    }
    return orElse();
  }
}

abstract class FetchDataSuccessfully<T> implements CoreState<T> {
  const factory FetchDataSuccessfully({required T data}) =
      _$FetchDataSuccessfully<T>;

  T get data;
  @JsonKey(ignore: true)
  $FetchDataSuccessfullyCopyWith<T, FetchDataSuccessfully<T>> get copyWith =>
      throw _privateConstructorUsedError;
}
