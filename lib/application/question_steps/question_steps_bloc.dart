import 'dart:async';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:nicode_charecter/domain/question/type/answers_type.dart';
import 'package:nicode_charecter/domain/question_steps/entity/question_steps_entity.dart';

part 'question_steps_bloc.freezed.dart';

part 'question_steps_event.dart';

class QuestionstepsBloc extends Bloc<QuestionstepsEvent, QuestionStepsEntity> {
  QuestionstepsBloc() : super(QuestionStepsEntity.initial());

  @override
  Stream<QuestionStepsEntity> mapEventToState(QuestionstepsEvent event) async* {
    yield* event.map(
      initial: (event) async* {
        yield QuestionStepsEntity.initial();
      },
      drinkWaterWithMouthChange: (event) async* {
        yield state.copyWith(
          drinkWaterWithMouth: event.answer,
        );
      },
      cheatingInExamChange: (event) async* {
        yield state.copyWith(
          cheatingInExam: event.answer,
        );
      },
      loveTheEmotionalMovieChange: (event) async* {
        yield state.copyWith(
          loveTheEmotionalMovie: event.answer,
        );
      },
      toBeImmortalChange: (event) async* {
        yield state.copyWith(
          toBeImmortal: event.answer,
        );
      },
      smokeCigarettesChange: (event) async* {
        yield state.copyWith(
          smokeCigarettes: event.answer,
        );
      },
      alwaysPresentInUniChange: (event) async* {
        yield state.copyWith(
          alwaysPresentInUni: event.answer,
        );
      },
      travelWithoutPermissionChange: (event) async* {
        yield state.copyWith(
          travelWithoutPermission: event.answer,
        );
      },
      addDataInStepList: (event) async* {
        final tempList = List.of(state.stepsData);
        tempList.add(event.data);
        yield state.copyWith(
          stepsData: tempList,
        );
      },
      removeFromStepListWitnIndex: (event) async* {
        final tempList = List.of(state.stepsData);
        tempList.removeRange(event.index, tempList.length);
        yield state.copyWith(
          stepsData: tempList,
        );
      },
    );
  }
}
