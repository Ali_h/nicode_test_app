part of 'question_steps_bloc.dart';

@freezed
class QuestionstepsEvent with _$QuestionstepsEvent {
  const factory QuestionstepsEvent.initial() = _QuestionstepsEventInitial;

  const factory QuestionstepsEvent.drinkWaterWithMouthChange(
      {required AnswersType? answer}) = _DrinkWaterWithMouthChange;

  const factory QuestionstepsEvent.cheatingInExamChange({required AnswersType? answer}) =
      _CheatingInExamChange;

  const factory QuestionstepsEvent.loveTheEmotionalMovieChange(
      {required AnswersType? answer}) = _LoveTheEmotionalMovieChange;

  const factory QuestionstepsEvent.toBeImmortalChange({required AnswersType? answer}) =
      _ToBeImmortalChange;

  const factory QuestionstepsEvent.smokeCigarettesChange({required AnswersType? answer}) =
      _SmokeCigarettesChange;

  const factory QuestionstepsEvent.alwaysPresentInUniChange(
      {required AnswersType? answer}) = _AlwaysPresentInUniChange;

  const factory QuestionstepsEvent.travelWithoutPermissionChange(
      {required AnswersType? answer}) = _TravelWithoutPermissionChange;

  const factory QuestionstepsEvent.addDataInStepList({required String data}) = _AddDataInStepList;

  const factory QuestionstepsEvent.removeFromStepListWitnIndex({required int index}) =
      _RemoveFromStepListWitnIndex;
}
