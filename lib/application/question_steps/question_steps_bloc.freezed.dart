// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'question_steps_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$QuestionstepsEventTearOff {
  const _$QuestionstepsEventTearOff();

  _QuestionstepsEventInitial initial() {
    return const _QuestionstepsEventInitial();
  }

  _DrinkWaterWithMouthChange drinkWaterWithMouthChange(
      {required AnswersType? answer}) {
    return _DrinkWaterWithMouthChange(
      answer: answer,
    );
  }

  _CheatingInExamChange cheatingInExamChange({required AnswersType? answer}) {
    return _CheatingInExamChange(
      answer: answer,
    );
  }

  _LoveTheEmotionalMovieChange loveTheEmotionalMovieChange(
      {required AnswersType? answer}) {
    return _LoveTheEmotionalMovieChange(
      answer: answer,
    );
  }

  _ToBeImmortalChange toBeImmortalChange({required AnswersType? answer}) {
    return _ToBeImmortalChange(
      answer: answer,
    );
  }

  _SmokeCigarettesChange smokeCigarettesChange({required AnswersType? answer}) {
    return _SmokeCigarettesChange(
      answer: answer,
    );
  }

  _AlwaysPresentInUniChange alwaysPresentInUniChange(
      {required AnswersType? answer}) {
    return _AlwaysPresentInUniChange(
      answer: answer,
    );
  }

  _TravelWithoutPermissionChange travelWithoutPermissionChange(
      {required AnswersType? answer}) {
    return _TravelWithoutPermissionChange(
      answer: answer,
    );
  }

  _AddDataInStepList addDataInStepList({required String data}) {
    return _AddDataInStepList(
      data: data,
    );
  }

  _RemoveFromStepListWitnIndex removeFromStepListWitnIndex(
      {required int index}) {
    return _RemoveFromStepListWitnIndex(
      index: index,
    );
  }
}

/// @nodoc
const $QuestionstepsEvent = _$QuestionstepsEventTearOff();

/// @nodoc
mixin _$QuestionstepsEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(AnswersType? answer) drinkWaterWithMouthChange,
    required TResult Function(AnswersType? answer) cheatingInExamChange,
    required TResult Function(AnswersType? answer) loveTheEmotionalMovieChange,
    required TResult Function(AnswersType? answer) toBeImmortalChange,
    required TResult Function(AnswersType? answer) smokeCigarettesChange,
    required TResult Function(AnswersType? answer) alwaysPresentInUniChange,
    required TResult Function(AnswersType? answer)
        travelWithoutPermissionChange,
    required TResult Function(String data) addDataInStepList,
    required TResult Function(int index) removeFromStepListWitnIndex,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(AnswersType? answer)? drinkWaterWithMouthChange,
    TResult Function(AnswersType? answer)? cheatingInExamChange,
    TResult Function(AnswersType? answer)? loveTheEmotionalMovieChange,
    TResult Function(AnswersType? answer)? toBeImmortalChange,
    TResult Function(AnswersType? answer)? smokeCigarettesChange,
    TResult Function(AnswersType? answer)? alwaysPresentInUniChange,
    TResult Function(AnswersType? answer)? travelWithoutPermissionChange,
    TResult Function(String data)? addDataInStepList,
    TResult Function(int index)? removeFromStepListWitnIndex,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(AnswersType? answer)? drinkWaterWithMouthChange,
    TResult Function(AnswersType? answer)? cheatingInExamChange,
    TResult Function(AnswersType? answer)? loveTheEmotionalMovieChange,
    TResult Function(AnswersType? answer)? toBeImmortalChange,
    TResult Function(AnswersType? answer)? smokeCigarettesChange,
    TResult Function(AnswersType? answer)? alwaysPresentInUniChange,
    TResult Function(AnswersType? answer)? travelWithoutPermissionChange,
    TResult Function(String data)? addDataInStepList,
    TResult Function(int index)? removeFromStepListWitnIndex,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_QuestionstepsEventInitial value) initial,
    required TResult Function(_DrinkWaterWithMouthChange value)
        drinkWaterWithMouthChange,
    required TResult Function(_CheatingInExamChange value) cheatingInExamChange,
    required TResult Function(_LoveTheEmotionalMovieChange value)
        loveTheEmotionalMovieChange,
    required TResult Function(_ToBeImmortalChange value) toBeImmortalChange,
    required TResult Function(_SmokeCigarettesChange value)
        smokeCigarettesChange,
    required TResult Function(_AlwaysPresentInUniChange value)
        alwaysPresentInUniChange,
    required TResult Function(_TravelWithoutPermissionChange value)
        travelWithoutPermissionChange,
    required TResult Function(_AddDataInStepList value) addDataInStepList,
    required TResult Function(_RemoveFromStepListWitnIndex value)
        removeFromStepListWitnIndex,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_QuestionstepsEventInitial value)? initial,
    TResult Function(_DrinkWaterWithMouthChange value)?
        drinkWaterWithMouthChange,
    TResult Function(_CheatingInExamChange value)? cheatingInExamChange,
    TResult Function(_LoveTheEmotionalMovieChange value)?
        loveTheEmotionalMovieChange,
    TResult Function(_ToBeImmortalChange value)? toBeImmortalChange,
    TResult Function(_SmokeCigarettesChange value)? smokeCigarettesChange,
    TResult Function(_AlwaysPresentInUniChange value)? alwaysPresentInUniChange,
    TResult Function(_TravelWithoutPermissionChange value)?
        travelWithoutPermissionChange,
    TResult Function(_AddDataInStepList value)? addDataInStepList,
    TResult Function(_RemoveFromStepListWitnIndex value)?
        removeFromStepListWitnIndex,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_QuestionstepsEventInitial value)? initial,
    TResult Function(_DrinkWaterWithMouthChange value)?
        drinkWaterWithMouthChange,
    TResult Function(_CheatingInExamChange value)? cheatingInExamChange,
    TResult Function(_LoveTheEmotionalMovieChange value)?
        loveTheEmotionalMovieChange,
    TResult Function(_ToBeImmortalChange value)? toBeImmortalChange,
    TResult Function(_SmokeCigarettesChange value)? smokeCigarettesChange,
    TResult Function(_AlwaysPresentInUniChange value)? alwaysPresentInUniChange,
    TResult Function(_TravelWithoutPermissionChange value)?
        travelWithoutPermissionChange,
    TResult Function(_AddDataInStepList value)? addDataInStepList,
    TResult Function(_RemoveFromStepListWitnIndex value)?
        removeFromStepListWitnIndex,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $QuestionstepsEventCopyWith<$Res> {
  factory $QuestionstepsEventCopyWith(
          QuestionstepsEvent value, $Res Function(QuestionstepsEvent) then) =
      _$QuestionstepsEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$QuestionstepsEventCopyWithImpl<$Res>
    implements $QuestionstepsEventCopyWith<$Res> {
  _$QuestionstepsEventCopyWithImpl(this._value, this._then);

  final QuestionstepsEvent _value;
  // ignore: unused_field
  final $Res Function(QuestionstepsEvent) _then;
}

/// @nodoc
abstract class _$QuestionstepsEventInitialCopyWith<$Res> {
  factory _$QuestionstepsEventInitialCopyWith(_QuestionstepsEventInitial value,
          $Res Function(_QuestionstepsEventInitial) then) =
      __$QuestionstepsEventInitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$QuestionstepsEventInitialCopyWithImpl<$Res>
    extends _$QuestionstepsEventCopyWithImpl<$Res>
    implements _$QuestionstepsEventInitialCopyWith<$Res> {
  __$QuestionstepsEventInitialCopyWithImpl(_QuestionstepsEventInitial _value,
      $Res Function(_QuestionstepsEventInitial) _then)
      : super(_value, (v) => _then(v as _QuestionstepsEventInitial));

  @override
  _QuestionstepsEventInitial get _value =>
      super._value as _QuestionstepsEventInitial;
}

/// @nodoc

class _$_QuestionstepsEventInitial implements _QuestionstepsEventInitial {
  const _$_QuestionstepsEventInitial();

  @override
  String toString() {
    return 'QuestionstepsEvent.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _QuestionstepsEventInitial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(AnswersType? answer) drinkWaterWithMouthChange,
    required TResult Function(AnswersType? answer) cheatingInExamChange,
    required TResult Function(AnswersType? answer) loveTheEmotionalMovieChange,
    required TResult Function(AnswersType? answer) toBeImmortalChange,
    required TResult Function(AnswersType? answer) smokeCigarettesChange,
    required TResult Function(AnswersType? answer) alwaysPresentInUniChange,
    required TResult Function(AnswersType? answer)
        travelWithoutPermissionChange,
    required TResult Function(String data) addDataInStepList,
    required TResult Function(int index) removeFromStepListWitnIndex,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(AnswersType? answer)? drinkWaterWithMouthChange,
    TResult Function(AnswersType? answer)? cheatingInExamChange,
    TResult Function(AnswersType? answer)? loveTheEmotionalMovieChange,
    TResult Function(AnswersType? answer)? toBeImmortalChange,
    TResult Function(AnswersType? answer)? smokeCigarettesChange,
    TResult Function(AnswersType? answer)? alwaysPresentInUniChange,
    TResult Function(AnswersType? answer)? travelWithoutPermissionChange,
    TResult Function(String data)? addDataInStepList,
    TResult Function(int index)? removeFromStepListWitnIndex,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(AnswersType? answer)? drinkWaterWithMouthChange,
    TResult Function(AnswersType? answer)? cheatingInExamChange,
    TResult Function(AnswersType? answer)? loveTheEmotionalMovieChange,
    TResult Function(AnswersType? answer)? toBeImmortalChange,
    TResult Function(AnswersType? answer)? smokeCigarettesChange,
    TResult Function(AnswersType? answer)? alwaysPresentInUniChange,
    TResult Function(AnswersType? answer)? travelWithoutPermissionChange,
    TResult Function(String data)? addDataInStepList,
    TResult Function(int index)? removeFromStepListWitnIndex,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_QuestionstepsEventInitial value) initial,
    required TResult Function(_DrinkWaterWithMouthChange value)
        drinkWaterWithMouthChange,
    required TResult Function(_CheatingInExamChange value) cheatingInExamChange,
    required TResult Function(_LoveTheEmotionalMovieChange value)
        loveTheEmotionalMovieChange,
    required TResult Function(_ToBeImmortalChange value) toBeImmortalChange,
    required TResult Function(_SmokeCigarettesChange value)
        smokeCigarettesChange,
    required TResult Function(_AlwaysPresentInUniChange value)
        alwaysPresentInUniChange,
    required TResult Function(_TravelWithoutPermissionChange value)
        travelWithoutPermissionChange,
    required TResult Function(_AddDataInStepList value) addDataInStepList,
    required TResult Function(_RemoveFromStepListWitnIndex value)
        removeFromStepListWitnIndex,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_QuestionstepsEventInitial value)? initial,
    TResult Function(_DrinkWaterWithMouthChange value)?
        drinkWaterWithMouthChange,
    TResult Function(_CheatingInExamChange value)? cheatingInExamChange,
    TResult Function(_LoveTheEmotionalMovieChange value)?
        loveTheEmotionalMovieChange,
    TResult Function(_ToBeImmortalChange value)? toBeImmortalChange,
    TResult Function(_SmokeCigarettesChange value)? smokeCigarettesChange,
    TResult Function(_AlwaysPresentInUniChange value)? alwaysPresentInUniChange,
    TResult Function(_TravelWithoutPermissionChange value)?
        travelWithoutPermissionChange,
    TResult Function(_AddDataInStepList value)? addDataInStepList,
    TResult Function(_RemoveFromStepListWitnIndex value)?
        removeFromStepListWitnIndex,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_QuestionstepsEventInitial value)? initial,
    TResult Function(_DrinkWaterWithMouthChange value)?
        drinkWaterWithMouthChange,
    TResult Function(_CheatingInExamChange value)? cheatingInExamChange,
    TResult Function(_LoveTheEmotionalMovieChange value)?
        loveTheEmotionalMovieChange,
    TResult Function(_ToBeImmortalChange value)? toBeImmortalChange,
    TResult Function(_SmokeCigarettesChange value)? smokeCigarettesChange,
    TResult Function(_AlwaysPresentInUniChange value)? alwaysPresentInUniChange,
    TResult Function(_TravelWithoutPermissionChange value)?
        travelWithoutPermissionChange,
    TResult Function(_AddDataInStepList value)? addDataInStepList,
    TResult Function(_RemoveFromStepListWitnIndex value)?
        removeFromStepListWitnIndex,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _QuestionstepsEventInitial implements QuestionstepsEvent {
  const factory _QuestionstepsEventInitial() = _$_QuestionstepsEventInitial;
}

/// @nodoc
abstract class _$DrinkWaterWithMouthChangeCopyWith<$Res> {
  factory _$DrinkWaterWithMouthChangeCopyWith(_DrinkWaterWithMouthChange value,
          $Res Function(_DrinkWaterWithMouthChange) then) =
      __$DrinkWaterWithMouthChangeCopyWithImpl<$Res>;
  $Res call({AnswersType? answer});

  $AnswersTypeCopyWith<$Res>? get answer;
}

/// @nodoc
class __$DrinkWaterWithMouthChangeCopyWithImpl<$Res>
    extends _$QuestionstepsEventCopyWithImpl<$Res>
    implements _$DrinkWaterWithMouthChangeCopyWith<$Res> {
  __$DrinkWaterWithMouthChangeCopyWithImpl(_DrinkWaterWithMouthChange _value,
      $Res Function(_DrinkWaterWithMouthChange) _then)
      : super(_value, (v) => _then(v as _DrinkWaterWithMouthChange));

  @override
  _DrinkWaterWithMouthChange get _value =>
      super._value as _DrinkWaterWithMouthChange;

  @override
  $Res call({
    Object? answer = freezed,
  }) {
    return _then(_DrinkWaterWithMouthChange(
      answer: answer == freezed
          ? _value.answer
          : answer // ignore: cast_nullable_to_non_nullable
              as AnswersType?,
    ));
  }

  @override
  $AnswersTypeCopyWith<$Res>? get answer {
    if (_value.answer == null) {
      return null;
    }

    return $AnswersTypeCopyWith<$Res>(_value.answer!, (value) {
      return _then(_value.copyWith(answer: value));
    });
  }
}

/// @nodoc

class _$_DrinkWaterWithMouthChange implements _DrinkWaterWithMouthChange {
  const _$_DrinkWaterWithMouthChange({required this.answer});

  @override
  final AnswersType? answer;

  @override
  String toString() {
    return 'QuestionstepsEvent.drinkWaterWithMouthChange(answer: $answer)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _DrinkWaterWithMouthChange &&
            const DeepCollectionEquality().equals(other.answer, answer));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(answer));

  @JsonKey(ignore: true)
  @override
  _$DrinkWaterWithMouthChangeCopyWith<_DrinkWaterWithMouthChange>
      get copyWith =>
          __$DrinkWaterWithMouthChangeCopyWithImpl<_DrinkWaterWithMouthChange>(
              this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(AnswersType? answer) drinkWaterWithMouthChange,
    required TResult Function(AnswersType? answer) cheatingInExamChange,
    required TResult Function(AnswersType? answer) loveTheEmotionalMovieChange,
    required TResult Function(AnswersType? answer) toBeImmortalChange,
    required TResult Function(AnswersType? answer) smokeCigarettesChange,
    required TResult Function(AnswersType? answer) alwaysPresentInUniChange,
    required TResult Function(AnswersType? answer)
        travelWithoutPermissionChange,
    required TResult Function(String data) addDataInStepList,
    required TResult Function(int index) removeFromStepListWitnIndex,
  }) {
    return drinkWaterWithMouthChange(answer);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(AnswersType? answer)? drinkWaterWithMouthChange,
    TResult Function(AnswersType? answer)? cheatingInExamChange,
    TResult Function(AnswersType? answer)? loveTheEmotionalMovieChange,
    TResult Function(AnswersType? answer)? toBeImmortalChange,
    TResult Function(AnswersType? answer)? smokeCigarettesChange,
    TResult Function(AnswersType? answer)? alwaysPresentInUniChange,
    TResult Function(AnswersType? answer)? travelWithoutPermissionChange,
    TResult Function(String data)? addDataInStepList,
    TResult Function(int index)? removeFromStepListWitnIndex,
  }) {
    return drinkWaterWithMouthChange?.call(answer);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(AnswersType? answer)? drinkWaterWithMouthChange,
    TResult Function(AnswersType? answer)? cheatingInExamChange,
    TResult Function(AnswersType? answer)? loveTheEmotionalMovieChange,
    TResult Function(AnswersType? answer)? toBeImmortalChange,
    TResult Function(AnswersType? answer)? smokeCigarettesChange,
    TResult Function(AnswersType? answer)? alwaysPresentInUniChange,
    TResult Function(AnswersType? answer)? travelWithoutPermissionChange,
    TResult Function(String data)? addDataInStepList,
    TResult Function(int index)? removeFromStepListWitnIndex,
    required TResult orElse(),
  }) {
    if (drinkWaterWithMouthChange != null) {
      return drinkWaterWithMouthChange(answer);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_QuestionstepsEventInitial value) initial,
    required TResult Function(_DrinkWaterWithMouthChange value)
        drinkWaterWithMouthChange,
    required TResult Function(_CheatingInExamChange value) cheatingInExamChange,
    required TResult Function(_LoveTheEmotionalMovieChange value)
        loveTheEmotionalMovieChange,
    required TResult Function(_ToBeImmortalChange value) toBeImmortalChange,
    required TResult Function(_SmokeCigarettesChange value)
        smokeCigarettesChange,
    required TResult Function(_AlwaysPresentInUniChange value)
        alwaysPresentInUniChange,
    required TResult Function(_TravelWithoutPermissionChange value)
        travelWithoutPermissionChange,
    required TResult Function(_AddDataInStepList value) addDataInStepList,
    required TResult Function(_RemoveFromStepListWitnIndex value)
        removeFromStepListWitnIndex,
  }) {
    return drinkWaterWithMouthChange(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_QuestionstepsEventInitial value)? initial,
    TResult Function(_DrinkWaterWithMouthChange value)?
        drinkWaterWithMouthChange,
    TResult Function(_CheatingInExamChange value)? cheatingInExamChange,
    TResult Function(_LoveTheEmotionalMovieChange value)?
        loveTheEmotionalMovieChange,
    TResult Function(_ToBeImmortalChange value)? toBeImmortalChange,
    TResult Function(_SmokeCigarettesChange value)? smokeCigarettesChange,
    TResult Function(_AlwaysPresentInUniChange value)? alwaysPresentInUniChange,
    TResult Function(_TravelWithoutPermissionChange value)?
        travelWithoutPermissionChange,
    TResult Function(_AddDataInStepList value)? addDataInStepList,
    TResult Function(_RemoveFromStepListWitnIndex value)?
        removeFromStepListWitnIndex,
  }) {
    return drinkWaterWithMouthChange?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_QuestionstepsEventInitial value)? initial,
    TResult Function(_DrinkWaterWithMouthChange value)?
        drinkWaterWithMouthChange,
    TResult Function(_CheatingInExamChange value)? cheatingInExamChange,
    TResult Function(_LoveTheEmotionalMovieChange value)?
        loveTheEmotionalMovieChange,
    TResult Function(_ToBeImmortalChange value)? toBeImmortalChange,
    TResult Function(_SmokeCigarettesChange value)? smokeCigarettesChange,
    TResult Function(_AlwaysPresentInUniChange value)? alwaysPresentInUniChange,
    TResult Function(_TravelWithoutPermissionChange value)?
        travelWithoutPermissionChange,
    TResult Function(_AddDataInStepList value)? addDataInStepList,
    TResult Function(_RemoveFromStepListWitnIndex value)?
        removeFromStepListWitnIndex,
    required TResult orElse(),
  }) {
    if (drinkWaterWithMouthChange != null) {
      return drinkWaterWithMouthChange(this);
    }
    return orElse();
  }
}

abstract class _DrinkWaterWithMouthChange implements QuestionstepsEvent {
  const factory _DrinkWaterWithMouthChange({required AnswersType? answer}) =
      _$_DrinkWaterWithMouthChange;

  AnswersType? get answer;
  @JsonKey(ignore: true)
  _$DrinkWaterWithMouthChangeCopyWith<_DrinkWaterWithMouthChange>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$CheatingInExamChangeCopyWith<$Res> {
  factory _$CheatingInExamChangeCopyWith(_CheatingInExamChange value,
          $Res Function(_CheatingInExamChange) then) =
      __$CheatingInExamChangeCopyWithImpl<$Res>;
  $Res call({AnswersType? answer});

  $AnswersTypeCopyWith<$Res>? get answer;
}

/// @nodoc
class __$CheatingInExamChangeCopyWithImpl<$Res>
    extends _$QuestionstepsEventCopyWithImpl<$Res>
    implements _$CheatingInExamChangeCopyWith<$Res> {
  __$CheatingInExamChangeCopyWithImpl(
      _CheatingInExamChange _value, $Res Function(_CheatingInExamChange) _then)
      : super(_value, (v) => _then(v as _CheatingInExamChange));

  @override
  _CheatingInExamChange get _value => super._value as _CheatingInExamChange;

  @override
  $Res call({
    Object? answer = freezed,
  }) {
    return _then(_CheatingInExamChange(
      answer: answer == freezed
          ? _value.answer
          : answer // ignore: cast_nullable_to_non_nullable
              as AnswersType?,
    ));
  }

  @override
  $AnswersTypeCopyWith<$Res>? get answer {
    if (_value.answer == null) {
      return null;
    }

    return $AnswersTypeCopyWith<$Res>(_value.answer!, (value) {
      return _then(_value.copyWith(answer: value));
    });
  }
}

/// @nodoc

class _$_CheatingInExamChange implements _CheatingInExamChange {
  const _$_CheatingInExamChange({required this.answer});

  @override
  final AnswersType? answer;

  @override
  String toString() {
    return 'QuestionstepsEvent.cheatingInExamChange(answer: $answer)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _CheatingInExamChange &&
            const DeepCollectionEquality().equals(other.answer, answer));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(answer));

  @JsonKey(ignore: true)
  @override
  _$CheatingInExamChangeCopyWith<_CheatingInExamChange> get copyWith =>
      __$CheatingInExamChangeCopyWithImpl<_CheatingInExamChange>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(AnswersType? answer) drinkWaterWithMouthChange,
    required TResult Function(AnswersType? answer) cheatingInExamChange,
    required TResult Function(AnswersType? answer) loveTheEmotionalMovieChange,
    required TResult Function(AnswersType? answer) toBeImmortalChange,
    required TResult Function(AnswersType? answer) smokeCigarettesChange,
    required TResult Function(AnswersType? answer) alwaysPresentInUniChange,
    required TResult Function(AnswersType? answer)
        travelWithoutPermissionChange,
    required TResult Function(String data) addDataInStepList,
    required TResult Function(int index) removeFromStepListWitnIndex,
  }) {
    return cheatingInExamChange(answer);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(AnswersType? answer)? drinkWaterWithMouthChange,
    TResult Function(AnswersType? answer)? cheatingInExamChange,
    TResult Function(AnswersType? answer)? loveTheEmotionalMovieChange,
    TResult Function(AnswersType? answer)? toBeImmortalChange,
    TResult Function(AnswersType? answer)? smokeCigarettesChange,
    TResult Function(AnswersType? answer)? alwaysPresentInUniChange,
    TResult Function(AnswersType? answer)? travelWithoutPermissionChange,
    TResult Function(String data)? addDataInStepList,
    TResult Function(int index)? removeFromStepListWitnIndex,
  }) {
    return cheatingInExamChange?.call(answer);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(AnswersType? answer)? drinkWaterWithMouthChange,
    TResult Function(AnswersType? answer)? cheatingInExamChange,
    TResult Function(AnswersType? answer)? loveTheEmotionalMovieChange,
    TResult Function(AnswersType? answer)? toBeImmortalChange,
    TResult Function(AnswersType? answer)? smokeCigarettesChange,
    TResult Function(AnswersType? answer)? alwaysPresentInUniChange,
    TResult Function(AnswersType? answer)? travelWithoutPermissionChange,
    TResult Function(String data)? addDataInStepList,
    TResult Function(int index)? removeFromStepListWitnIndex,
    required TResult orElse(),
  }) {
    if (cheatingInExamChange != null) {
      return cheatingInExamChange(answer);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_QuestionstepsEventInitial value) initial,
    required TResult Function(_DrinkWaterWithMouthChange value)
        drinkWaterWithMouthChange,
    required TResult Function(_CheatingInExamChange value) cheatingInExamChange,
    required TResult Function(_LoveTheEmotionalMovieChange value)
        loveTheEmotionalMovieChange,
    required TResult Function(_ToBeImmortalChange value) toBeImmortalChange,
    required TResult Function(_SmokeCigarettesChange value)
        smokeCigarettesChange,
    required TResult Function(_AlwaysPresentInUniChange value)
        alwaysPresentInUniChange,
    required TResult Function(_TravelWithoutPermissionChange value)
        travelWithoutPermissionChange,
    required TResult Function(_AddDataInStepList value) addDataInStepList,
    required TResult Function(_RemoveFromStepListWitnIndex value)
        removeFromStepListWitnIndex,
  }) {
    return cheatingInExamChange(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_QuestionstepsEventInitial value)? initial,
    TResult Function(_DrinkWaterWithMouthChange value)?
        drinkWaterWithMouthChange,
    TResult Function(_CheatingInExamChange value)? cheatingInExamChange,
    TResult Function(_LoveTheEmotionalMovieChange value)?
        loveTheEmotionalMovieChange,
    TResult Function(_ToBeImmortalChange value)? toBeImmortalChange,
    TResult Function(_SmokeCigarettesChange value)? smokeCigarettesChange,
    TResult Function(_AlwaysPresentInUniChange value)? alwaysPresentInUniChange,
    TResult Function(_TravelWithoutPermissionChange value)?
        travelWithoutPermissionChange,
    TResult Function(_AddDataInStepList value)? addDataInStepList,
    TResult Function(_RemoveFromStepListWitnIndex value)?
        removeFromStepListWitnIndex,
  }) {
    return cheatingInExamChange?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_QuestionstepsEventInitial value)? initial,
    TResult Function(_DrinkWaterWithMouthChange value)?
        drinkWaterWithMouthChange,
    TResult Function(_CheatingInExamChange value)? cheatingInExamChange,
    TResult Function(_LoveTheEmotionalMovieChange value)?
        loveTheEmotionalMovieChange,
    TResult Function(_ToBeImmortalChange value)? toBeImmortalChange,
    TResult Function(_SmokeCigarettesChange value)? smokeCigarettesChange,
    TResult Function(_AlwaysPresentInUniChange value)? alwaysPresentInUniChange,
    TResult Function(_TravelWithoutPermissionChange value)?
        travelWithoutPermissionChange,
    TResult Function(_AddDataInStepList value)? addDataInStepList,
    TResult Function(_RemoveFromStepListWitnIndex value)?
        removeFromStepListWitnIndex,
    required TResult orElse(),
  }) {
    if (cheatingInExamChange != null) {
      return cheatingInExamChange(this);
    }
    return orElse();
  }
}

abstract class _CheatingInExamChange implements QuestionstepsEvent {
  const factory _CheatingInExamChange({required AnswersType? answer}) =
      _$_CheatingInExamChange;

  AnswersType? get answer;
  @JsonKey(ignore: true)
  _$CheatingInExamChangeCopyWith<_CheatingInExamChange> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$LoveTheEmotionalMovieChangeCopyWith<$Res> {
  factory _$LoveTheEmotionalMovieChangeCopyWith(
          _LoveTheEmotionalMovieChange value,
          $Res Function(_LoveTheEmotionalMovieChange) then) =
      __$LoveTheEmotionalMovieChangeCopyWithImpl<$Res>;
  $Res call({AnswersType? answer});

  $AnswersTypeCopyWith<$Res>? get answer;
}

/// @nodoc
class __$LoveTheEmotionalMovieChangeCopyWithImpl<$Res>
    extends _$QuestionstepsEventCopyWithImpl<$Res>
    implements _$LoveTheEmotionalMovieChangeCopyWith<$Res> {
  __$LoveTheEmotionalMovieChangeCopyWithImpl(
      _LoveTheEmotionalMovieChange _value,
      $Res Function(_LoveTheEmotionalMovieChange) _then)
      : super(_value, (v) => _then(v as _LoveTheEmotionalMovieChange));

  @override
  _LoveTheEmotionalMovieChange get _value =>
      super._value as _LoveTheEmotionalMovieChange;

  @override
  $Res call({
    Object? answer = freezed,
  }) {
    return _then(_LoveTheEmotionalMovieChange(
      answer: answer == freezed
          ? _value.answer
          : answer // ignore: cast_nullable_to_non_nullable
              as AnswersType?,
    ));
  }

  @override
  $AnswersTypeCopyWith<$Res>? get answer {
    if (_value.answer == null) {
      return null;
    }

    return $AnswersTypeCopyWith<$Res>(_value.answer!, (value) {
      return _then(_value.copyWith(answer: value));
    });
  }
}

/// @nodoc

class _$_LoveTheEmotionalMovieChange implements _LoveTheEmotionalMovieChange {
  const _$_LoveTheEmotionalMovieChange({required this.answer});

  @override
  final AnswersType? answer;

  @override
  String toString() {
    return 'QuestionstepsEvent.loveTheEmotionalMovieChange(answer: $answer)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _LoveTheEmotionalMovieChange &&
            const DeepCollectionEquality().equals(other.answer, answer));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(answer));

  @JsonKey(ignore: true)
  @override
  _$LoveTheEmotionalMovieChangeCopyWith<_LoveTheEmotionalMovieChange>
      get copyWith => __$LoveTheEmotionalMovieChangeCopyWithImpl<
          _LoveTheEmotionalMovieChange>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(AnswersType? answer) drinkWaterWithMouthChange,
    required TResult Function(AnswersType? answer) cheatingInExamChange,
    required TResult Function(AnswersType? answer) loveTheEmotionalMovieChange,
    required TResult Function(AnswersType? answer) toBeImmortalChange,
    required TResult Function(AnswersType? answer) smokeCigarettesChange,
    required TResult Function(AnswersType? answer) alwaysPresentInUniChange,
    required TResult Function(AnswersType? answer)
        travelWithoutPermissionChange,
    required TResult Function(String data) addDataInStepList,
    required TResult Function(int index) removeFromStepListWitnIndex,
  }) {
    return loveTheEmotionalMovieChange(answer);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(AnswersType? answer)? drinkWaterWithMouthChange,
    TResult Function(AnswersType? answer)? cheatingInExamChange,
    TResult Function(AnswersType? answer)? loveTheEmotionalMovieChange,
    TResult Function(AnswersType? answer)? toBeImmortalChange,
    TResult Function(AnswersType? answer)? smokeCigarettesChange,
    TResult Function(AnswersType? answer)? alwaysPresentInUniChange,
    TResult Function(AnswersType? answer)? travelWithoutPermissionChange,
    TResult Function(String data)? addDataInStepList,
    TResult Function(int index)? removeFromStepListWitnIndex,
  }) {
    return loveTheEmotionalMovieChange?.call(answer);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(AnswersType? answer)? drinkWaterWithMouthChange,
    TResult Function(AnswersType? answer)? cheatingInExamChange,
    TResult Function(AnswersType? answer)? loveTheEmotionalMovieChange,
    TResult Function(AnswersType? answer)? toBeImmortalChange,
    TResult Function(AnswersType? answer)? smokeCigarettesChange,
    TResult Function(AnswersType? answer)? alwaysPresentInUniChange,
    TResult Function(AnswersType? answer)? travelWithoutPermissionChange,
    TResult Function(String data)? addDataInStepList,
    TResult Function(int index)? removeFromStepListWitnIndex,
    required TResult orElse(),
  }) {
    if (loveTheEmotionalMovieChange != null) {
      return loveTheEmotionalMovieChange(answer);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_QuestionstepsEventInitial value) initial,
    required TResult Function(_DrinkWaterWithMouthChange value)
        drinkWaterWithMouthChange,
    required TResult Function(_CheatingInExamChange value) cheatingInExamChange,
    required TResult Function(_LoveTheEmotionalMovieChange value)
        loveTheEmotionalMovieChange,
    required TResult Function(_ToBeImmortalChange value) toBeImmortalChange,
    required TResult Function(_SmokeCigarettesChange value)
        smokeCigarettesChange,
    required TResult Function(_AlwaysPresentInUniChange value)
        alwaysPresentInUniChange,
    required TResult Function(_TravelWithoutPermissionChange value)
        travelWithoutPermissionChange,
    required TResult Function(_AddDataInStepList value) addDataInStepList,
    required TResult Function(_RemoveFromStepListWitnIndex value)
        removeFromStepListWitnIndex,
  }) {
    return loveTheEmotionalMovieChange(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_QuestionstepsEventInitial value)? initial,
    TResult Function(_DrinkWaterWithMouthChange value)?
        drinkWaterWithMouthChange,
    TResult Function(_CheatingInExamChange value)? cheatingInExamChange,
    TResult Function(_LoveTheEmotionalMovieChange value)?
        loveTheEmotionalMovieChange,
    TResult Function(_ToBeImmortalChange value)? toBeImmortalChange,
    TResult Function(_SmokeCigarettesChange value)? smokeCigarettesChange,
    TResult Function(_AlwaysPresentInUniChange value)? alwaysPresentInUniChange,
    TResult Function(_TravelWithoutPermissionChange value)?
        travelWithoutPermissionChange,
    TResult Function(_AddDataInStepList value)? addDataInStepList,
    TResult Function(_RemoveFromStepListWitnIndex value)?
        removeFromStepListWitnIndex,
  }) {
    return loveTheEmotionalMovieChange?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_QuestionstepsEventInitial value)? initial,
    TResult Function(_DrinkWaterWithMouthChange value)?
        drinkWaterWithMouthChange,
    TResult Function(_CheatingInExamChange value)? cheatingInExamChange,
    TResult Function(_LoveTheEmotionalMovieChange value)?
        loveTheEmotionalMovieChange,
    TResult Function(_ToBeImmortalChange value)? toBeImmortalChange,
    TResult Function(_SmokeCigarettesChange value)? smokeCigarettesChange,
    TResult Function(_AlwaysPresentInUniChange value)? alwaysPresentInUniChange,
    TResult Function(_TravelWithoutPermissionChange value)?
        travelWithoutPermissionChange,
    TResult Function(_AddDataInStepList value)? addDataInStepList,
    TResult Function(_RemoveFromStepListWitnIndex value)?
        removeFromStepListWitnIndex,
    required TResult orElse(),
  }) {
    if (loveTheEmotionalMovieChange != null) {
      return loveTheEmotionalMovieChange(this);
    }
    return orElse();
  }
}

abstract class _LoveTheEmotionalMovieChange implements QuestionstepsEvent {
  const factory _LoveTheEmotionalMovieChange({required AnswersType? answer}) =
      _$_LoveTheEmotionalMovieChange;

  AnswersType? get answer;
  @JsonKey(ignore: true)
  _$LoveTheEmotionalMovieChangeCopyWith<_LoveTheEmotionalMovieChange>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$ToBeImmortalChangeCopyWith<$Res> {
  factory _$ToBeImmortalChangeCopyWith(
          _ToBeImmortalChange value, $Res Function(_ToBeImmortalChange) then) =
      __$ToBeImmortalChangeCopyWithImpl<$Res>;
  $Res call({AnswersType? answer});

  $AnswersTypeCopyWith<$Res>? get answer;
}

/// @nodoc
class __$ToBeImmortalChangeCopyWithImpl<$Res>
    extends _$QuestionstepsEventCopyWithImpl<$Res>
    implements _$ToBeImmortalChangeCopyWith<$Res> {
  __$ToBeImmortalChangeCopyWithImpl(
      _ToBeImmortalChange _value, $Res Function(_ToBeImmortalChange) _then)
      : super(_value, (v) => _then(v as _ToBeImmortalChange));

  @override
  _ToBeImmortalChange get _value => super._value as _ToBeImmortalChange;

  @override
  $Res call({
    Object? answer = freezed,
  }) {
    return _then(_ToBeImmortalChange(
      answer: answer == freezed
          ? _value.answer
          : answer // ignore: cast_nullable_to_non_nullable
              as AnswersType?,
    ));
  }

  @override
  $AnswersTypeCopyWith<$Res>? get answer {
    if (_value.answer == null) {
      return null;
    }

    return $AnswersTypeCopyWith<$Res>(_value.answer!, (value) {
      return _then(_value.copyWith(answer: value));
    });
  }
}

/// @nodoc

class _$_ToBeImmortalChange implements _ToBeImmortalChange {
  const _$_ToBeImmortalChange({required this.answer});

  @override
  final AnswersType? answer;

  @override
  String toString() {
    return 'QuestionstepsEvent.toBeImmortalChange(answer: $answer)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _ToBeImmortalChange &&
            const DeepCollectionEquality().equals(other.answer, answer));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(answer));

  @JsonKey(ignore: true)
  @override
  _$ToBeImmortalChangeCopyWith<_ToBeImmortalChange> get copyWith =>
      __$ToBeImmortalChangeCopyWithImpl<_ToBeImmortalChange>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(AnswersType? answer) drinkWaterWithMouthChange,
    required TResult Function(AnswersType? answer) cheatingInExamChange,
    required TResult Function(AnswersType? answer) loveTheEmotionalMovieChange,
    required TResult Function(AnswersType? answer) toBeImmortalChange,
    required TResult Function(AnswersType? answer) smokeCigarettesChange,
    required TResult Function(AnswersType? answer) alwaysPresentInUniChange,
    required TResult Function(AnswersType? answer)
        travelWithoutPermissionChange,
    required TResult Function(String data) addDataInStepList,
    required TResult Function(int index) removeFromStepListWitnIndex,
  }) {
    return toBeImmortalChange(answer);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(AnswersType? answer)? drinkWaterWithMouthChange,
    TResult Function(AnswersType? answer)? cheatingInExamChange,
    TResult Function(AnswersType? answer)? loveTheEmotionalMovieChange,
    TResult Function(AnswersType? answer)? toBeImmortalChange,
    TResult Function(AnswersType? answer)? smokeCigarettesChange,
    TResult Function(AnswersType? answer)? alwaysPresentInUniChange,
    TResult Function(AnswersType? answer)? travelWithoutPermissionChange,
    TResult Function(String data)? addDataInStepList,
    TResult Function(int index)? removeFromStepListWitnIndex,
  }) {
    return toBeImmortalChange?.call(answer);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(AnswersType? answer)? drinkWaterWithMouthChange,
    TResult Function(AnswersType? answer)? cheatingInExamChange,
    TResult Function(AnswersType? answer)? loveTheEmotionalMovieChange,
    TResult Function(AnswersType? answer)? toBeImmortalChange,
    TResult Function(AnswersType? answer)? smokeCigarettesChange,
    TResult Function(AnswersType? answer)? alwaysPresentInUniChange,
    TResult Function(AnswersType? answer)? travelWithoutPermissionChange,
    TResult Function(String data)? addDataInStepList,
    TResult Function(int index)? removeFromStepListWitnIndex,
    required TResult orElse(),
  }) {
    if (toBeImmortalChange != null) {
      return toBeImmortalChange(answer);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_QuestionstepsEventInitial value) initial,
    required TResult Function(_DrinkWaterWithMouthChange value)
        drinkWaterWithMouthChange,
    required TResult Function(_CheatingInExamChange value) cheatingInExamChange,
    required TResult Function(_LoveTheEmotionalMovieChange value)
        loveTheEmotionalMovieChange,
    required TResult Function(_ToBeImmortalChange value) toBeImmortalChange,
    required TResult Function(_SmokeCigarettesChange value)
        smokeCigarettesChange,
    required TResult Function(_AlwaysPresentInUniChange value)
        alwaysPresentInUniChange,
    required TResult Function(_TravelWithoutPermissionChange value)
        travelWithoutPermissionChange,
    required TResult Function(_AddDataInStepList value) addDataInStepList,
    required TResult Function(_RemoveFromStepListWitnIndex value)
        removeFromStepListWitnIndex,
  }) {
    return toBeImmortalChange(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_QuestionstepsEventInitial value)? initial,
    TResult Function(_DrinkWaterWithMouthChange value)?
        drinkWaterWithMouthChange,
    TResult Function(_CheatingInExamChange value)? cheatingInExamChange,
    TResult Function(_LoveTheEmotionalMovieChange value)?
        loveTheEmotionalMovieChange,
    TResult Function(_ToBeImmortalChange value)? toBeImmortalChange,
    TResult Function(_SmokeCigarettesChange value)? smokeCigarettesChange,
    TResult Function(_AlwaysPresentInUniChange value)? alwaysPresentInUniChange,
    TResult Function(_TravelWithoutPermissionChange value)?
        travelWithoutPermissionChange,
    TResult Function(_AddDataInStepList value)? addDataInStepList,
    TResult Function(_RemoveFromStepListWitnIndex value)?
        removeFromStepListWitnIndex,
  }) {
    return toBeImmortalChange?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_QuestionstepsEventInitial value)? initial,
    TResult Function(_DrinkWaterWithMouthChange value)?
        drinkWaterWithMouthChange,
    TResult Function(_CheatingInExamChange value)? cheatingInExamChange,
    TResult Function(_LoveTheEmotionalMovieChange value)?
        loveTheEmotionalMovieChange,
    TResult Function(_ToBeImmortalChange value)? toBeImmortalChange,
    TResult Function(_SmokeCigarettesChange value)? smokeCigarettesChange,
    TResult Function(_AlwaysPresentInUniChange value)? alwaysPresentInUniChange,
    TResult Function(_TravelWithoutPermissionChange value)?
        travelWithoutPermissionChange,
    TResult Function(_AddDataInStepList value)? addDataInStepList,
    TResult Function(_RemoveFromStepListWitnIndex value)?
        removeFromStepListWitnIndex,
    required TResult orElse(),
  }) {
    if (toBeImmortalChange != null) {
      return toBeImmortalChange(this);
    }
    return orElse();
  }
}

abstract class _ToBeImmortalChange implements QuestionstepsEvent {
  const factory _ToBeImmortalChange({required AnswersType? answer}) =
      _$_ToBeImmortalChange;

  AnswersType? get answer;
  @JsonKey(ignore: true)
  _$ToBeImmortalChangeCopyWith<_ToBeImmortalChange> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$SmokeCigarettesChangeCopyWith<$Res> {
  factory _$SmokeCigarettesChangeCopyWith(_SmokeCigarettesChange value,
          $Res Function(_SmokeCigarettesChange) then) =
      __$SmokeCigarettesChangeCopyWithImpl<$Res>;
  $Res call({AnswersType? answer});

  $AnswersTypeCopyWith<$Res>? get answer;
}

/// @nodoc
class __$SmokeCigarettesChangeCopyWithImpl<$Res>
    extends _$QuestionstepsEventCopyWithImpl<$Res>
    implements _$SmokeCigarettesChangeCopyWith<$Res> {
  __$SmokeCigarettesChangeCopyWithImpl(_SmokeCigarettesChange _value,
      $Res Function(_SmokeCigarettesChange) _then)
      : super(_value, (v) => _then(v as _SmokeCigarettesChange));

  @override
  _SmokeCigarettesChange get _value => super._value as _SmokeCigarettesChange;

  @override
  $Res call({
    Object? answer = freezed,
  }) {
    return _then(_SmokeCigarettesChange(
      answer: answer == freezed
          ? _value.answer
          : answer // ignore: cast_nullable_to_non_nullable
              as AnswersType?,
    ));
  }

  @override
  $AnswersTypeCopyWith<$Res>? get answer {
    if (_value.answer == null) {
      return null;
    }

    return $AnswersTypeCopyWith<$Res>(_value.answer!, (value) {
      return _then(_value.copyWith(answer: value));
    });
  }
}

/// @nodoc

class _$_SmokeCigarettesChange implements _SmokeCigarettesChange {
  const _$_SmokeCigarettesChange({required this.answer});

  @override
  final AnswersType? answer;

  @override
  String toString() {
    return 'QuestionstepsEvent.smokeCigarettesChange(answer: $answer)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _SmokeCigarettesChange &&
            const DeepCollectionEquality().equals(other.answer, answer));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(answer));

  @JsonKey(ignore: true)
  @override
  _$SmokeCigarettesChangeCopyWith<_SmokeCigarettesChange> get copyWith =>
      __$SmokeCigarettesChangeCopyWithImpl<_SmokeCigarettesChange>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(AnswersType? answer) drinkWaterWithMouthChange,
    required TResult Function(AnswersType? answer) cheatingInExamChange,
    required TResult Function(AnswersType? answer) loveTheEmotionalMovieChange,
    required TResult Function(AnswersType? answer) toBeImmortalChange,
    required TResult Function(AnswersType? answer) smokeCigarettesChange,
    required TResult Function(AnswersType? answer) alwaysPresentInUniChange,
    required TResult Function(AnswersType? answer)
        travelWithoutPermissionChange,
    required TResult Function(String data) addDataInStepList,
    required TResult Function(int index) removeFromStepListWitnIndex,
  }) {
    return smokeCigarettesChange(answer);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(AnswersType? answer)? drinkWaterWithMouthChange,
    TResult Function(AnswersType? answer)? cheatingInExamChange,
    TResult Function(AnswersType? answer)? loveTheEmotionalMovieChange,
    TResult Function(AnswersType? answer)? toBeImmortalChange,
    TResult Function(AnswersType? answer)? smokeCigarettesChange,
    TResult Function(AnswersType? answer)? alwaysPresentInUniChange,
    TResult Function(AnswersType? answer)? travelWithoutPermissionChange,
    TResult Function(String data)? addDataInStepList,
    TResult Function(int index)? removeFromStepListWitnIndex,
  }) {
    return smokeCigarettesChange?.call(answer);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(AnswersType? answer)? drinkWaterWithMouthChange,
    TResult Function(AnswersType? answer)? cheatingInExamChange,
    TResult Function(AnswersType? answer)? loveTheEmotionalMovieChange,
    TResult Function(AnswersType? answer)? toBeImmortalChange,
    TResult Function(AnswersType? answer)? smokeCigarettesChange,
    TResult Function(AnswersType? answer)? alwaysPresentInUniChange,
    TResult Function(AnswersType? answer)? travelWithoutPermissionChange,
    TResult Function(String data)? addDataInStepList,
    TResult Function(int index)? removeFromStepListWitnIndex,
    required TResult orElse(),
  }) {
    if (smokeCigarettesChange != null) {
      return smokeCigarettesChange(answer);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_QuestionstepsEventInitial value) initial,
    required TResult Function(_DrinkWaterWithMouthChange value)
        drinkWaterWithMouthChange,
    required TResult Function(_CheatingInExamChange value) cheatingInExamChange,
    required TResult Function(_LoveTheEmotionalMovieChange value)
        loveTheEmotionalMovieChange,
    required TResult Function(_ToBeImmortalChange value) toBeImmortalChange,
    required TResult Function(_SmokeCigarettesChange value)
        smokeCigarettesChange,
    required TResult Function(_AlwaysPresentInUniChange value)
        alwaysPresentInUniChange,
    required TResult Function(_TravelWithoutPermissionChange value)
        travelWithoutPermissionChange,
    required TResult Function(_AddDataInStepList value) addDataInStepList,
    required TResult Function(_RemoveFromStepListWitnIndex value)
        removeFromStepListWitnIndex,
  }) {
    return smokeCigarettesChange(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_QuestionstepsEventInitial value)? initial,
    TResult Function(_DrinkWaterWithMouthChange value)?
        drinkWaterWithMouthChange,
    TResult Function(_CheatingInExamChange value)? cheatingInExamChange,
    TResult Function(_LoveTheEmotionalMovieChange value)?
        loveTheEmotionalMovieChange,
    TResult Function(_ToBeImmortalChange value)? toBeImmortalChange,
    TResult Function(_SmokeCigarettesChange value)? smokeCigarettesChange,
    TResult Function(_AlwaysPresentInUniChange value)? alwaysPresentInUniChange,
    TResult Function(_TravelWithoutPermissionChange value)?
        travelWithoutPermissionChange,
    TResult Function(_AddDataInStepList value)? addDataInStepList,
    TResult Function(_RemoveFromStepListWitnIndex value)?
        removeFromStepListWitnIndex,
  }) {
    return smokeCigarettesChange?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_QuestionstepsEventInitial value)? initial,
    TResult Function(_DrinkWaterWithMouthChange value)?
        drinkWaterWithMouthChange,
    TResult Function(_CheatingInExamChange value)? cheatingInExamChange,
    TResult Function(_LoveTheEmotionalMovieChange value)?
        loveTheEmotionalMovieChange,
    TResult Function(_ToBeImmortalChange value)? toBeImmortalChange,
    TResult Function(_SmokeCigarettesChange value)? smokeCigarettesChange,
    TResult Function(_AlwaysPresentInUniChange value)? alwaysPresentInUniChange,
    TResult Function(_TravelWithoutPermissionChange value)?
        travelWithoutPermissionChange,
    TResult Function(_AddDataInStepList value)? addDataInStepList,
    TResult Function(_RemoveFromStepListWitnIndex value)?
        removeFromStepListWitnIndex,
    required TResult orElse(),
  }) {
    if (smokeCigarettesChange != null) {
      return smokeCigarettesChange(this);
    }
    return orElse();
  }
}

abstract class _SmokeCigarettesChange implements QuestionstepsEvent {
  const factory _SmokeCigarettesChange({required AnswersType? answer}) =
      _$_SmokeCigarettesChange;

  AnswersType? get answer;
  @JsonKey(ignore: true)
  _$SmokeCigarettesChangeCopyWith<_SmokeCigarettesChange> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$AlwaysPresentInUniChangeCopyWith<$Res> {
  factory _$AlwaysPresentInUniChangeCopyWith(_AlwaysPresentInUniChange value,
          $Res Function(_AlwaysPresentInUniChange) then) =
      __$AlwaysPresentInUniChangeCopyWithImpl<$Res>;
  $Res call({AnswersType? answer});

  $AnswersTypeCopyWith<$Res>? get answer;
}

/// @nodoc
class __$AlwaysPresentInUniChangeCopyWithImpl<$Res>
    extends _$QuestionstepsEventCopyWithImpl<$Res>
    implements _$AlwaysPresentInUniChangeCopyWith<$Res> {
  __$AlwaysPresentInUniChangeCopyWithImpl(_AlwaysPresentInUniChange _value,
      $Res Function(_AlwaysPresentInUniChange) _then)
      : super(_value, (v) => _then(v as _AlwaysPresentInUniChange));

  @override
  _AlwaysPresentInUniChange get _value =>
      super._value as _AlwaysPresentInUniChange;

  @override
  $Res call({
    Object? answer = freezed,
  }) {
    return _then(_AlwaysPresentInUniChange(
      answer: answer == freezed
          ? _value.answer
          : answer // ignore: cast_nullable_to_non_nullable
              as AnswersType?,
    ));
  }

  @override
  $AnswersTypeCopyWith<$Res>? get answer {
    if (_value.answer == null) {
      return null;
    }

    return $AnswersTypeCopyWith<$Res>(_value.answer!, (value) {
      return _then(_value.copyWith(answer: value));
    });
  }
}

/// @nodoc

class _$_AlwaysPresentInUniChange implements _AlwaysPresentInUniChange {
  const _$_AlwaysPresentInUniChange({required this.answer});

  @override
  final AnswersType? answer;

  @override
  String toString() {
    return 'QuestionstepsEvent.alwaysPresentInUniChange(answer: $answer)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _AlwaysPresentInUniChange &&
            const DeepCollectionEquality().equals(other.answer, answer));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(answer));

  @JsonKey(ignore: true)
  @override
  _$AlwaysPresentInUniChangeCopyWith<_AlwaysPresentInUniChange> get copyWith =>
      __$AlwaysPresentInUniChangeCopyWithImpl<_AlwaysPresentInUniChange>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(AnswersType? answer) drinkWaterWithMouthChange,
    required TResult Function(AnswersType? answer) cheatingInExamChange,
    required TResult Function(AnswersType? answer) loveTheEmotionalMovieChange,
    required TResult Function(AnswersType? answer) toBeImmortalChange,
    required TResult Function(AnswersType? answer) smokeCigarettesChange,
    required TResult Function(AnswersType? answer) alwaysPresentInUniChange,
    required TResult Function(AnswersType? answer)
        travelWithoutPermissionChange,
    required TResult Function(String data) addDataInStepList,
    required TResult Function(int index) removeFromStepListWitnIndex,
  }) {
    return alwaysPresentInUniChange(answer);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(AnswersType? answer)? drinkWaterWithMouthChange,
    TResult Function(AnswersType? answer)? cheatingInExamChange,
    TResult Function(AnswersType? answer)? loveTheEmotionalMovieChange,
    TResult Function(AnswersType? answer)? toBeImmortalChange,
    TResult Function(AnswersType? answer)? smokeCigarettesChange,
    TResult Function(AnswersType? answer)? alwaysPresentInUniChange,
    TResult Function(AnswersType? answer)? travelWithoutPermissionChange,
    TResult Function(String data)? addDataInStepList,
    TResult Function(int index)? removeFromStepListWitnIndex,
  }) {
    return alwaysPresentInUniChange?.call(answer);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(AnswersType? answer)? drinkWaterWithMouthChange,
    TResult Function(AnswersType? answer)? cheatingInExamChange,
    TResult Function(AnswersType? answer)? loveTheEmotionalMovieChange,
    TResult Function(AnswersType? answer)? toBeImmortalChange,
    TResult Function(AnswersType? answer)? smokeCigarettesChange,
    TResult Function(AnswersType? answer)? alwaysPresentInUniChange,
    TResult Function(AnswersType? answer)? travelWithoutPermissionChange,
    TResult Function(String data)? addDataInStepList,
    TResult Function(int index)? removeFromStepListWitnIndex,
    required TResult orElse(),
  }) {
    if (alwaysPresentInUniChange != null) {
      return alwaysPresentInUniChange(answer);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_QuestionstepsEventInitial value) initial,
    required TResult Function(_DrinkWaterWithMouthChange value)
        drinkWaterWithMouthChange,
    required TResult Function(_CheatingInExamChange value) cheatingInExamChange,
    required TResult Function(_LoveTheEmotionalMovieChange value)
        loveTheEmotionalMovieChange,
    required TResult Function(_ToBeImmortalChange value) toBeImmortalChange,
    required TResult Function(_SmokeCigarettesChange value)
        smokeCigarettesChange,
    required TResult Function(_AlwaysPresentInUniChange value)
        alwaysPresentInUniChange,
    required TResult Function(_TravelWithoutPermissionChange value)
        travelWithoutPermissionChange,
    required TResult Function(_AddDataInStepList value) addDataInStepList,
    required TResult Function(_RemoveFromStepListWitnIndex value)
        removeFromStepListWitnIndex,
  }) {
    return alwaysPresentInUniChange(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_QuestionstepsEventInitial value)? initial,
    TResult Function(_DrinkWaterWithMouthChange value)?
        drinkWaterWithMouthChange,
    TResult Function(_CheatingInExamChange value)? cheatingInExamChange,
    TResult Function(_LoveTheEmotionalMovieChange value)?
        loveTheEmotionalMovieChange,
    TResult Function(_ToBeImmortalChange value)? toBeImmortalChange,
    TResult Function(_SmokeCigarettesChange value)? smokeCigarettesChange,
    TResult Function(_AlwaysPresentInUniChange value)? alwaysPresentInUniChange,
    TResult Function(_TravelWithoutPermissionChange value)?
        travelWithoutPermissionChange,
    TResult Function(_AddDataInStepList value)? addDataInStepList,
    TResult Function(_RemoveFromStepListWitnIndex value)?
        removeFromStepListWitnIndex,
  }) {
    return alwaysPresentInUniChange?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_QuestionstepsEventInitial value)? initial,
    TResult Function(_DrinkWaterWithMouthChange value)?
        drinkWaterWithMouthChange,
    TResult Function(_CheatingInExamChange value)? cheatingInExamChange,
    TResult Function(_LoveTheEmotionalMovieChange value)?
        loveTheEmotionalMovieChange,
    TResult Function(_ToBeImmortalChange value)? toBeImmortalChange,
    TResult Function(_SmokeCigarettesChange value)? smokeCigarettesChange,
    TResult Function(_AlwaysPresentInUniChange value)? alwaysPresentInUniChange,
    TResult Function(_TravelWithoutPermissionChange value)?
        travelWithoutPermissionChange,
    TResult Function(_AddDataInStepList value)? addDataInStepList,
    TResult Function(_RemoveFromStepListWitnIndex value)?
        removeFromStepListWitnIndex,
    required TResult orElse(),
  }) {
    if (alwaysPresentInUniChange != null) {
      return alwaysPresentInUniChange(this);
    }
    return orElse();
  }
}

abstract class _AlwaysPresentInUniChange implements QuestionstepsEvent {
  const factory _AlwaysPresentInUniChange({required AnswersType? answer}) =
      _$_AlwaysPresentInUniChange;

  AnswersType? get answer;
  @JsonKey(ignore: true)
  _$AlwaysPresentInUniChangeCopyWith<_AlwaysPresentInUniChange> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$TravelWithoutPermissionChangeCopyWith<$Res> {
  factory _$TravelWithoutPermissionChangeCopyWith(
          _TravelWithoutPermissionChange value,
          $Res Function(_TravelWithoutPermissionChange) then) =
      __$TravelWithoutPermissionChangeCopyWithImpl<$Res>;
  $Res call({AnswersType? answer});

  $AnswersTypeCopyWith<$Res>? get answer;
}

/// @nodoc
class __$TravelWithoutPermissionChangeCopyWithImpl<$Res>
    extends _$QuestionstepsEventCopyWithImpl<$Res>
    implements _$TravelWithoutPermissionChangeCopyWith<$Res> {
  __$TravelWithoutPermissionChangeCopyWithImpl(
      _TravelWithoutPermissionChange _value,
      $Res Function(_TravelWithoutPermissionChange) _then)
      : super(_value, (v) => _then(v as _TravelWithoutPermissionChange));

  @override
  _TravelWithoutPermissionChange get _value =>
      super._value as _TravelWithoutPermissionChange;

  @override
  $Res call({
    Object? answer = freezed,
  }) {
    return _then(_TravelWithoutPermissionChange(
      answer: answer == freezed
          ? _value.answer
          : answer // ignore: cast_nullable_to_non_nullable
              as AnswersType?,
    ));
  }

  @override
  $AnswersTypeCopyWith<$Res>? get answer {
    if (_value.answer == null) {
      return null;
    }

    return $AnswersTypeCopyWith<$Res>(_value.answer!, (value) {
      return _then(_value.copyWith(answer: value));
    });
  }
}

/// @nodoc

class _$_TravelWithoutPermissionChange
    implements _TravelWithoutPermissionChange {
  const _$_TravelWithoutPermissionChange({required this.answer});

  @override
  final AnswersType? answer;

  @override
  String toString() {
    return 'QuestionstepsEvent.travelWithoutPermissionChange(answer: $answer)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _TravelWithoutPermissionChange &&
            const DeepCollectionEquality().equals(other.answer, answer));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(answer));

  @JsonKey(ignore: true)
  @override
  _$TravelWithoutPermissionChangeCopyWith<_TravelWithoutPermissionChange>
      get copyWith => __$TravelWithoutPermissionChangeCopyWithImpl<
          _TravelWithoutPermissionChange>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(AnswersType? answer) drinkWaterWithMouthChange,
    required TResult Function(AnswersType? answer) cheatingInExamChange,
    required TResult Function(AnswersType? answer) loveTheEmotionalMovieChange,
    required TResult Function(AnswersType? answer) toBeImmortalChange,
    required TResult Function(AnswersType? answer) smokeCigarettesChange,
    required TResult Function(AnswersType? answer) alwaysPresentInUniChange,
    required TResult Function(AnswersType? answer)
        travelWithoutPermissionChange,
    required TResult Function(String data) addDataInStepList,
    required TResult Function(int index) removeFromStepListWitnIndex,
  }) {
    return travelWithoutPermissionChange(answer);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(AnswersType? answer)? drinkWaterWithMouthChange,
    TResult Function(AnswersType? answer)? cheatingInExamChange,
    TResult Function(AnswersType? answer)? loveTheEmotionalMovieChange,
    TResult Function(AnswersType? answer)? toBeImmortalChange,
    TResult Function(AnswersType? answer)? smokeCigarettesChange,
    TResult Function(AnswersType? answer)? alwaysPresentInUniChange,
    TResult Function(AnswersType? answer)? travelWithoutPermissionChange,
    TResult Function(String data)? addDataInStepList,
    TResult Function(int index)? removeFromStepListWitnIndex,
  }) {
    return travelWithoutPermissionChange?.call(answer);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(AnswersType? answer)? drinkWaterWithMouthChange,
    TResult Function(AnswersType? answer)? cheatingInExamChange,
    TResult Function(AnswersType? answer)? loveTheEmotionalMovieChange,
    TResult Function(AnswersType? answer)? toBeImmortalChange,
    TResult Function(AnswersType? answer)? smokeCigarettesChange,
    TResult Function(AnswersType? answer)? alwaysPresentInUniChange,
    TResult Function(AnswersType? answer)? travelWithoutPermissionChange,
    TResult Function(String data)? addDataInStepList,
    TResult Function(int index)? removeFromStepListWitnIndex,
    required TResult orElse(),
  }) {
    if (travelWithoutPermissionChange != null) {
      return travelWithoutPermissionChange(answer);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_QuestionstepsEventInitial value) initial,
    required TResult Function(_DrinkWaterWithMouthChange value)
        drinkWaterWithMouthChange,
    required TResult Function(_CheatingInExamChange value) cheatingInExamChange,
    required TResult Function(_LoveTheEmotionalMovieChange value)
        loveTheEmotionalMovieChange,
    required TResult Function(_ToBeImmortalChange value) toBeImmortalChange,
    required TResult Function(_SmokeCigarettesChange value)
        smokeCigarettesChange,
    required TResult Function(_AlwaysPresentInUniChange value)
        alwaysPresentInUniChange,
    required TResult Function(_TravelWithoutPermissionChange value)
        travelWithoutPermissionChange,
    required TResult Function(_AddDataInStepList value) addDataInStepList,
    required TResult Function(_RemoveFromStepListWitnIndex value)
        removeFromStepListWitnIndex,
  }) {
    return travelWithoutPermissionChange(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_QuestionstepsEventInitial value)? initial,
    TResult Function(_DrinkWaterWithMouthChange value)?
        drinkWaterWithMouthChange,
    TResult Function(_CheatingInExamChange value)? cheatingInExamChange,
    TResult Function(_LoveTheEmotionalMovieChange value)?
        loveTheEmotionalMovieChange,
    TResult Function(_ToBeImmortalChange value)? toBeImmortalChange,
    TResult Function(_SmokeCigarettesChange value)? smokeCigarettesChange,
    TResult Function(_AlwaysPresentInUniChange value)? alwaysPresentInUniChange,
    TResult Function(_TravelWithoutPermissionChange value)?
        travelWithoutPermissionChange,
    TResult Function(_AddDataInStepList value)? addDataInStepList,
    TResult Function(_RemoveFromStepListWitnIndex value)?
        removeFromStepListWitnIndex,
  }) {
    return travelWithoutPermissionChange?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_QuestionstepsEventInitial value)? initial,
    TResult Function(_DrinkWaterWithMouthChange value)?
        drinkWaterWithMouthChange,
    TResult Function(_CheatingInExamChange value)? cheatingInExamChange,
    TResult Function(_LoveTheEmotionalMovieChange value)?
        loveTheEmotionalMovieChange,
    TResult Function(_ToBeImmortalChange value)? toBeImmortalChange,
    TResult Function(_SmokeCigarettesChange value)? smokeCigarettesChange,
    TResult Function(_AlwaysPresentInUniChange value)? alwaysPresentInUniChange,
    TResult Function(_TravelWithoutPermissionChange value)?
        travelWithoutPermissionChange,
    TResult Function(_AddDataInStepList value)? addDataInStepList,
    TResult Function(_RemoveFromStepListWitnIndex value)?
        removeFromStepListWitnIndex,
    required TResult orElse(),
  }) {
    if (travelWithoutPermissionChange != null) {
      return travelWithoutPermissionChange(this);
    }
    return orElse();
  }
}

abstract class _TravelWithoutPermissionChange implements QuestionstepsEvent {
  const factory _TravelWithoutPermissionChange({required AnswersType? answer}) =
      _$_TravelWithoutPermissionChange;

  AnswersType? get answer;
  @JsonKey(ignore: true)
  _$TravelWithoutPermissionChangeCopyWith<_TravelWithoutPermissionChange>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$AddDataInStepListCopyWith<$Res> {
  factory _$AddDataInStepListCopyWith(
          _AddDataInStepList value, $Res Function(_AddDataInStepList) then) =
      __$AddDataInStepListCopyWithImpl<$Res>;
  $Res call({String data});
}

/// @nodoc
class __$AddDataInStepListCopyWithImpl<$Res>
    extends _$QuestionstepsEventCopyWithImpl<$Res>
    implements _$AddDataInStepListCopyWith<$Res> {
  __$AddDataInStepListCopyWithImpl(
      _AddDataInStepList _value, $Res Function(_AddDataInStepList) _then)
      : super(_value, (v) => _then(v as _AddDataInStepList));

  @override
  _AddDataInStepList get _value => super._value as _AddDataInStepList;

  @override
  $Res call({
    Object? data = freezed,
  }) {
    return _then(_AddDataInStepList(
      data: data == freezed
          ? _value.data
          : data // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_AddDataInStepList implements _AddDataInStepList {
  const _$_AddDataInStepList({required this.data});

  @override
  final String data;

  @override
  String toString() {
    return 'QuestionstepsEvent.addDataInStepList(data: $data)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _AddDataInStepList &&
            const DeepCollectionEquality().equals(other.data, data));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(data));

  @JsonKey(ignore: true)
  @override
  _$AddDataInStepListCopyWith<_AddDataInStepList> get copyWith =>
      __$AddDataInStepListCopyWithImpl<_AddDataInStepList>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(AnswersType? answer) drinkWaterWithMouthChange,
    required TResult Function(AnswersType? answer) cheatingInExamChange,
    required TResult Function(AnswersType? answer) loveTheEmotionalMovieChange,
    required TResult Function(AnswersType? answer) toBeImmortalChange,
    required TResult Function(AnswersType? answer) smokeCigarettesChange,
    required TResult Function(AnswersType? answer) alwaysPresentInUniChange,
    required TResult Function(AnswersType? answer)
        travelWithoutPermissionChange,
    required TResult Function(String data) addDataInStepList,
    required TResult Function(int index) removeFromStepListWitnIndex,
  }) {
    return addDataInStepList(data);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(AnswersType? answer)? drinkWaterWithMouthChange,
    TResult Function(AnswersType? answer)? cheatingInExamChange,
    TResult Function(AnswersType? answer)? loveTheEmotionalMovieChange,
    TResult Function(AnswersType? answer)? toBeImmortalChange,
    TResult Function(AnswersType? answer)? smokeCigarettesChange,
    TResult Function(AnswersType? answer)? alwaysPresentInUniChange,
    TResult Function(AnswersType? answer)? travelWithoutPermissionChange,
    TResult Function(String data)? addDataInStepList,
    TResult Function(int index)? removeFromStepListWitnIndex,
  }) {
    return addDataInStepList?.call(data);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(AnswersType? answer)? drinkWaterWithMouthChange,
    TResult Function(AnswersType? answer)? cheatingInExamChange,
    TResult Function(AnswersType? answer)? loveTheEmotionalMovieChange,
    TResult Function(AnswersType? answer)? toBeImmortalChange,
    TResult Function(AnswersType? answer)? smokeCigarettesChange,
    TResult Function(AnswersType? answer)? alwaysPresentInUniChange,
    TResult Function(AnswersType? answer)? travelWithoutPermissionChange,
    TResult Function(String data)? addDataInStepList,
    TResult Function(int index)? removeFromStepListWitnIndex,
    required TResult orElse(),
  }) {
    if (addDataInStepList != null) {
      return addDataInStepList(data);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_QuestionstepsEventInitial value) initial,
    required TResult Function(_DrinkWaterWithMouthChange value)
        drinkWaterWithMouthChange,
    required TResult Function(_CheatingInExamChange value) cheatingInExamChange,
    required TResult Function(_LoveTheEmotionalMovieChange value)
        loveTheEmotionalMovieChange,
    required TResult Function(_ToBeImmortalChange value) toBeImmortalChange,
    required TResult Function(_SmokeCigarettesChange value)
        smokeCigarettesChange,
    required TResult Function(_AlwaysPresentInUniChange value)
        alwaysPresentInUniChange,
    required TResult Function(_TravelWithoutPermissionChange value)
        travelWithoutPermissionChange,
    required TResult Function(_AddDataInStepList value) addDataInStepList,
    required TResult Function(_RemoveFromStepListWitnIndex value)
        removeFromStepListWitnIndex,
  }) {
    return addDataInStepList(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_QuestionstepsEventInitial value)? initial,
    TResult Function(_DrinkWaterWithMouthChange value)?
        drinkWaterWithMouthChange,
    TResult Function(_CheatingInExamChange value)? cheatingInExamChange,
    TResult Function(_LoveTheEmotionalMovieChange value)?
        loveTheEmotionalMovieChange,
    TResult Function(_ToBeImmortalChange value)? toBeImmortalChange,
    TResult Function(_SmokeCigarettesChange value)? smokeCigarettesChange,
    TResult Function(_AlwaysPresentInUniChange value)? alwaysPresentInUniChange,
    TResult Function(_TravelWithoutPermissionChange value)?
        travelWithoutPermissionChange,
    TResult Function(_AddDataInStepList value)? addDataInStepList,
    TResult Function(_RemoveFromStepListWitnIndex value)?
        removeFromStepListWitnIndex,
  }) {
    return addDataInStepList?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_QuestionstepsEventInitial value)? initial,
    TResult Function(_DrinkWaterWithMouthChange value)?
        drinkWaterWithMouthChange,
    TResult Function(_CheatingInExamChange value)? cheatingInExamChange,
    TResult Function(_LoveTheEmotionalMovieChange value)?
        loveTheEmotionalMovieChange,
    TResult Function(_ToBeImmortalChange value)? toBeImmortalChange,
    TResult Function(_SmokeCigarettesChange value)? smokeCigarettesChange,
    TResult Function(_AlwaysPresentInUniChange value)? alwaysPresentInUniChange,
    TResult Function(_TravelWithoutPermissionChange value)?
        travelWithoutPermissionChange,
    TResult Function(_AddDataInStepList value)? addDataInStepList,
    TResult Function(_RemoveFromStepListWitnIndex value)?
        removeFromStepListWitnIndex,
    required TResult orElse(),
  }) {
    if (addDataInStepList != null) {
      return addDataInStepList(this);
    }
    return orElse();
  }
}

abstract class _AddDataInStepList implements QuestionstepsEvent {
  const factory _AddDataInStepList({required String data}) =
      _$_AddDataInStepList;

  String get data;
  @JsonKey(ignore: true)
  _$AddDataInStepListCopyWith<_AddDataInStepList> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$RemoveFromStepListWitnIndexCopyWith<$Res> {
  factory _$RemoveFromStepListWitnIndexCopyWith(
          _RemoveFromStepListWitnIndex value,
          $Res Function(_RemoveFromStepListWitnIndex) then) =
      __$RemoveFromStepListWitnIndexCopyWithImpl<$Res>;
  $Res call({int index});
}

/// @nodoc
class __$RemoveFromStepListWitnIndexCopyWithImpl<$Res>
    extends _$QuestionstepsEventCopyWithImpl<$Res>
    implements _$RemoveFromStepListWitnIndexCopyWith<$Res> {
  __$RemoveFromStepListWitnIndexCopyWithImpl(
      _RemoveFromStepListWitnIndex _value,
      $Res Function(_RemoveFromStepListWitnIndex) _then)
      : super(_value, (v) => _then(v as _RemoveFromStepListWitnIndex));

  @override
  _RemoveFromStepListWitnIndex get _value =>
      super._value as _RemoveFromStepListWitnIndex;

  @override
  $Res call({
    Object? index = freezed,
  }) {
    return _then(_RemoveFromStepListWitnIndex(
      index: index == freezed
          ? _value.index
          : index // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_RemoveFromStepListWitnIndex implements _RemoveFromStepListWitnIndex {
  const _$_RemoveFromStepListWitnIndex({required this.index});

  @override
  final int index;

  @override
  String toString() {
    return 'QuestionstepsEvent.removeFromStepListWitnIndex(index: $index)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _RemoveFromStepListWitnIndex &&
            const DeepCollectionEquality().equals(other.index, index));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(index));

  @JsonKey(ignore: true)
  @override
  _$RemoveFromStepListWitnIndexCopyWith<_RemoveFromStepListWitnIndex>
      get copyWith => __$RemoveFromStepListWitnIndexCopyWithImpl<
          _RemoveFromStepListWitnIndex>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(AnswersType? answer) drinkWaterWithMouthChange,
    required TResult Function(AnswersType? answer) cheatingInExamChange,
    required TResult Function(AnswersType? answer) loveTheEmotionalMovieChange,
    required TResult Function(AnswersType? answer) toBeImmortalChange,
    required TResult Function(AnswersType? answer) smokeCigarettesChange,
    required TResult Function(AnswersType? answer) alwaysPresentInUniChange,
    required TResult Function(AnswersType? answer)
        travelWithoutPermissionChange,
    required TResult Function(String data) addDataInStepList,
    required TResult Function(int index) removeFromStepListWitnIndex,
  }) {
    return removeFromStepListWitnIndex(index);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(AnswersType? answer)? drinkWaterWithMouthChange,
    TResult Function(AnswersType? answer)? cheatingInExamChange,
    TResult Function(AnswersType? answer)? loveTheEmotionalMovieChange,
    TResult Function(AnswersType? answer)? toBeImmortalChange,
    TResult Function(AnswersType? answer)? smokeCigarettesChange,
    TResult Function(AnswersType? answer)? alwaysPresentInUniChange,
    TResult Function(AnswersType? answer)? travelWithoutPermissionChange,
    TResult Function(String data)? addDataInStepList,
    TResult Function(int index)? removeFromStepListWitnIndex,
  }) {
    return removeFromStepListWitnIndex?.call(index);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(AnswersType? answer)? drinkWaterWithMouthChange,
    TResult Function(AnswersType? answer)? cheatingInExamChange,
    TResult Function(AnswersType? answer)? loveTheEmotionalMovieChange,
    TResult Function(AnswersType? answer)? toBeImmortalChange,
    TResult Function(AnswersType? answer)? smokeCigarettesChange,
    TResult Function(AnswersType? answer)? alwaysPresentInUniChange,
    TResult Function(AnswersType? answer)? travelWithoutPermissionChange,
    TResult Function(String data)? addDataInStepList,
    TResult Function(int index)? removeFromStepListWitnIndex,
    required TResult orElse(),
  }) {
    if (removeFromStepListWitnIndex != null) {
      return removeFromStepListWitnIndex(index);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_QuestionstepsEventInitial value) initial,
    required TResult Function(_DrinkWaterWithMouthChange value)
        drinkWaterWithMouthChange,
    required TResult Function(_CheatingInExamChange value) cheatingInExamChange,
    required TResult Function(_LoveTheEmotionalMovieChange value)
        loveTheEmotionalMovieChange,
    required TResult Function(_ToBeImmortalChange value) toBeImmortalChange,
    required TResult Function(_SmokeCigarettesChange value)
        smokeCigarettesChange,
    required TResult Function(_AlwaysPresentInUniChange value)
        alwaysPresentInUniChange,
    required TResult Function(_TravelWithoutPermissionChange value)
        travelWithoutPermissionChange,
    required TResult Function(_AddDataInStepList value) addDataInStepList,
    required TResult Function(_RemoveFromStepListWitnIndex value)
        removeFromStepListWitnIndex,
  }) {
    return removeFromStepListWitnIndex(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_QuestionstepsEventInitial value)? initial,
    TResult Function(_DrinkWaterWithMouthChange value)?
        drinkWaterWithMouthChange,
    TResult Function(_CheatingInExamChange value)? cheatingInExamChange,
    TResult Function(_LoveTheEmotionalMovieChange value)?
        loveTheEmotionalMovieChange,
    TResult Function(_ToBeImmortalChange value)? toBeImmortalChange,
    TResult Function(_SmokeCigarettesChange value)? smokeCigarettesChange,
    TResult Function(_AlwaysPresentInUniChange value)? alwaysPresentInUniChange,
    TResult Function(_TravelWithoutPermissionChange value)?
        travelWithoutPermissionChange,
    TResult Function(_AddDataInStepList value)? addDataInStepList,
    TResult Function(_RemoveFromStepListWitnIndex value)?
        removeFromStepListWitnIndex,
  }) {
    return removeFromStepListWitnIndex?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_QuestionstepsEventInitial value)? initial,
    TResult Function(_DrinkWaterWithMouthChange value)?
        drinkWaterWithMouthChange,
    TResult Function(_CheatingInExamChange value)? cheatingInExamChange,
    TResult Function(_LoveTheEmotionalMovieChange value)?
        loveTheEmotionalMovieChange,
    TResult Function(_ToBeImmortalChange value)? toBeImmortalChange,
    TResult Function(_SmokeCigarettesChange value)? smokeCigarettesChange,
    TResult Function(_AlwaysPresentInUniChange value)? alwaysPresentInUniChange,
    TResult Function(_TravelWithoutPermissionChange value)?
        travelWithoutPermissionChange,
    TResult Function(_AddDataInStepList value)? addDataInStepList,
    TResult Function(_RemoveFromStepListWitnIndex value)?
        removeFromStepListWitnIndex,
    required TResult orElse(),
  }) {
    if (removeFromStepListWitnIndex != null) {
      return removeFromStepListWitnIndex(this);
    }
    return orElse();
  }
}

abstract class _RemoveFromStepListWitnIndex implements QuestionstepsEvent {
  const factory _RemoveFromStepListWitnIndex({required int index}) =
      _$_RemoveFromStepListWitnIndex;

  int get index;
  @JsonKey(ignore: true)
  _$RemoveFromStepListWitnIndexCopyWith<_RemoveFromStepListWitnIndex>
      get copyWith => throw _privateConstructorUsedError;
}
