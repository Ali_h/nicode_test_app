import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:nicode_charecter/application/shared/core_state.dart';
import 'package:nicode_charecter/domain/question_steps/entity/question_steps_entity.dart';
import 'package:nicode_charecter/domain/test_result/entity/test_result_entity.dart';
import 'package:nicode_charecter/domain/test_result/i_test_result_facade.dart';

@injectable
class TestResultCubit extends Cubit<CoreState<TestResultEntity>> {
  final ITestResultFacade _facade;

  TestResultCubit(this._facade) : super(const CoreState.initial());

  Future<void> getRasultData({required QuestionStepsEntity data}) async {
    emit(const CoreState.loadInProgress());

    final testResultData = _facade.getTestResult(answersData: data);

    emit(CoreState.fetchDataSuccessfully(data: testResultData));
  }
}
