import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:nicode_charecter/application/shared/core_state.dart';
import 'package:nicode_charecter/domain/profile/entity/user_profile_entity.dart';
import 'package:nicode_charecter/domain/profile/i_profile_facade.dart';
import 'package:nicode_charecter/infrastructure/storage/shared_pref.dart';

@injectable
class GetProfileBloc extends Cubit<CoreState<UserProfileEntity>> {
  final IProfileFacade _profileFacade;
  final SharedPref _pref;

  GetProfileBloc(this._profileFacade, this._pref) : super(const CoreState.initial());

  Future<void> getUserProfile() async {
    emit(const CoreState.loadInProgress());

    final successOrFailure = await _profileFacade.getUserProfileData();
    emit(
      successOrFailure.fold(
        (failure) => CoreState.fetchDataFailure(dataFailure: failure),
        (vehiclesUsageType) => CoreState.fetchDataSuccessfully(data: vehiclesUsageType),
      ),
    );
  }

  Future<void> logoutUser() async {
    await _pref.signOut();
    return getUserProfile();
  }
}
