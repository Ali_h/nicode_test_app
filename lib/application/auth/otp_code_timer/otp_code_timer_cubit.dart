import 'package:flutter_bloc/flutter_bloc.dart';

class OtpCodeTimerCubit extends Cubit<int> {
  final int initialTimerInSecond;

  OtpCodeTimerCubit({required this.initialTimerInSecond}) : super(initialTimerInSecond);

  void initialTimer() {
    emit(initialTimerInSecond);
  }

  void tick() {
    if (state > 0) {
      emit(state - 1);
    }
  }
}
