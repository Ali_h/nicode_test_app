part of 'auth_bloc.dart';


@freezed
class AuthEvent with _$AuthEvent {
  const factory AuthEvent.initial() = _AuthEventInitial;

  const factory AuthEvent.phoneNumberChanged({required String phone}) = _AuthEventPhoneNumberChanged;

  const factory AuthEvent.otpCodeChange({required String otpCode}) = _AuthEventOtpCodeChange;

  const factory AuthEvent.sendOtpCode() = _AuthEventSendOtpCode;

  const factory AuthEvent.sendPhoneNumber() = _AuthEventSendPhoneNumber;
}
