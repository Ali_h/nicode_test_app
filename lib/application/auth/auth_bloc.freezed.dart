// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'auth_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$AuthEventTearOff {
  const _$AuthEventTearOff();

  _AuthEventInitial initial() {
    return const _AuthEventInitial();
  }

  _AuthEventPhoneNumberChanged phoneNumberChanged({required String phone}) {
    return _AuthEventPhoneNumberChanged(
      phone: phone,
    );
  }

  _AuthEventOtpCodeChange otpCodeChange({required String otpCode}) {
    return _AuthEventOtpCodeChange(
      otpCode: otpCode,
    );
  }

  _AuthEventSendOtpCode sendOtpCode() {
    return const _AuthEventSendOtpCode();
  }

  _AuthEventSendPhoneNumber sendPhoneNumber() {
    return const _AuthEventSendPhoneNumber();
  }
}

/// @nodoc
const $AuthEvent = _$AuthEventTearOff();

/// @nodoc
mixin _$AuthEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(String phone) phoneNumberChanged,
    required TResult Function(String otpCode) otpCodeChange,
    required TResult Function() sendOtpCode,
    required TResult Function() sendPhoneNumber,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(String phone)? phoneNumberChanged,
    TResult Function(String otpCode)? otpCodeChange,
    TResult Function()? sendOtpCode,
    TResult Function()? sendPhoneNumber,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(String phone)? phoneNumberChanged,
    TResult Function(String otpCode)? otpCodeChange,
    TResult Function()? sendOtpCode,
    TResult Function()? sendPhoneNumber,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_AuthEventInitial value) initial,
    required TResult Function(_AuthEventPhoneNumberChanged value)
        phoneNumberChanged,
    required TResult Function(_AuthEventOtpCodeChange value) otpCodeChange,
    required TResult Function(_AuthEventSendOtpCode value) sendOtpCode,
    required TResult Function(_AuthEventSendPhoneNumber value) sendPhoneNumber,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_AuthEventInitial value)? initial,
    TResult Function(_AuthEventPhoneNumberChanged value)? phoneNumberChanged,
    TResult Function(_AuthEventOtpCodeChange value)? otpCodeChange,
    TResult Function(_AuthEventSendOtpCode value)? sendOtpCode,
    TResult Function(_AuthEventSendPhoneNumber value)? sendPhoneNumber,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_AuthEventInitial value)? initial,
    TResult Function(_AuthEventPhoneNumberChanged value)? phoneNumberChanged,
    TResult Function(_AuthEventOtpCodeChange value)? otpCodeChange,
    TResult Function(_AuthEventSendOtpCode value)? sendOtpCode,
    TResult Function(_AuthEventSendPhoneNumber value)? sendPhoneNumber,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AuthEventCopyWith<$Res> {
  factory $AuthEventCopyWith(AuthEvent value, $Res Function(AuthEvent) then) =
      _$AuthEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$AuthEventCopyWithImpl<$Res> implements $AuthEventCopyWith<$Res> {
  _$AuthEventCopyWithImpl(this._value, this._then);

  final AuthEvent _value;
  // ignore: unused_field
  final $Res Function(AuthEvent) _then;
}

/// @nodoc
abstract class _$AuthEventInitialCopyWith<$Res> {
  factory _$AuthEventInitialCopyWith(
          _AuthEventInitial value, $Res Function(_AuthEventInitial) then) =
      __$AuthEventInitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$AuthEventInitialCopyWithImpl<$Res>
    extends _$AuthEventCopyWithImpl<$Res>
    implements _$AuthEventInitialCopyWith<$Res> {
  __$AuthEventInitialCopyWithImpl(
      _AuthEventInitial _value, $Res Function(_AuthEventInitial) _then)
      : super(_value, (v) => _then(v as _AuthEventInitial));

  @override
  _AuthEventInitial get _value => super._value as _AuthEventInitial;
}

/// @nodoc

class _$_AuthEventInitial implements _AuthEventInitial {
  const _$_AuthEventInitial();

  @override
  String toString() {
    return 'AuthEvent.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _AuthEventInitial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(String phone) phoneNumberChanged,
    required TResult Function(String otpCode) otpCodeChange,
    required TResult Function() sendOtpCode,
    required TResult Function() sendPhoneNumber,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(String phone)? phoneNumberChanged,
    TResult Function(String otpCode)? otpCodeChange,
    TResult Function()? sendOtpCode,
    TResult Function()? sendPhoneNumber,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(String phone)? phoneNumberChanged,
    TResult Function(String otpCode)? otpCodeChange,
    TResult Function()? sendOtpCode,
    TResult Function()? sendPhoneNumber,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_AuthEventInitial value) initial,
    required TResult Function(_AuthEventPhoneNumberChanged value)
        phoneNumberChanged,
    required TResult Function(_AuthEventOtpCodeChange value) otpCodeChange,
    required TResult Function(_AuthEventSendOtpCode value) sendOtpCode,
    required TResult Function(_AuthEventSendPhoneNumber value) sendPhoneNumber,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_AuthEventInitial value)? initial,
    TResult Function(_AuthEventPhoneNumberChanged value)? phoneNumberChanged,
    TResult Function(_AuthEventOtpCodeChange value)? otpCodeChange,
    TResult Function(_AuthEventSendOtpCode value)? sendOtpCode,
    TResult Function(_AuthEventSendPhoneNumber value)? sendPhoneNumber,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_AuthEventInitial value)? initial,
    TResult Function(_AuthEventPhoneNumberChanged value)? phoneNumberChanged,
    TResult Function(_AuthEventOtpCodeChange value)? otpCodeChange,
    TResult Function(_AuthEventSendOtpCode value)? sendOtpCode,
    TResult Function(_AuthEventSendPhoneNumber value)? sendPhoneNumber,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _AuthEventInitial implements AuthEvent {
  const factory _AuthEventInitial() = _$_AuthEventInitial;
}

/// @nodoc
abstract class _$AuthEventPhoneNumberChangedCopyWith<$Res> {
  factory _$AuthEventPhoneNumberChangedCopyWith(
          _AuthEventPhoneNumberChanged value,
          $Res Function(_AuthEventPhoneNumberChanged) then) =
      __$AuthEventPhoneNumberChangedCopyWithImpl<$Res>;
  $Res call({String phone});
}

/// @nodoc
class __$AuthEventPhoneNumberChangedCopyWithImpl<$Res>
    extends _$AuthEventCopyWithImpl<$Res>
    implements _$AuthEventPhoneNumberChangedCopyWith<$Res> {
  __$AuthEventPhoneNumberChangedCopyWithImpl(
      _AuthEventPhoneNumberChanged _value,
      $Res Function(_AuthEventPhoneNumberChanged) _then)
      : super(_value, (v) => _then(v as _AuthEventPhoneNumberChanged));

  @override
  _AuthEventPhoneNumberChanged get _value =>
      super._value as _AuthEventPhoneNumberChanged;

  @override
  $Res call({
    Object? phone = freezed,
  }) {
    return _then(_AuthEventPhoneNumberChanged(
      phone: phone == freezed
          ? _value.phone
          : phone // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_AuthEventPhoneNumberChanged implements _AuthEventPhoneNumberChanged {
  const _$_AuthEventPhoneNumberChanged({required this.phone});

  @override
  final String phone;

  @override
  String toString() {
    return 'AuthEvent.phoneNumberChanged(phone: $phone)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _AuthEventPhoneNumberChanged &&
            const DeepCollectionEquality().equals(other.phone, phone));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(phone));

  @JsonKey(ignore: true)
  @override
  _$AuthEventPhoneNumberChangedCopyWith<_AuthEventPhoneNumberChanged>
      get copyWith => __$AuthEventPhoneNumberChangedCopyWithImpl<
          _AuthEventPhoneNumberChanged>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(String phone) phoneNumberChanged,
    required TResult Function(String otpCode) otpCodeChange,
    required TResult Function() sendOtpCode,
    required TResult Function() sendPhoneNumber,
  }) {
    return phoneNumberChanged(phone);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(String phone)? phoneNumberChanged,
    TResult Function(String otpCode)? otpCodeChange,
    TResult Function()? sendOtpCode,
    TResult Function()? sendPhoneNumber,
  }) {
    return phoneNumberChanged?.call(phone);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(String phone)? phoneNumberChanged,
    TResult Function(String otpCode)? otpCodeChange,
    TResult Function()? sendOtpCode,
    TResult Function()? sendPhoneNumber,
    required TResult orElse(),
  }) {
    if (phoneNumberChanged != null) {
      return phoneNumberChanged(phone);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_AuthEventInitial value) initial,
    required TResult Function(_AuthEventPhoneNumberChanged value)
        phoneNumberChanged,
    required TResult Function(_AuthEventOtpCodeChange value) otpCodeChange,
    required TResult Function(_AuthEventSendOtpCode value) sendOtpCode,
    required TResult Function(_AuthEventSendPhoneNumber value) sendPhoneNumber,
  }) {
    return phoneNumberChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_AuthEventInitial value)? initial,
    TResult Function(_AuthEventPhoneNumberChanged value)? phoneNumberChanged,
    TResult Function(_AuthEventOtpCodeChange value)? otpCodeChange,
    TResult Function(_AuthEventSendOtpCode value)? sendOtpCode,
    TResult Function(_AuthEventSendPhoneNumber value)? sendPhoneNumber,
  }) {
    return phoneNumberChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_AuthEventInitial value)? initial,
    TResult Function(_AuthEventPhoneNumberChanged value)? phoneNumberChanged,
    TResult Function(_AuthEventOtpCodeChange value)? otpCodeChange,
    TResult Function(_AuthEventSendOtpCode value)? sendOtpCode,
    TResult Function(_AuthEventSendPhoneNumber value)? sendPhoneNumber,
    required TResult orElse(),
  }) {
    if (phoneNumberChanged != null) {
      return phoneNumberChanged(this);
    }
    return orElse();
  }
}

abstract class _AuthEventPhoneNumberChanged implements AuthEvent {
  const factory _AuthEventPhoneNumberChanged({required String phone}) =
      _$_AuthEventPhoneNumberChanged;

  String get phone;
  @JsonKey(ignore: true)
  _$AuthEventPhoneNumberChangedCopyWith<_AuthEventPhoneNumberChanged>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$AuthEventOtpCodeChangeCopyWith<$Res> {
  factory _$AuthEventOtpCodeChangeCopyWith(_AuthEventOtpCodeChange value,
          $Res Function(_AuthEventOtpCodeChange) then) =
      __$AuthEventOtpCodeChangeCopyWithImpl<$Res>;
  $Res call({String otpCode});
}

/// @nodoc
class __$AuthEventOtpCodeChangeCopyWithImpl<$Res>
    extends _$AuthEventCopyWithImpl<$Res>
    implements _$AuthEventOtpCodeChangeCopyWith<$Res> {
  __$AuthEventOtpCodeChangeCopyWithImpl(_AuthEventOtpCodeChange _value,
      $Res Function(_AuthEventOtpCodeChange) _then)
      : super(_value, (v) => _then(v as _AuthEventOtpCodeChange));

  @override
  _AuthEventOtpCodeChange get _value => super._value as _AuthEventOtpCodeChange;

  @override
  $Res call({
    Object? otpCode = freezed,
  }) {
    return _then(_AuthEventOtpCodeChange(
      otpCode: otpCode == freezed
          ? _value.otpCode
          : otpCode // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_AuthEventOtpCodeChange implements _AuthEventOtpCodeChange {
  const _$_AuthEventOtpCodeChange({required this.otpCode});

  @override
  final String otpCode;

  @override
  String toString() {
    return 'AuthEvent.otpCodeChange(otpCode: $otpCode)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _AuthEventOtpCodeChange &&
            const DeepCollectionEquality().equals(other.otpCode, otpCode));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(otpCode));

  @JsonKey(ignore: true)
  @override
  _$AuthEventOtpCodeChangeCopyWith<_AuthEventOtpCodeChange> get copyWith =>
      __$AuthEventOtpCodeChangeCopyWithImpl<_AuthEventOtpCodeChange>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(String phone) phoneNumberChanged,
    required TResult Function(String otpCode) otpCodeChange,
    required TResult Function() sendOtpCode,
    required TResult Function() sendPhoneNumber,
  }) {
    return otpCodeChange(otpCode);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(String phone)? phoneNumberChanged,
    TResult Function(String otpCode)? otpCodeChange,
    TResult Function()? sendOtpCode,
    TResult Function()? sendPhoneNumber,
  }) {
    return otpCodeChange?.call(otpCode);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(String phone)? phoneNumberChanged,
    TResult Function(String otpCode)? otpCodeChange,
    TResult Function()? sendOtpCode,
    TResult Function()? sendPhoneNumber,
    required TResult orElse(),
  }) {
    if (otpCodeChange != null) {
      return otpCodeChange(otpCode);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_AuthEventInitial value) initial,
    required TResult Function(_AuthEventPhoneNumberChanged value)
        phoneNumberChanged,
    required TResult Function(_AuthEventOtpCodeChange value) otpCodeChange,
    required TResult Function(_AuthEventSendOtpCode value) sendOtpCode,
    required TResult Function(_AuthEventSendPhoneNumber value) sendPhoneNumber,
  }) {
    return otpCodeChange(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_AuthEventInitial value)? initial,
    TResult Function(_AuthEventPhoneNumberChanged value)? phoneNumberChanged,
    TResult Function(_AuthEventOtpCodeChange value)? otpCodeChange,
    TResult Function(_AuthEventSendOtpCode value)? sendOtpCode,
    TResult Function(_AuthEventSendPhoneNumber value)? sendPhoneNumber,
  }) {
    return otpCodeChange?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_AuthEventInitial value)? initial,
    TResult Function(_AuthEventPhoneNumberChanged value)? phoneNumberChanged,
    TResult Function(_AuthEventOtpCodeChange value)? otpCodeChange,
    TResult Function(_AuthEventSendOtpCode value)? sendOtpCode,
    TResult Function(_AuthEventSendPhoneNumber value)? sendPhoneNumber,
    required TResult orElse(),
  }) {
    if (otpCodeChange != null) {
      return otpCodeChange(this);
    }
    return orElse();
  }
}

abstract class _AuthEventOtpCodeChange implements AuthEvent {
  const factory _AuthEventOtpCodeChange({required String otpCode}) =
      _$_AuthEventOtpCodeChange;

  String get otpCode;
  @JsonKey(ignore: true)
  _$AuthEventOtpCodeChangeCopyWith<_AuthEventOtpCodeChange> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$AuthEventSendOtpCodeCopyWith<$Res> {
  factory _$AuthEventSendOtpCodeCopyWith(_AuthEventSendOtpCode value,
          $Res Function(_AuthEventSendOtpCode) then) =
      __$AuthEventSendOtpCodeCopyWithImpl<$Res>;
}

/// @nodoc
class __$AuthEventSendOtpCodeCopyWithImpl<$Res>
    extends _$AuthEventCopyWithImpl<$Res>
    implements _$AuthEventSendOtpCodeCopyWith<$Res> {
  __$AuthEventSendOtpCodeCopyWithImpl(
      _AuthEventSendOtpCode _value, $Res Function(_AuthEventSendOtpCode) _then)
      : super(_value, (v) => _then(v as _AuthEventSendOtpCode));

  @override
  _AuthEventSendOtpCode get _value => super._value as _AuthEventSendOtpCode;
}

/// @nodoc

class _$_AuthEventSendOtpCode implements _AuthEventSendOtpCode {
  const _$_AuthEventSendOtpCode();

  @override
  String toString() {
    return 'AuthEvent.sendOtpCode()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _AuthEventSendOtpCode);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(String phone) phoneNumberChanged,
    required TResult Function(String otpCode) otpCodeChange,
    required TResult Function() sendOtpCode,
    required TResult Function() sendPhoneNumber,
  }) {
    return sendOtpCode();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(String phone)? phoneNumberChanged,
    TResult Function(String otpCode)? otpCodeChange,
    TResult Function()? sendOtpCode,
    TResult Function()? sendPhoneNumber,
  }) {
    return sendOtpCode?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(String phone)? phoneNumberChanged,
    TResult Function(String otpCode)? otpCodeChange,
    TResult Function()? sendOtpCode,
    TResult Function()? sendPhoneNumber,
    required TResult orElse(),
  }) {
    if (sendOtpCode != null) {
      return sendOtpCode();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_AuthEventInitial value) initial,
    required TResult Function(_AuthEventPhoneNumberChanged value)
        phoneNumberChanged,
    required TResult Function(_AuthEventOtpCodeChange value) otpCodeChange,
    required TResult Function(_AuthEventSendOtpCode value) sendOtpCode,
    required TResult Function(_AuthEventSendPhoneNumber value) sendPhoneNumber,
  }) {
    return sendOtpCode(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_AuthEventInitial value)? initial,
    TResult Function(_AuthEventPhoneNumberChanged value)? phoneNumberChanged,
    TResult Function(_AuthEventOtpCodeChange value)? otpCodeChange,
    TResult Function(_AuthEventSendOtpCode value)? sendOtpCode,
    TResult Function(_AuthEventSendPhoneNumber value)? sendPhoneNumber,
  }) {
    return sendOtpCode?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_AuthEventInitial value)? initial,
    TResult Function(_AuthEventPhoneNumberChanged value)? phoneNumberChanged,
    TResult Function(_AuthEventOtpCodeChange value)? otpCodeChange,
    TResult Function(_AuthEventSendOtpCode value)? sendOtpCode,
    TResult Function(_AuthEventSendPhoneNumber value)? sendPhoneNumber,
    required TResult orElse(),
  }) {
    if (sendOtpCode != null) {
      return sendOtpCode(this);
    }
    return orElse();
  }
}

abstract class _AuthEventSendOtpCode implements AuthEvent {
  const factory _AuthEventSendOtpCode() = _$_AuthEventSendOtpCode;
}

/// @nodoc
abstract class _$AuthEventSendPhoneNumberCopyWith<$Res> {
  factory _$AuthEventSendPhoneNumberCopyWith(_AuthEventSendPhoneNumber value,
          $Res Function(_AuthEventSendPhoneNumber) then) =
      __$AuthEventSendPhoneNumberCopyWithImpl<$Res>;
}

/// @nodoc
class __$AuthEventSendPhoneNumberCopyWithImpl<$Res>
    extends _$AuthEventCopyWithImpl<$Res>
    implements _$AuthEventSendPhoneNumberCopyWith<$Res> {
  __$AuthEventSendPhoneNumberCopyWithImpl(_AuthEventSendPhoneNumber _value,
      $Res Function(_AuthEventSendPhoneNumber) _then)
      : super(_value, (v) => _then(v as _AuthEventSendPhoneNumber));

  @override
  _AuthEventSendPhoneNumber get _value =>
      super._value as _AuthEventSendPhoneNumber;
}

/// @nodoc

class _$_AuthEventSendPhoneNumber implements _AuthEventSendPhoneNumber {
  const _$_AuthEventSendPhoneNumber();

  @override
  String toString() {
    return 'AuthEvent.sendPhoneNumber()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _AuthEventSendPhoneNumber);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(String phone) phoneNumberChanged,
    required TResult Function(String otpCode) otpCodeChange,
    required TResult Function() sendOtpCode,
    required TResult Function() sendPhoneNumber,
  }) {
    return sendPhoneNumber();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(String phone)? phoneNumberChanged,
    TResult Function(String otpCode)? otpCodeChange,
    TResult Function()? sendOtpCode,
    TResult Function()? sendPhoneNumber,
  }) {
    return sendPhoneNumber?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(String phone)? phoneNumberChanged,
    TResult Function(String otpCode)? otpCodeChange,
    TResult Function()? sendOtpCode,
    TResult Function()? sendPhoneNumber,
    required TResult orElse(),
  }) {
    if (sendPhoneNumber != null) {
      return sendPhoneNumber();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_AuthEventInitial value) initial,
    required TResult Function(_AuthEventPhoneNumberChanged value)
        phoneNumberChanged,
    required TResult Function(_AuthEventOtpCodeChange value) otpCodeChange,
    required TResult Function(_AuthEventSendOtpCode value) sendOtpCode,
    required TResult Function(_AuthEventSendPhoneNumber value) sendPhoneNumber,
  }) {
    return sendPhoneNumber(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_AuthEventInitial value)? initial,
    TResult Function(_AuthEventPhoneNumberChanged value)? phoneNumberChanged,
    TResult Function(_AuthEventOtpCodeChange value)? otpCodeChange,
    TResult Function(_AuthEventSendOtpCode value)? sendOtpCode,
    TResult Function(_AuthEventSendPhoneNumber value)? sendPhoneNumber,
  }) {
    return sendPhoneNumber?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_AuthEventInitial value)? initial,
    TResult Function(_AuthEventPhoneNumberChanged value)? phoneNumberChanged,
    TResult Function(_AuthEventOtpCodeChange value)? otpCodeChange,
    TResult Function(_AuthEventSendOtpCode value)? sendOtpCode,
    TResult Function(_AuthEventSendPhoneNumber value)? sendPhoneNumber,
    required TResult orElse(),
  }) {
    if (sendPhoneNumber != null) {
      return sendPhoneNumber(this);
    }
    return orElse();
  }
}

abstract class _AuthEventSendPhoneNumber implements AuthEvent {
  const factory _AuthEventSendPhoneNumber() = _$_AuthEventSendPhoneNumber;
}

/// @nodoc
class _$AuthStateTearOff {
  const _$AuthStateTearOff();

  _AuthState call(
      {required PhoneNumber phone,
      required OtpCode otpCode,
      required bool isSubmitting,
      required Option<Either<Failure, Unit>> authFailureOrSuccessOption,
      required Option<Either<Failure, Unit>> otpFailureOrSuccessOption}) {
    return _AuthState(
      phone: phone,
      otpCode: otpCode,
      isSubmitting: isSubmitting,
      authFailureOrSuccessOption: authFailureOrSuccessOption,
      otpFailureOrSuccessOption: otpFailureOrSuccessOption,
    );
  }
}

/// @nodoc
const $AuthState = _$AuthStateTearOff();

/// @nodoc
mixin _$AuthState {
  PhoneNumber get phone => throw _privateConstructorUsedError;
  OtpCode get otpCode => throw _privateConstructorUsedError;
  bool get isSubmitting => throw _privateConstructorUsedError;
  Option<Either<Failure, Unit>> get authFailureOrSuccessOption =>
      throw _privateConstructorUsedError;
  Option<Either<Failure, Unit>> get otpFailureOrSuccessOption =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $AuthStateCopyWith<AuthState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AuthStateCopyWith<$Res> {
  factory $AuthStateCopyWith(AuthState value, $Res Function(AuthState) then) =
      _$AuthStateCopyWithImpl<$Res>;
  $Res call(
      {PhoneNumber phone,
      OtpCode otpCode,
      bool isSubmitting,
      Option<Either<Failure, Unit>> authFailureOrSuccessOption,
      Option<Either<Failure, Unit>> otpFailureOrSuccessOption});
}

/// @nodoc
class _$AuthStateCopyWithImpl<$Res> implements $AuthStateCopyWith<$Res> {
  _$AuthStateCopyWithImpl(this._value, this._then);

  final AuthState _value;
  // ignore: unused_field
  final $Res Function(AuthState) _then;

  @override
  $Res call({
    Object? phone = freezed,
    Object? otpCode = freezed,
    Object? isSubmitting = freezed,
    Object? authFailureOrSuccessOption = freezed,
    Object? otpFailureOrSuccessOption = freezed,
  }) {
    return _then(_value.copyWith(
      phone: phone == freezed
          ? _value.phone
          : phone // ignore: cast_nullable_to_non_nullable
              as PhoneNumber,
      otpCode: otpCode == freezed
          ? _value.otpCode
          : otpCode // ignore: cast_nullable_to_non_nullable
              as OtpCode,
      isSubmitting: isSubmitting == freezed
          ? _value.isSubmitting
          : isSubmitting // ignore: cast_nullable_to_non_nullable
              as bool,
      authFailureOrSuccessOption: authFailureOrSuccessOption == freezed
          ? _value.authFailureOrSuccessOption
          : authFailureOrSuccessOption // ignore: cast_nullable_to_non_nullable
              as Option<Either<Failure, Unit>>,
      otpFailureOrSuccessOption: otpFailureOrSuccessOption == freezed
          ? _value.otpFailureOrSuccessOption
          : otpFailureOrSuccessOption // ignore: cast_nullable_to_non_nullable
              as Option<Either<Failure, Unit>>,
    ));
  }
}

/// @nodoc
abstract class _$AuthStateCopyWith<$Res> implements $AuthStateCopyWith<$Res> {
  factory _$AuthStateCopyWith(
          _AuthState value, $Res Function(_AuthState) then) =
      __$AuthStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {PhoneNumber phone,
      OtpCode otpCode,
      bool isSubmitting,
      Option<Either<Failure, Unit>> authFailureOrSuccessOption,
      Option<Either<Failure, Unit>> otpFailureOrSuccessOption});
}

/// @nodoc
class __$AuthStateCopyWithImpl<$Res> extends _$AuthStateCopyWithImpl<$Res>
    implements _$AuthStateCopyWith<$Res> {
  __$AuthStateCopyWithImpl(_AuthState _value, $Res Function(_AuthState) _then)
      : super(_value, (v) => _then(v as _AuthState));

  @override
  _AuthState get _value => super._value as _AuthState;

  @override
  $Res call({
    Object? phone = freezed,
    Object? otpCode = freezed,
    Object? isSubmitting = freezed,
    Object? authFailureOrSuccessOption = freezed,
    Object? otpFailureOrSuccessOption = freezed,
  }) {
    return _then(_AuthState(
      phone: phone == freezed
          ? _value.phone
          : phone // ignore: cast_nullable_to_non_nullable
              as PhoneNumber,
      otpCode: otpCode == freezed
          ? _value.otpCode
          : otpCode // ignore: cast_nullable_to_non_nullable
              as OtpCode,
      isSubmitting: isSubmitting == freezed
          ? _value.isSubmitting
          : isSubmitting // ignore: cast_nullable_to_non_nullable
              as bool,
      authFailureOrSuccessOption: authFailureOrSuccessOption == freezed
          ? _value.authFailureOrSuccessOption
          : authFailureOrSuccessOption // ignore: cast_nullable_to_non_nullable
              as Option<Either<Failure, Unit>>,
      otpFailureOrSuccessOption: otpFailureOrSuccessOption == freezed
          ? _value.otpFailureOrSuccessOption
          : otpFailureOrSuccessOption // ignore: cast_nullable_to_non_nullable
              as Option<Either<Failure, Unit>>,
    ));
  }
}

/// @nodoc

class _$_AuthState implements _AuthState {
  const _$_AuthState(
      {required this.phone,
      required this.otpCode,
      required this.isSubmitting,
      required this.authFailureOrSuccessOption,
      required this.otpFailureOrSuccessOption});

  @override
  final PhoneNumber phone;
  @override
  final OtpCode otpCode;
  @override
  final bool isSubmitting;
  @override
  final Option<Either<Failure, Unit>> authFailureOrSuccessOption;
  @override
  final Option<Either<Failure, Unit>> otpFailureOrSuccessOption;

  @override
  String toString() {
    return 'AuthState(phone: $phone, otpCode: $otpCode, isSubmitting: $isSubmitting, authFailureOrSuccessOption: $authFailureOrSuccessOption, otpFailureOrSuccessOption: $otpFailureOrSuccessOption)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _AuthState &&
            const DeepCollectionEquality().equals(other.phone, phone) &&
            const DeepCollectionEquality().equals(other.otpCode, otpCode) &&
            const DeepCollectionEquality()
                .equals(other.isSubmitting, isSubmitting) &&
            const DeepCollectionEquality().equals(
                other.authFailureOrSuccessOption, authFailureOrSuccessOption) &&
            const DeepCollectionEquality().equals(
                other.otpFailureOrSuccessOption, otpFailureOrSuccessOption));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(phone),
      const DeepCollectionEquality().hash(otpCode),
      const DeepCollectionEquality().hash(isSubmitting),
      const DeepCollectionEquality().hash(authFailureOrSuccessOption),
      const DeepCollectionEquality().hash(otpFailureOrSuccessOption));

  @JsonKey(ignore: true)
  @override
  _$AuthStateCopyWith<_AuthState> get copyWith =>
      __$AuthStateCopyWithImpl<_AuthState>(this, _$identity);
}

abstract class _AuthState implements AuthState {
  const factory _AuthState(
          {required PhoneNumber phone,
          required OtpCode otpCode,
          required bool isSubmitting,
          required Option<Either<Failure, Unit>> authFailureOrSuccessOption,
          required Option<Either<Failure, Unit>> otpFailureOrSuccessOption}) =
      _$_AuthState;

  @override
  PhoneNumber get phone;
  @override
  OtpCode get otpCode;
  @override
  bool get isSubmitting;
  @override
  Option<Either<Failure, Unit>> get authFailureOrSuccessOption;
  @override
  Option<Either<Failure, Unit>> get otpFailureOrSuccessOption;
  @override
  @JsonKey(ignore: true)
  _$AuthStateCopyWith<_AuthState> get copyWith =>
      throw _privateConstructorUsedError;
}
