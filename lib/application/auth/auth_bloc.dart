import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:nicode_charecter/domain/auth/i_auth_facade.dart';
import 'package:nicode_charecter/domain/core/failure/failure.dart';
import 'package:nicode_charecter/domain/core/value_objects.dart';

part 'auth_bloc.freezed.dart';

part 'auth_event.dart';

part 'auth_state.dart';

@injectable
class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final IAuthFacade _authFacade;

  AuthBloc(this._authFacade) : super(AuthState.initial());

  @override
  Stream<AuthState> mapEventToState(
    AuthEvent event,
  ) async* {
    yield* event.map(
      initial: (_) async* {
        yield AuthState.initial();
      },
      phoneNumberChanged: (event) async* {
        yield state.copyWith(
          phone: PhoneNumber(event.phone),
          authFailureOrSuccessOption: none(),
          otpFailureOrSuccessOption: none(),
        );
      },
      otpCodeChange: (event) async* {
        yield state.copyWith(
          otpCode: OtpCode(event.otpCode),
          authFailureOrSuccessOption: none(),
          otpFailureOrSuccessOption: none(),
        );
      },
      sendPhoneNumber: (event) async* {
        Either<Failure, Unit>? failureOrSuccess;

        if (state.phone.isValid()) {
          yield state.copyWith(
            isSubmitting: true,
            authFailureOrSuccessOption: none(),
            otpFailureOrSuccessOption: none(),
          );

          failureOrSuccess = await _authFacade.sendPhoneNumber(
            phoneNumber: state.phone,
          );
        }

        yield state.copyWith(
          isSubmitting: false,
          authFailureOrSuccessOption: optionOf<Either<Failure, Unit>>(failureOrSuccess),
          otpFailureOrSuccessOption: none(),
        );
      },
      sendOtpCode: (event) async* {
        Either<Failure, Unit>? failureOrSuccess;

        if (state.otpCode.isValid()) {
          yield state.copyWith(
            isSubmitting: true,
            authFailureOrSuccessOption: none(),
            otpFailureOrSuccessOption: none(),
          );

          failureOrSuccess = await _authFacade.sendOtpCode(
            otpCode: state.otpCode,
            phoneNumber: state.phone,
          );
        }

        yield state.copyWith(
          isSubmitting: false,
          authFailureOrSuccessOption: none(),
          otpFailureOrSuccessOption: optionOf<Either<Failure, Unit>>(failureOrSuccess),
        );
      },
    );
  }
}
