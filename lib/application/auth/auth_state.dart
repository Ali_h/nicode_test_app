part of 'auth_bloc.dart';

@freezed
class AuthState with _$AuthState {
  const factory AuthState({
    required PhoneNumber phone,
    required OtpCode otpCode,
    required bool isSubmitting,
    required Option<Either<Failure, Unit>> authFailureOrSuccessOption,
    required Option<Either<Failure, Unit>> otpFailureOrSuccessOption,
  }) = _AuthState;

  factory AuthState.initial() => AuthState(
        phone: PhoneNumber(''),
        otpCode: OtpCode(''),
        isSubmitting: false,
        authFailureOrSuccessOption: none(),
        otpFailureOrSuccessOption: none(),
      );
}
